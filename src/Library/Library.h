#ifndef LIBRARY_H
#define LIBRARY_H

#include "../Basic/DefineTypes.h"
//#include "../Parser/LefParserV2.h"
#include "Layer.h"
#include "LefVia.h"
#include "Macro.h"
#include <map>

namespace UCal{

class LefParserV2;

class Library{
//    friend class LefParserV2;
private:
    static Library* instance;
    std::string macroNameCurrent_;
    std::vector<std::shared_ptr<Layer>> layers_;
    Library(){
        macroNameCurrent_ = "-1";
    }
public:
    static Library* getInstance(){
        if (instance == 0)
        {
            instance = new Library();
        }//end if 

        return instance;
    }//end method 


    int getDataBaseNumber(){ return dataBaseNumber_;} 

    int getNumLayers(){return layerMap_.size();}

    std::vector<std::shared_ptr<Layer>>& getLayers(){
        return layers_;
    }//end method

    std::shared_ptr<UCal::Layer> getLayer(std::string layerName)
    {
        if(layerName.empty()){
            return nullptr;
        }//end if 


        if(layerMap_.find(layerName) == layerMap_.end())
        {        
            return nullptr;
        }//end if             
        
        return layerMap_[layerName];
    }//end method 

    std::shared_ptr<UCal::Macro> getMacro(std::string macroName)
    {
        if(macroName.empty()){
            return nullptr;
        }//end if

        if(macroMap_.find(macroName) == macroMap_.end())
        {        
            return nullptr;
        }//end if             
        
        return macroMap_[macroName];

    }//end method 

    std::shared_ptr<UCal::Layer> getLayer(std::string layerName,IdType idx)
    {
        std::string metal = "Metal";
        std::string via   = "Via";

        if(layerName.empty()){
            return nullptr;
        }//end if 

        std::string search = "";
        if(layerName.compare("routing") == 0){
            search = metal + std::to_string(idx);
        }else if(layerName.compare("cut") == 0){
            search = via + std::to_string(idx);
        }//end if 

        if(layerMap_.find(search) == layerMap_.end())
        {        
            return nullptr;
        }//end if             
        
        return layerMap_[search];
    }//end method 

    // print for test

    void printLayer(){

        std::ofstream     log_file_;
        log_file_.open("Log//LayerLibrary.txt", std::ios::out | std::ios::trunc );

       for(auto itr : layerMap_){
           if(itr.second->hasId())
                log_file_ << ("LayerId:    " + std::to_string(itr.second->getId())    + "\n");

           if(itr.second->hasName())
                log_file_ << ("LayerName:  " + itr.second->getName()  + "\n");

           if(itr.second->hasType())
                log_file_ << ("LayerType:  " + itr.second->getType()  + "\n");

            if(itr.second->hasDirection())
                log_file_ << ("LayerDireciton:  " + itr.second->getDirection()  + "\n");

            if(itr.second->hasMinWidth())
                log_file_ << ("LayerMinWidth:  " + std::to_string(itr.second->getMinWidth())  + "\n");

           if(itr.second->hasArea())
                log_file_ << ("LayerArea:  " + std::to_string(itr.second->getArea())  + "\n");

           if(itr.second->hasWidth())
                log_file_ << ("LayerWidth: " + std::to_string(itr.second->getWidth()) + "\n");

           if(itr.second->hasSpacing()){
               for(auto itrSp : itr.second->getSpacing())
               {
                    if(itrSp.hasSpEOL())
                    {
                        for(auto itrSpEOL : itrSp.getSpEOL()){
                            if(itrSpEOL.hasSpacing()) 
                                log_file_ << ("SPACING " + std::to_string(itrSpEOL.getSpacing()));
                            if(itrSpEOL.hasEndOfLine()) 
                                log_file_ << (" EOL " + std::to_string(itrSpEOL.getEndOfLine()));
                            if(itrSpEOL.hasWidthIn()) 
                                log_file_ << (" WITHIN " + std::to_string(itrSpEOL.getWidthIn()));
                            log_file_ << "\n";
                        }//end for 
                    }//end if
                    // if(itrSp.hasSpTable())
                    // {
                    //     log_file_ << "PRL:\n";
                    //     for(auto itrSpTable : itrSp.getSpTable()){
                    //         if(itrSpTable.hasWidth()) 
                    //             log_file_ << ("Width " + std::to_string(itrSpTable.getWidth()));
                    //         if(itrSpTable.hasWidthSpacing()) 
                    //             log_file_ << ("   " + std::to_string(itrSpTable.getWidthSpacing()));
                    //         log_file_ << "\n";
                    //     }//end for 
                    // }//end if 
               }//end for
           }//end if 

           if(itr.second->hasPitch()){
               log_file_ << ("PITCH " + std::to_string(itr.second->getPitch().first)
                + "  " + std::to_string(itr.second->getPitch().second) + "\n");
           }

       }//end for 

       log_file_.close();
   }//end method 

   void logLayer(){

        std::ofstream     log_file_;
        log_file_.open("LayerLibrary.txt", std::ios::out | std::ios::trunc );

       for(auto itr : layerMap_){
           if(itr.second->hasId())
                log_file_ << ("LayerId:    " + std::to_string(itr.second->getId())    + "\n");

           if(itr.second->hasName())
                log_file_ << ("LayerName:  " + itr.second->getName()  + "\n");

           if(itr.second->hasType())
                log_file_ << ("LayerType:  " + itr.second->getType()  + "\n");

            if(itr.second->hasDirection())
                log_file_ << ("LayerDireciton:  " + itr.second->getDirection()  + "\n");

            if(itr.second->hasMinWidth())
                log_file_ << ("LayerMinWidth:  " + std::to_string(itr.second->getMinWidth())  + "\n");

           if(itr.second->hasArea())
                log_file_ << ("LayerArea:  " + std::to_string(itr.second->getArea())  + "\n");

           if(itr.second->hasWidth())
                log_file_ << ("LayerWidth: " + std::to_string(itr.second->getWidth()) + "\n");

           if(itr.second->hasSpacing()){
               for(auto itrSp : itr.second->getSpacing())
               {
                    if(itrSp.hasSpEOL())
                    {
                        for(auto itrSpEOL : itrSp.getSpEOL()){
                            if(itrSpEOL.hasSpacing()) 
                                log_file_ << ("SPACING " + std::to_string(itrSpEOL.getSpacing()));
                            if(itrSpEOL.hasEndOfLine()) 
                                log_file_ << (" EOL " + std::to_string(itrSpEOL.getEndOfLine()));
                            if(itrSpEOL.hasWidthIn()) 
                                log_file_ << (" WITHIN " + std::to_string(itrSpEOL.getWidthIn()));
                            if(itrSpEOL.hasParallelSpace()) 
                                log_file_ << (" PARALLELEDGE " + std::to_string(itrSpEOL.getParallelSpace()));
                            if(itrSpEOL.hasParallelWidthIn()) 
                                log_file_ << (" WITHIN " + std::to_string(itrSpEOL.getParallelWidthIn()));                                
                            log_file_ << "\n";
                        }//end for 
                    }//end if
                    // if(itrSp.hasSpTable())
                    // {
                    //     log_file_ << "PRL:\n";
                    //     for(auto itrSpTable : itrSp.getSpTable()){
                    //         if(itrSpTable.hasWidth()) 
                    //             log_file_ << ("Width " + std::to_string(itrSpTable.getWidth()));
                    //         if(itrSpTable.hasWidthSpacing()) 
                    //             log_file_ << ("   " + std::to_string(itrSpTable.getWidthSpacing()));
                    //         log_file_ << "\n";
                    //     }//end for 
                    // }//end if 
               }//end for
           }//end if 

           if(itr.second->hasPitch()){
               log_file_ << ("PITCH " + std::to_string(itr.second->getPitch().first)
                + "  " + std::to_string(itr.second->getPitch().second) + "\n");
           }

       }//end for 

       log_file_.close();
   }//end method 

    void printVia(){

        std::ofstream     log_file_;
        log_file_.open("Log//ViaLibrary.txt", std::ios::out | std::ios::trunc );

       for(auto itr : viaMap_){
           if(itr.second->hasId())
                log_file_ << ("LayerId:    " + std::to_string(itr.second->getId())    + "\n");

           if(itr.second->hasName())
                log_file_ << ("LayerName:  " + itr.second->getName()  + "\n");

           for(int i = 0; i < itr.second->getNumLayers(); i++){
               auto layer = itr.second->getLayer(i);
               log_file_ << (layer->getName() + "\n");
               auto rects = itr.second->getRect(layer);
               for(auto itrRect : rects){
                   log_file_ << (itrRect.getString() + "\n");
               }//end for 
               
           }//end for 

           

       }//end for 
       log_file_.close();
    }//end method 

    // Write Cells
    // "AOI221X2 " , "2.600000" , "1.710000",
    void logMacroCells(){
        std::ofstream     log_file_;
        log_file_.open("cells.txt", std::ios::out | std::ios::trunc );

       for(auto itr : macroMap_){
           if(itr.second->hasName())
                log_file_ << ("\"" + itr.second->getName()                 + "\" , "); 

                log_file_ << ("\"" + std::to_string(itr.second->getSizeX()*getDataBaseNumber()) + "\" , "); 

                log_file_ << ("\"" + std::to_string(itr.second->getSizeY()*getDataBaseNumber())  + "\" , \n"); 

       }//end for 
       log_file_.close();
    }//end method 

    void printMacro(){
        std::ofstream     log_file_;
        log_file_.open("Log//MacroLibrary.txt", std::ios::out | std::ios::trunc );

       for(auto itr : macroMap_){
           if(itr.second->hasId())
                log_file_ << ("Id:    " + std::to_string(itr.second->getId())    + "\n");

           if(itr.second->hasName())
                log_file_ << ("Macro:  " + itr.second->getName()  + "\n");

           if(itr.second->hasClass())
                log_file_ << ("CLASS:  " + itr.second->getClass()  + "\n");

            if(itr.second->hasForeign()){
                auto foreigns = itr.second->getForeigns();
                for(auto itrForeign : foreigns){
                    if(itrForeign.hasName())
                        log_file_ << ("FOREIGN:  " + itrForeign.getName());
                    if(itrForeign.hasPointXY())
                        log_file_ << (" " + std::to_string(itrForeign.getPointX()) + " " +
                            std::to_string(itrForeign.getPointY()));
                    log_file_ << "\n";
                }//end for 
            }//end if 

            if(itr.second->hasSizeXY())
                log_file_ << ("Size " + std::to_string(itr.second->getSizeX())+ " " +
                            std::to_string(itr.second->getSizeY()) + "\n") ;

            if(itr.second->hasSymmetry())
                log_file_ << ("Symmetry " + itr.second->getSymmetry()+ "\n");


            if(itr.second->hasSite())
                log_file_ << ("Site " + itr.second->getSite() + "\n");

            if(itr.second->hasPin()){
                auto pins = itr.second->getPin();
                for(auto pinItr : pins){
                    log_file_ << ("Pin " + pinItr->getName() + "\n");
                    auto ports = pinItr->getPort();
                    log_file_ << "Port\n";
                    for(auto portItr : ports){
                        log_file_ << ("Layer " + portItr.getLayer() + "\n");
                        log_file_ << ("Rect " + portItr.getString() + "\n");
                    }//end for 
                    log_file_ << ("END " + pinItr->getName() + "\n");
                }//end for 
                
            }//end if 
           

       }//end for 
       log_file_.close();
    }//end method 


    void logCellPins(){
        std::ofstream     log_file_;
        log_file_.open("cellPins.txt", std::ios::out | std::ios::trunc );

       for(auto itr : macroMap_){
           if(itr.second->hasName())
           log_file_ << ("\"macro\" , "); 
           log_file_ << ("\"" +  itr.second->getName()  + "\" , "); 

            int dummyLen = 2;
           for(int i = 0 ; i < dummyLen ; i++)
                log_file_ << ("\"0\" , "); 
            log_file_ << ("\"0\" ;\n "); 


            if(itr.second->hasPin()){
                auto pins = itr.second->getPin();
                for(auto pinItr : pins){
                    log_file_ << ("\"pin\" , "); 
                    log_file_ << ("\"" +  pinItr->getName()  + "\" , "); 
                    for(int i = 0 ; i < dummyLen ; i++)
                            log_file_ << ("\"0\" , "); 
                    log_file_ << ("\"0\" ;\n "); 

                    auto ports = pinItr->getPort();
                    for(auto portItr : ports){
                        log_file_ << (portItr.getStringMatlabDBU(getDataBaseNumber()) + ", \"" + portItr.getLayer() + "\"; \n");
                    }//end for 

                }//end for 
                
            }//end if 

            if(itr.second->hasOBS()){
                auto obs = itr.second->getOBS();
                log_file_ << ("\"OBS\" , "); 
                for(auto obsItr : obs){
                    log_file_ << ("\"" +  obsItr->getName()  + "\" , "); 
                    for(int i = 0 ; i < dummyLen ; i++)
                            log_file_ << ("\"0\" , "); 
                    log_file_ << ("\"0\" ;\n "); 

                    auto obsRects = obsItr->getOBS();
                    for(auto obsRectsItr : obsRects){
                        log_file_ << (obsRectsItr.getStringMatlabDBU(getDataBaseNumber()) + ", \"" + obsRectsItr.getLayer() + "\"; \n");
                    }//end for 

                }//end for 
                
            }//end if 
           

       }//end for 
       log_file_.close();
    }//end method 




public:
    
    void setMacroNameCurrent(std::string name){macroNameCurrent_ = name;}
    std::string getMacroNameCurrent() {return macroNameCurrent_;}

    void setDataBaseNumber(int dbN){ dataBaseNumber_ = dbN;}


    // get Object builders

    std::shared_ptr<UCal::Layer> getLayerBuilder()
    {
        auto layerPtr = std::make_shared<UCal::Layer>();
        return layerPtr;
    }//end method 

    std::shared_ptr<UCal::LefVia> getViaBuilder()
    {
        auto viaPtr = std::make_shared<UCal::LefVia>();
        return viaPtr;
    }//end method 

    std::shared_ptr<UCal::Macro> getMacroBuilder()
    {
        auto macroPtr = std::make_shared<UCal::Macro>();
        return macroPtr;
    }//end method 

    // Create Objects and insert into map

    void createLayer(std::shared_ptr<Layer> layerPtr){  
        layerMap_[layerPtr->getName()] = layerPtr;
        layers_.push_back(layerPtr);
    }//end method 

    void createVia(std::shared_ptr<UCal::LefVia> viaPtr){  
        viaMap_[viaPtr->getName()] = viaPtr;
    }//end method    

    void createMacro(std::shared_ptr<UCal::Macro> macroPtr){  
        macroMap_[macroPtr->getName()] = macroPtr;
    }//end method
    
    
    
private:
    int dataBaseNumber_;
    std::map<std::string, std::shared_ptr<UCal::Layer>>  layerMap_;
    std::map<std::string, std::shared_ptr<UCal::LefVia>> viaMap_;
    std::map<std::string, std::shared_ptr<UCal::Macro>>  macroMap_;
    
};//end class 

Library* Library::instance = 0;

};//end namespace 

#endif