#ifndef OBS_H
#define OBS_H

#include <string>
#include "../Basic/Geo.h"
#include <vector>

namespace UCal{


class OBS{
    std::string obsName_;
    std::vector<Geo> geometries_;
public:
    OBS(){
        obsName_ = "-1";
    }//end constructor

    void setName(std::string name){
        obsName_ = name;
    }//end name 

    void setOBS(Geo& geo){
        geometries_.push_back(geo);
    }//end method


    std::string getName(){
        return obsName_;
    }//end name 
    std::vector<Geo> getOBS(){return geometries_;}

    bool hasOBS(){if(geometries_.size() <= 0) return false; return true;}

};//end class 

};//end namespace 
#endif