#ifndef MACRO_H
#define MACRO_H

#include "LefBase.h"
#include <vector>
#include "Pin.h"
#include "OBS.h"

namespace UCal{

class Foreign{
    std::string name_;
    std::pair<double, double> pointXY_;
public:
    Foreign(){
        name_ = "-1";
        pointXY_.first  = -1;
        pointXY_.second = -1;
    }//end method

    // set & get
    void setName(std::string name){name_ = name;}
    void setPointX(double x){pointXY_.first  = x;}
    void setPointY(double y){pointXY_.second = y;}
    void setPointXY(std::pair<double, double> pointXY){
        pointXY_.first  = pointXY.first;
        pointXY_.second = pointXY.second;
    }//end method 

    std::string getName(){return name_;}
    double getPointX(){return pointXY_.first;}
    double getPointY(){return pointXY_.second;}
    std::pair<double, double> getPointXY(){ return pointXY_;}
        
    //checker 
    bool hasName(){if(name_.compare("-1") == 0) return false; return true;}
    bool hasPointX(){if(pointXY_.first  == -1) return false; return true;}
    bool hasPointY(){if(pointXY_.second == -1) return false; return true;}
    bool hasPointXY(){if(pointXY_.first  == -1 || pointXY_.second == -1) return false; return true;}


};//end class 


class Macro : public LefBase{
    std::string class_;
    std::vector<Foreign> foreigns_;
    std::pair<double,double> origin_;
    std::pair<double,double> sizeXY_;
    std::string symmetry_;
    std::string site_;
    std::map<std::string, std::shared_ptr<Pin>> pinMap_;
    std::vector<std::shared_ptr<Pin>> pins_;
    std::vector<std::shared_ptr<OBS>> obs_;
public:

    Macro(){
        class_      = "-1";
        origin_.first      = -1;
        origin_.second     = -1;
        sizeXY_.first      = -1;
        sizeXY_.second     = -1;
        symmetry_   = "-1";
        site_       = "-1";
    }

    // set & get
    void setClass(std::string className) {class_ = className;}
    void setForeign(Foreign& foreign){ foreigns_.push_back(foreign);}
    void setOriginX(double x) { origin_.first  = x;}
    void setOriginY(double y) { origin_.second = y;}
    void setSizeX(double x) {sizeXY_.first  = x;}
    void setSizeY(double y) {sizeXY_.second = y;}    
    void setSymmetry(std::string symmetry){ symmetry_ = symmetry;}
    void setSite(std::string site){site_ = site;}



    std::string getClass() {return class_;}    
    std::vector<Foreign>  getForeigns(){return foreigns_;}
    double getOriginX() { return origin_.first ;}
    double getOriginY() { return origin_.second;}
    std::pair<double,double> getOriginXY() { origin_;}
    double getSizeX() {return sizeXY_.first ;}
    double getSizeY() {return sizeXY_.second;}    
    std::pair<double,double> getSizeXY() {return sizeXY_;}
    std::string getSymmetry(){return symmetry_;}
    std::string getSite(){return site_;}
    std::shared_ptr<Pin> getPin(std::string pinName) {return pinMap_[pinName];}
    std::vector<std::shared_ptr<Pin>> getPin() {return pins_;}
    std::vector<std::shared_ptr<OBS>> getOBS() {return obs_;}

    //checker 
    bool hasClass() {if( class_.compare("-1")==0 ) return false; return true;}
    bool hasForeign(){ if(foreigns_.size() <= 0) return false; return true;}
    bool hasOriginX() { if(origin_.first   == -1) return false; return true;}
    bool hasOriginY() { if(origin_.second  == -1) return false; return true;}
    bool hasOriginXY() { if(origin_.first   == -1 || origin_.second  == -1) return false; return true;}
    bool hasSizeX() { if(sizeXY_.first   == -1) return false; return true;}
    bool hasSizeY() { if(sizeXY_.second  == -1) return false; return true;}    
    bool hasSizeXY() { if(sizeXY_.first   == -1 || sizeXY_.second  == -1) return false; return true;}    
    bool hasSymmetry(){ if( symmetry_.compare("-1")==0 ) return false; return true;}
    bool hasSite(){if( site_.compare("-1")==0 ) return false; return true;}
    bool hasPin(std::string pinName){
        if(pinMap_.find(pinName) == pinMap_.end())
        {
            return false;
        }//end if 
        return true;
    }//end method 
    bool hasPin(){ if(pins_.size() <= 0) return false; return true;}
    bool hasOBS(){ if(obs_.size() <= 0) return false; return true;}
        

    // Builder
    std::shared_ptr<UCal::Pin> getPinBuilder(){
        auto pinPtr = std::make_shared<UCal::Pin>();
        return pinPtr;
    }

    std::shared_ptr<UCal::OBS> getOBSBuilder(){
        auto obsPtr = std::make_shared<UCal::OBS>();
        return obsPtr;
    }

    // Create Pin
    void createPin(std::shared_ptr<Pin> pin){
        pinMap_[pin->getName()] = pin;
        pins_.push_back(pin);
    }//end method 

    void createOBS(std::shared_ptr<OBS> obs){
        obs_.push_back(obs);
    }//end method 

    
};//end class 
};//end namespace 


#endif 