#ifndef LEFVIA_H
#define LEFVIA_H

#include "LefBase.h"
#include "../Basic/Rect.h"
#include "Layer.h"
#include <map>
#include <vector>


namespace UCal{

class LefVia : public LefBase{
    std::string  default_;
    std::string  generated_;
    std::map<std::shared_ptr<Layer>,std::vector<Rect>> viaMap_;
    std::vector<std::shared_ptr<Layer>> layers_;

public:
    LefVia(){
        default_   = "-1";
        generated_ = "-1";
    }

    // set & get
    void setDefault(){default_ = "default";}
    void setGenerated(){generated_ = "generated";}
    void setVia(std::shared_ptr<Layer> layer, Rect& rect){
        if(viaMap_.find(layer) == viaMap_.end())
        {   
            std::vector<Rect> rects; rects.push_back(rect);     
            viaMap_[layer] = rects;  
            layers_.push_back(layer);
        }else{
            auto rects = viaMap_[layer];
            rects.push_back(rect);
        }//end if 
    }//end method 


    

    std::string getDefault()  {return default_  ; }
    std::string getGenerated(){return generated_; }
    std::vector<Rect> getRect(std::shared_ptr<Layer> layer){
        return viaMap_[layer];
    }//end method 
    int getNumLayers(){return layers_.size();}
    std::shared_ptr<Layer> getLayer(int idx){return layers_[idx];}


    // checker
    bool hasDefault(){if(default_.compare("-1")==0) return false; return true;}
    bool hasGenerated(){if(generated_.compare("-1")==0) return false; return true;}
    bool hasVia(std::shared_ptr<Layer> layer){
        auto rects = viaMap_[layer];
        if(rects.size() <= 0) return false; 
        return true;
    }

};//end class 

};//end namespace 


#endif 