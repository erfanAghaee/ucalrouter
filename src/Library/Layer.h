#ifndef LAYER_H
#define LAYER_H

#include "LefBase.h"
#include "../Basic/DefineTypes.h"
#include <vector>
namespace UCal{

class Track{
    std::string macro_;
    double x_;
    double numX_;
    double stepX_;
public:
    Track(){
        std::string macro_;
        double x_;
        double numX_;
        double stepX_;
    }//end method 

    // set & get
    void setMacro(std::string macro){macro_ = macro;}
    void setX   (double x   ){x_ = x;}
    void setNum (double num ){numX_ = num;}
    void setStep(double step){stepX_ = step;}

    std::string getMacro(){ return macro_;}
    double      getX   (){return x_    ;}
    double      getNum (){return numX_ ;}
    double      getStep(){return stepX_;}
    //checker 
    bool hasMacro(){ if(macro_.compare("-1") == 0) return false; return true;}
    bool hasX   () { if(x_     == -1) return false; return true;}
    bool hasNum () { if(numX_  == -1) return false; return true;}
    bool hasStep() { if(stepX_ == -1) return false; return true;}



};//end class 

class SpEOL{
    double spacing_;
    double endOfLine_;
    double widthIn_;
    double parSpace_;
    double parWidthIn_;
public:
    SpEOL(){
        spacing_   = -1;
        endOfLine_ = -1;
        widthIn_   = -1;
        parSpace_   = -1;
        parWidthIn_ = -1;
    }//end method 
    
    // set & get
    void setSpacing(double spacing){spacing_ = spacing;}
    void setEndOfLine(double endOfLine){endOfLine_ = endOfLine;}
    void setWidthIn(double widthIn){ widthIn_ = widthIn;}
    void setParallelWidthIn(double parWidthIn){ parWidthIn_ = parWidthIn;}
    void setParallelSpace(double parSpace){ parSpace_ = parSpace;}


    double getSpacing(){return spacing_;}
    double getEndOfLine(){return endOfLine_;}
    double getWidthIn(){return widthIn_;}
    double getParallelWidthIn(){return parWidthIn_;}
    double getParallelSpace(){return parSpace_;}


    // checker
    bool hasSpacing(){if(spacing_ == -1 ) return false; return true;}
    bool hasEndOfLine(){if(endOfLine_ == -1 ) return false; return true;}
    bool hasWidthIn(){if(widthIn_ == -1 ) return false; return true;}
    bool hasParallelWidthIn(){if(parWidthIn_ == -1 ) return false; return true;}
    bool hasParallelSpace(){if(parSpace_ == -1 ) return false; return true;}


};//end class 

class SpTable{
    // double width_;
    // double widthSpacing_;
    int numWidth_;
    int numParallel_;

    std::vector<std::vector<double>> spTable_;

public:
    SpTable(int numParallel,int numWidth){
        
    }//end constructor

    // set & get
    // void setWidth(double width){width_ = width;}
    // void setWidthSpacing(double widthSpacing){widthSpacing_ = widthSpacing;}
    void setPRL(int idx, int prlValue){
        spTable_[0][idx+1] = prlValue;
    }

    void setWidth(int idx, int prlValue){
        spTable_[idx+1][0] = prlValue;
    }

    void setSpacing(int i, int j, int prlValue){
        spTable_[i+1][j+1] = prlValue;
    }

    // double getSpace(int i, int j){
    //     if(i <= numWi)
    // }//end method 


    


    // double getWidth(){return width_;}
    // double getWidthSpacing(){return widthSpacing_;}

    // checker
    // bool hasWidth(){if(width_ == -1 ) return false; return true;} 
    // bool hasWidthSpacing(){if(widthSpacing_ == -1 )return false; return true;}

};//end class 

class Spacing{
    std::vector<SpEOL>   spEOLs_;
    std::vector<SpTable> spTables_;

public: 
    Spacing(){
    }

    // set & get
    void setSpEOL(SpEOL& spEOL){spEOLs_.push_back(spEOL);}
    void setSpTable(SpTable& spTable){spTables_.push_back(spTable);}

    std::vector<SpEOL>   getSpEOL(){return spEOLs_;}
    std::vector<SpTable> getSpTable(){return spTables_;}
    
    //checker
    bool hasSpEOL(){if(spEOLs_.size() <= 0) return false; return true;}
    bool hasSpTable(){if(spTables_.size() <= 0) return false; return true;}


};//end class 

class Layer : public LefBase{
    std::string              type_;
    std::string              direction_;
    double                   minWidth_;
    double                   area_;
    double                   width_;
    std::vector<Spacing>     spacings_;
    std::pair<double,double> pitch_;
    std::vector<Track>       tracks_;
    
public:
    Layer(){
       type_      = "-1";
       direction_ = "-1";
       minWidth_  = -1;
       area_      = -1;
       width_     = -1;
       pitch_.first  = -1;
       pitch_.second = -1;
    }

    // set & get
    void  setType(std::string type){ type_ = type;}
    void  setDirection(std::string direction){direction_ = direction;};
    void  setMinWidth(double minWidth){minWidth_ = minWidth;}
    void  setArea(double area){area_ = area;}
    void  setWidth(double width){width_ = width;}
    void  setSpacing(Spacing& spacing){spacings_.push_back(spacing);}
    void  setPitch(std::pair<double,double>& pitch){
        pitch_.first  = pitch.first;
        pitch_.second = pitch.second;
    }//end method
    void  setTrack(Track& track){tracks_.push_back(track);}

    std::string                getType(){return type_;}
    std::string                getDirection(){return direction_;}
    double                     getMinWidth(){return minWidth_;}
    double                     getArea(){return area_;}
    double                     getWidth(){return width_;}
    std::vector<Spacing>&      getSpacing(){return spacings_;}
    std::pair<double,double>   getPitch(){return pitch_;}
    std::vector<Track>&        getTracks(){return tracks_;}

    // check methods 
    bool hasType(){if(type_.compare("-1")==0) return false; return true;}
    bool hasDirection(){if(direction_.compare("-1")==0) return false; return true;}
    bool hasMinWidth(){if(minWidth_ == -1) return false; return true;}
    bool hasArea(){if(area_ == -1) return false; return true;}
    bool hasWidth(){if(width_ == -1) return false; return true;}
    bool hasSpacing(){if(spacings_.size() <= 0) return false; return true;}
    bool hasPitch(){if(pitch_.first == -1 || pitch_.second == -1) return false; return true;}
    bool hasTrack(){if(tracks_.size() <= 0) return false; return true;}
};//end class 

};//end namespace 

#endif