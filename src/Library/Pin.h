#ifndef PIN_H
#define PIN_H

#include "LefBase.h"
#include <map>
#include <memory>
#include <vector>
#include "Layer.h"
#include "../Basic/Geo.h"
namespace UCal{

class Pin : public LefBase{
    std::string direction_;
    std::string shape_;
    std::string use_;
    // std::map<std::shared_ptr<Layer>,std::vector<Rect>> portMap_;
    // std::vector<std::shared_ptr<Layer>> layers_;
    std::vector<Geo> geometries_;
public:

    Pin(){
        direction_ = "-1";
        shape_ = "-1";
        use_ = "-1";
    }//end method

    // set & get
    void setDirection(std::string direction){direction_ = direction;}
    void setShape(std::string shape){shape_ = shape;}
    void setUse(std::string use){use_ = use;}
    // void setPort(std::shared_ptr<Layer> layer, Rect& rect){
    //     if(portMap_.find(layer) == portMap_.end())
    //     {   
    //         std::vector<Rect> rects; rects.push_back(rect);     
    //         portMap_[layer] = rects;  
    //         // layers_.push_back(layer);
    //     }else{
    //         auto rects = portMap_[layer];
    //         rects.push_back(rect);
    //     }//end if 
    // }//end method 
    void setPort(Geo& geo){
        geometries_.push_back(geo);
    }//end method   

    std::string getDirection(){return direction_;}
    std::string getShape(){return shape_;}
    std::string getUse(){ return use_;}
    // std::vector<Rect> getPort(std::shared_ptr<Layer> layer){return portMap_[layer];}
    // std::map<std::shared_ptr<Layer>,std::vector<Rect>> getPort(){return portMap_;}
    std::vector<Geo> getPort(){return geometries_;}

    //checker 
    bool hasDirection(){if(direction_.compare("-1") == 0) return false; return true;}
    bool hasShape(){if(shape_.compare("-1") == 0) return false; return true;}
    bool hasUse(){if(use_.compare("-1") == 0) return false; return true;}
    // bool hasPort(std::shared_ptr<Layer> layer){
    //     if(portMap_.find(layer) == portMap_.end())
    //     {   
    //         return false;
    //     }else{
    //         auto rects = portMap_[layer];
    //         if(rects.size() <= 0) return false;
    //     }//end if 
    //     return true;
    // }//end method 
    bool hasPort(){if(geometries_.size() <= 0) return false; return true;}
};//end class

};//end namespace 

#endif 