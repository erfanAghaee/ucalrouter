
#include <iostream>


void MainMenu(){
    std::cout << "\n"; // skipping the terminal line
	std::cout << std::string(1, ' ') << R"( __    __ _____     __     __    _____  ___ _ _   _ _____ ___ _____   )" << "\n";
	std::cout << std::string(1, ' ') << R"(|  |  |  |   __|   /  \   |  |  |  _  \|  _  | | | |_   _|  _|  _  \  )" << "\n";
	std::cout << std::string(1, ' ') << R"(|  |  |  |  |     / __ \  |  |  | |_|  | | | | | | | | | | |_| |_|  | )" << "\n";
	std::cout << std::string(1, ' ') << R"(|  |__|  |  |__  / |__| \ |  |__|     /| |_| | |_| | | | | |_|     /  )" << "\n";
	std::cout << std::string(1, ' ') << R"(|________|_____|/__/  \__\|_____|__|\_\|_____|_____| |_| |___|__|\_\  )" << "\n";
	std::cout << "\n" << std::endl;

}