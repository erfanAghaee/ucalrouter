#ifndef DEFBASE_H
#define DEFBASE_H

#include <string>
#include "../Basic/DefineTypes.h"


namespace UCal{


class DefBase{
    IdType id_;
    std::string name_;

public:
    DefBase(){
        id_   = 0 ;
        name_ = "-1";
    }
    // set & get
    void setId(IdType id){id_ = id;}
    void setName(std::string name){ name_ = name;}


    IdType getId(){return id_;}
    std::string  getName(){return name_;}
    // check
    bool hasId(){if(id_ == 0) return false; return true;}
    bool hasName(){if(name_.compare("-1") == 0) return false; return true;}
    
};//end class

};//end namespace 

#endif