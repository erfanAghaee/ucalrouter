#ifndef DESIGN_H
#define DESIGN_H

#include "../Basic/DefineTypes.h"
// #include "../Parser/DefParserV2.h"
#include "../Basic/Rect.h"
#include "DefBase.h"
#include <vector>
#include <unordered_map>

namespace UCal{


class Connection{
    std::string instance_;
    std::string pin_;
    bool        isConnected_;
public:
    Connection(){
        instance_ = "-1";
        pin_      = "-1";
        isConnected_ = false;        
    }//end method   

    // set & get
    void setInstance(std::string instance){ instance_ = instance;}
    void setPin(std::string pin)          { pin_ = pin;}
    void setIsConnected(bool isConnected) { isConnected_ = isConnected;}

    std::string getInstance(){ return instance_;}
    std::string getPin()     { return pin_;}
    bool getIsConnected()    { return isConnected_;}

    // checker
    bool hasInstance(){ if(instance_.compare("-1") == 0 ) return false; return true;}
    bool hasPin()     { if(pin_.compare("-1") == 0 )      return false; return true;}
};//end class


class Net : public DefBase{
    std::vector<Connection> connections_;
public:

    //set & get
    void setConnection(Connection& connection){connections_.push_back(connection);}

    std::vector<Connection> getConnections() {return connections_;}

    // checker
    bool hasConnection() {if (connections_.size() <= 0) return false; return true;}


};//end classs



// class DefParserV2;
class Component: public DefBase{
    std::string macroName_;
    std::pair<double,double> placementXY_;
    std::string orient_;
public:
    Component(){
        macroName_   = "-1";
        placementXY_.first  = -1;
        placementXY_.second = -1;
        orient_ = "-1";
    }//end method

    // set & get
    void setMacroName(std::string macroName){macroName_ = macroName;}
    void setPlacementX(double x){placementXY_.first  = x;}
    void setPlacementY(double y){placementXY_.second = y;}
    void setOrient(std::string orient){orient_ = orient;}

    
    double      getPlacementX(){return placementXY_.first;}
    double      getPlacementY(){return placementXY_.second;}
    std::string getMacroName() {return macroName_;}
    std::string getOrient()    {return orient_;}

    // Checker   
    bool hasPlacementX(){if(placementXY_.first  == -1)     return false; return true ;}
    bool hasPlacementY(){if(placementXY_.second == -1)     return false; return true ;}
    bool hasMacroName() {if(macroName_.compare("-1") == 0) return false; return true ;}
    bool hasOrient()    {if(orient_.compare("-1") == 0)    return false; return true ;}
 

};//end class

class Row : public DefBase{
    std::string              coreSite_;
    std::pair<double,double> rowXY_;
    std::string              orient_;
    std::pair<double,double> numXY_;
    std::pair<double,double> stepXY_;
public:
    Row(){
        coreSite_ = "-1";
        rowXY_.first = -1;rowXY_.second = -1;
        orient_ = "-1";
        numXY_.first = -1; numXY_.second = -1;
        stepXY_.first = -1;stepXY_.second = -1;
    }//end method 

    // set & get
    void setCoreSite(std::string coreSite){coreSite_ = coreSite;}
    void setRowXY(std::pair<double,double> rowXY){ rowXY_.first = rowXY.first; rowXY_.second = rowXY.second;}
    void setRowX(double rowX){ rowXY_.first = rowX;}
    void setRowY(double rowY){ rowXY_.second = rowY;}
    void setOrient(std::string orient){ orient_ = orient;}
    void setNumXY(std::pair<double,double> numXY){ numXY_.first = numXY.first; numXY_.second = numXY.second;}
    void setNumX(double numX){ numXY_.first = numX;}
    void setNumY(double numY){ numXY_.second = numY;}    
    void setStepXY(std::pair<double,double> stepXY){ stepXY_.first = stepXY.first; stepXY_.second = stepXY.second;}
    void setStepX(double stepX){ stepXY_.first = stepX;}
    void setStepY(double stepY){ stepXY_.second = stepY;} 

    std::string              getCoreSite(){ return coreSite_;}
    std::pair<double,double> getRowXY()   { return rowXY_;}
    double                   getRowX()    { return rowXY_.first;}
    double                   getRowY()    { return rowXY_.second;}
    std::string              getOrient()  { return orient_ ;}
    std::pair<double,double> getNumXY()   { return numXY_;}
    double                   getNumX()    { return numXY_.first;}
    double                   getNumY()    { return numXY_.second;}    
    std::pair<double,double> getStepXY()  { return stepXY_;}
    double                   getStepX()   { return stepXY_.first;}
    double                   getStepY()   { return stepXY_.second;} 
    
    // Checker
    bool hasCoreSite(){ if(coreSite_.compare("-1") == 0) return false; return true;}
    bool hasRowXY()   { if(hasRowX()  == -1 || hasRowY() == -1 ) return false; return true;}
    bool hasRowX()    { if(rowXY_.first  == -1 ) return false; return true;}
    bool hasRowY()    { if(rowXY_.second == -1 ) return false; return true;}
    bool hasOrient()  { if( orient_.compare("-1") == 0) return false; return true;}
    bool hasNumXY()   { if(hasNumX()  == -1 || hasNumY() == -1) return false; return true;}
    bool hasNumX()    { if(numXY_.first  == -1 ) return false; return true;}
    bool hasNumY()    { if(numXY_.second == -1 ) return false; return true;}
    bool hasStepXY()  { if(hasStepX()  == -1 || hasStepY() == -1 ) return false; return true;}
    bool hasStepX()   { if(stepXY_.first  == -1 ) return false; return true;}
    bool hasStepY()   { if(stepXY_.second == -1 ) return false; return true;}




};//end class

class Design{
    friend class DefParserV2;
private:
    std::string version_;
    std::string divideChar_;
    std::string busBitChar_;
    std::string desingName_;
    double      unit_;
    Rect        dieArea_;
    std::vector<Row> rows_;
    std::unordered_map<std::string,std::shared_ptr<Component>> componentMap_;
    std::unordered_map<std::string,std::shared_ptr<Net>>       netsMap_;

    std::string netCurrentName_;

    std::vector<std::shared_ptr<Component>> components_;
    std::vector<std::shared_ptr<Net>>       nets_;
    

public:

    Design(){
            version_    = "-1";
            divideChar_ = "-1";
            busBitChar_ = "-1";
            desingName_ = "-1";
            unit_       = -1;
            dieArea_.setRect(-1,-1,-1,-1); 
            netCurrentName_ = "-1" ;
    }//end method 

    // get
    std::string                 getVersion()   { return version_    ;}
    std::string                 getDivideChar(){ return divideChar_ ;}
    std::string                 getBusBitChar(){ return busBitChar_ ;}
    std::string                 getDesingName(){ return desingName_ ;}
    double                      getUnit()      { return unit_       ;}
    Rect                        getDieArea()   { return dieArea_    ;}
    std::vector<Row>&           getRows(){return rows_;}
    std::shared_ptr<Component>  getComponent(std::string compName){return componentMap_[compName]; }
    std::vector<std::shared_ptr<Component>>& getComponents(){return components_;}
    std::shared_ptr<Net>        getNet(std::string netName){return netsMap_[netName]; }
    std::vector<std::shared_ptr<Net>>& getNets(){return nets_;}
    std::string                 getNetCurrentName() {return netCurrentName_;}


    
    

    // checker 
    bool hasVersion()   { if(version_.compare("-1") == 0 ) return false; return true; }
    bool hasDivideChar(){ if(divideChar_.compare("-1") == 0 ) return false; return true; }
    bool hasBusBitChar(){ if(busBitChar_.compare("-1") == 0 ) return false; return true; }
    bool hasDesingName(){ if(desingName_.compare("-1") == 0 ) return false; return true; }
    bool hasUnit()      { if(unit_ == -1 ) return false; return true; }
    bool hasDieArea()   { if(dieArea_.hasRect())           return false; return true; }
    bool hasRow()       { if(rows_.size() <= 0 ) return false; return true;}
    bool hasNetCurrentName() { if(netCurrentName_.compare("-1") == 0) return false; return true;}




// protected:
    // set 
    void setVersion(std::string version){version_ = version;}
    void setDivideChar(std::string divChar){divideChar_ = divChar;}
    void setBusBitChar(std::string busBit){busBitChar_ = busBit;}
    void setDesingName(std::string designName){desingName_ = designName;}
    void setUnit(double unit){unit_ = unit;}
    void setDieArea(Rect& rect){
        dieArea_.setRect(rect.getXl(),rect.getYl(),rect.getXh(),rect.getYh());
    }
    void setRow(Row& row){rows_.push_back(row);}
    void setNetCurrentName(std::string netName){netCurrentName_ = netName;}
 

    // Builder
    std::shared_ptr<Component> getComponentBuilder(){
        return std::make_shared<Component>();
    }//end method 


    void createComponent(std::shared_ptr<Component> component){
        componentMap_[component->getName()] = component;
        components_.push_back(component);
    }//end method   


    std::shared_ptr<Net> getNetBuilder(){
        return std::make_shared<Net>();
    }//end method 


    void createNet(std::shared_ptr<Net> net){
        netsMap_[net->getName()] = net;
        nets_.push_back(net);
    }//end method  


    void logInstances(){
        std::ofstream     log_file_;
        log_file_.open("instances.txt", std::ios::out | std::ios::trunc );

       for(auto itr : componentMap_){
                log_file_ << ("\"" + itr.second->getName()                       + "\" , "); 
            
                log_file_ << ("\"" + itr.second->getMacroName()                  + "\" , "); 

                log_file_ << ("\"" + std::to_string(itr.second->getPlacementX()) + "\" , "); 

                log_file_ << ("\"" + std::to_string(itr.second->getPlacementY())  + "\" , "); 

                log_file_ << ("\"" + itr.second->getOrient()  + "\" , \n"); 

       }//end for 
       log_file_.close();

    }//end method 






};//end class 
};//end namespace 

#endif