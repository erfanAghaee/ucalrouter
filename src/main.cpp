#include <iostream>
#include "MainMenu/MainMenu.h"

#include "Basic/Object.h"
#include "Basic/Rect.h"
#include "Parser/LefParser.h"
#include "States/StartState.h"
#include "Temp/ShapeFactory.h"
#include "States/StateContr.h"
//
#include "TestBench/TestContrl.h"
#include "Library/Library.h"
#include "Library/Layer.h"


#include "Design/Design.h"
//#include "DataStructure/LineIntersection.h"
//#include "DataStructure/BSTreeSTD.h"
//#include "Boost/RectB.h"
#include "Engine/Engine.h"


int    argc_global;
char** argv_global;



int main(int argc, char** argv){
    std::shared_ptr<UCal::Design> design = std::make_shared<UCal::Design>();
    MainMenu();
//
    argc_global = argc;
    argv_global = argv;
//
//    std::cout << "hi";
   

//    int lenPRL   = 5;
//    int lenWidth = 5;
//    std::vector<std::vector<double>> spTable_  
//   {{0          , 0.0  ,0.22 ,0.47 ,0.63 ,1.50},
//   {0.0        , 0.05 ,0.05 ,0.05 ,0.05 ,0.05},
//   {0.10       , 0.05 ,0.06 ,0.06 ,0.06 ,0.06},
//   {0.28       , 0.05 ,0.10 ,0.10 ,0.10 ,0.10},
//   {0.47       , 0.05 ,0.10 ,0.13 ,0.13 ,0.13},
//   {0.63       , 0.05 ,0.10 ,0.13 ,0.15 ,0.15},
//   {1.50       , 0.05 ,0.10 ,0.13 ,0.15 ,0.50}};
//
//
//   std::vector<std::vector<double>> spTable2_;
//
//   spTable2_.resize(lenPRL);
//   for(int i = 0; i < spTable_.size(); i++){
//        spTable2_.resize(spTable_[i].size());
//   }//end for 
//
//    // for(int i = 0 ; i < spTable_.size(); i++){
//    //     spTable2_[i].push_back(spTable_[0][i]);
//    // }//end for 
//
//    for(int i = 0; i < spTable_.size(); i++){
//        for(int j = 0; j < spTable_[i].size(); j++){
//            std::cout <<    spTable_[i][j] << "  " ;
//        }
//        std::cout << std::endl;
//    }

    UCal::Engine engine(design);

    // UCal::StateContr stateContr;
    // std::shared_ptr<UCal::State> statePtr = stateContr.getState("start");
    // statePtr->doAction();

    
//     UCal::TestContr testContrl;
//     std::shared_ptr<UCal::Test> test;
//
//     test = testContrl.getTest("DataStructureTest");
//    
//     bool pass = test->doTest(design);
//
//     std::cout << "Pass: " << pass << std::endl;



    

    return 0;
}//end method 








