#ifndef STARTSTATE_H
#define STARTSTATE_H

#include "State.h"

#include <iostream>
#include <string>
#include <memory>
#include "../Parser/DefParser.h"

namespace UCal{


class StartState: public State{
public:
   void doAction() override{
      state_ = "StartState";
      // Parser Lef File
      // UCal::LefParser lefParser(argc_global,argv_global);

      UCal::DefParser defParser(argc_global,argv_global);

   }//end method 

   std::string getState(){
       return state_;
   }//end method 


};//end class



};//end namespace 

#endif