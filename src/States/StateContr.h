#ifndef StateContr_H
#define StateContr_H
#include "State.h"
#include <memory>
#include <string>
#include "StartState.h"
#include "StopState.h"
namespace UCal{
class StateContr{
public:
    std::shared_ptr<UCal::State> getState(std::string stateType){
        if(stateType.empty()){
            return nullptr;
        }		
        
        if(stateType.compare("start") == 0 ){
            auto StartStatePtr = std::make_shared<StartState>();
            return StartStatePtr;
        }else if(stateType.compare("stop") == 0 ){
            auto StopStatePtr = std::make_shared<StopState>();
            return StopStatePtr;
        } 

        return nullptr;
    
    }//end method    
};//end class
};//end namespace 

#endif 