#ifndef StopState_H
#define StopState_H
#include "State.h"

namespace UCal{
class StopState: public State{
public:
   void doAction() override{
      state_ = "stopState";
      // std::cout << "Player is in stop state\n";

   }//end method 

   std::string getState(){
       return state_;
   }//end method 



};//end class 

};//end namespace 

#endif 