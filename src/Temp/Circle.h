#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>
#include "Shape.h"
namespace UCal{
class Circle : public Shape{
    public:
    void draw() override{
        std::cout << "Print Circle \n";
    }//end method 
};//end class 

};//end namespace

#endif