#ifndef SQUARE_H
#define SQUARE_H

#include <iostream>
#include "Shape.h"
namespace UCal{
class Square : public Shape{
    public:
    void draw() override{
        std::cout << "Print Square \n";
    }//end method 
};//end class 

};//end namespace

#endif