#ifndef ShapeFactory_H
#define ShapeFactory_H


#include "Shape.h"
#include "Circle.h"
#include "Square.h"
#include <string>
#include <memory>

#include <iostream>
#include <vector>
#include <memory>
#include <cstdio>
#include <fstream>
#include <cassert>
#include <functional>

namespace UCal{

class ShapeFactory {
public:
   //use getShape method to get object of type shape 
   std::shared_ptr<Shape> getShape(std::string shapeType){
      if(shapeType.empty()){
         return nullptr;
      }		
      if(shapeType.compare("circle") == 0 ){
         auto circlePtr = std::make_shared<Circle>();
         return circlePtr;
    //   } else if(shapeType.compare("square") == 0){
    //       std::unique_ptr<Square> squarePtr = std::make_unique<Square>();
    //      return squarePtr;
      }
      
      return nullptr;
   }
};//end class
};//end namespace 

#endif