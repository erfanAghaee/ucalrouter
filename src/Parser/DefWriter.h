#ifndef DEFWRITER_H
#define DEFWRITER_H
#include "../Design/Design.h"
#include "../Library/Library.h"
#include <string>
#include <memory>
#include "defwWriter.hpp"


#define CHECK_STATUS(status) \
  if (status) {              \
     defwPrintError(status); \
     return(status);         \
  }

namespace UCal{

class DefWriter{
    std::shared_ptr<Design> design_;
public:
    DefWriter(std::string defWriteName, std::shared_ptr<Design> design){
        design_ = design;
        init(defWriteName);
    }//end method 

    int init(std::string defWriteName){
        FILE* fout;
        int status;


        if ((fout = fopen(defWriteName.c_str(), "w")) == 0) {

            return 2;
        } //end if

        // Group init
        status = defwInitCbk(fout);
        CHECK_STATUS(status);
        status = defwVersion (5, 8);
        CHECK_STATUS(status);
        status = defwDividerChar(design_->getDivideChar().c_str());
        CHECK_STATUS(status);
        status = defwBusBitChars(design_->getBusBitChar().c_str());
        CHECK_STATUS(status);
        status = defwDesignName(design_->getDesingName().c_str());
        CHECK_STATUS(status);
        status = defwUnits(design_->getUnit());
        CHECK_STATUS(status);
        // End group init

        // die area
        UCal::Rect rect = design_->getDieArea();
        status = defwDieArea(rect.getXl(), rect.getYl(), rect.getXh(), rect.getYh());
        CHECK_STATUS(status);

        //end die area

        // Row 
        for(auto rowItr : design_->getRows()){
            status = defwRowStr(rowItr.getName().c_str(), rowItr.getCoreSite().c_str(), rowItr.getRowX(), rowItr.getRowY(), 
                rowItr.getOrient().c_str(), rowItr.getNumX(), rowItr.getNumY(), rowItr.getStepX(), rowItr.getStepY());
            CHECK_STATUS(status);
        }//end for 

        //end Row

        // Track
        auto library = Library::getInstance();
        auto layers = library->getLayers();
        for(auto layer: layers){
            auto tracks = layer->getTracks();
            for(auto track : tracks){
                const char** layersStr = (const char**)malloc(sizeof(char*)*1);
                layersStr[0] = strdup(layer->getName().c_str());
                status = defwTracks(track.getMacro().c_str(), track.getX(), track.getNum(), track.getStep(), 1,layersStr);
                CHECK_STATUS(status);
                free((char*)layersStr[0]);
                free((char*)layersStr);
                status = defwNewLine();
                CHECK_STATUS(status);
            }//end for 
        }//end for  

        //end Track

        // GCELLGRID
        // status = defwGcellGrid("X", 0, 100, 600);
        // CHECK_STATUS(status);
        // End GCELLGRID
        
        // COMPONENTS
        auto components = design_->getComponents();

        status = defwStartComponents(components.size());
        CHECK_STATUS(status);
        for(auto component : components){
            status = defwComponentStr(component->getName().c_str(), 
                component->getMacroName().c_str(), 0, NULL, NULL, NULL, NULL, NULL,
                0, NULL, NULL, NULL, NULL, "PLACED", component->getPlacementX()
                , component->getPlacementY(),
                  component->getOrient().c_str(), 0, NULL, 0, 0, 0, 0);
            CHECK_STATUS(status);
        }//end for 
        


        status = defwEndComponents();

        // End COMPONENTS


        // NETS
        auto nets = design_->getNets();
        std::cout << "nets size: " << nets.size() << std::endl;
        for(auto net : nets){
            std::cout << "Net name: " << net->getName() << std::endl;
        }
        status = defwStartNets(nets.size());
        CHECK_STATUS(status);
        for(auto net : nets)
        {
            status = defwNet(net->getName().c_str());            
            CHECK_STATUS(status);
            std::cout <<"Net Name: " << net->getName().c_str() << std::endl;
            auto connections = net->getConnections();

            for(auto connection : connections){
                std::cout << connection.getInstance().c_str()<< " " <<connection.getPin().c_str() << std::endl;
                status = defwNetConnection(connection.getInstance().c_str(), connection.getPin().c_str(), 0);
                CHECK_STATUS(status);
            }//end for 
            status = defwNetEndOneNet();
            CHECK_STATUS(status);
            
        }//end for 


        status = defwEndNets();
        CHECK_STATUS(status);
        
        // end NET
        

        return 0;
    }//end method
};//end class 

};//end namespace 
#endif