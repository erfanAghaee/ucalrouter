#ifndef LefParserV2_H
#define LefParserV2_H



#include <string> 
#include <iostream>
#include "lefrReader.hpp"
#include "lefiUtil.hpp"
#include "lefiDebug.hpp"
#include "../Library/Library.h"




namespace UCal{
    
class LefParserV2{
    // std::string macroName_;
public:
    LefParserV2(std::string lefFileName){
        init(lefFileName);
    }
private:
    // inline static LefParserV2& getParser(lefiUserData ud)
    // {
    //     return *((LefParserV2*)ud);
    // }

    bool init(std::string lefFileName)
    {

        // Library* library = Library::getInstance();    
        
        // std::cout << "Library: " << library << std::endl;
        
        FILE* lefFile;
        char* userData;
        int nCall = 0;
        int relax = 0;


        lefrInit();
        lefrReset();
        lefrSetUserData((void*)this );

        // sets the parser to be case sensitive...
        // default was supposed to be the case but false...
        // lefrSetCaseSensitivity(true);
        
        // 
        if(nCall==0)  
        {
            lefrSetLayerCbk(layerCB);
            lefrSetUnitsCbk(unitsCB);
            lefrSetViaCbk(viaCB);
            lefrSetMacroBeginCbk(macroBeginCB);
            lefrSetMacroCbk(macroCB);
            lefrSetObstructionCbk(obstructionCB);
            lefrSetPinCbk(pinCB);

        }//end if 
        
        // lefrSetUnitsCbk(unitsCB); 
   
        (void) lefrSetShiftCase();  // will shift name to uppercase if caseinsensitive
                                    // is set to off or not set

        (void) lefrSetOpenLogFileAppend();




        if ((lefFile = fopen(lefFileName.c_str(),"r")) == 0) {
            std::cout << "Couldn't open input file \n" ;
            return false;
        }

         

        int res = lefrRead(lefFile, lefFileName.c_str(), (void*)userData);

        // std::cout << "Lef: " << lefFile << std::endl;

        
        // (void)lefrPrintUnusedCallbacks(m_outStream);
        (void)lefrReleaseNResetMemory();

        // lefrSetLayerCbk(layerCB);
    }//end method 

    static int unitsCB(lefrCallbackType_e c, lefiUnits* unit, lefiUserData) {
        checkType(c);
        
        if (unit->lefiUnits::hasDatabase()){
            Library* library = Library::getInstance(); 
            library->setDataBaseNumber(unit->lefiUnits::databaseNumber() );
        }//end if 
        return 0;
    }//end method


    static int layerCB(lefrCallbackType_e c, lefiLayer* layer, lefiUserData){
        

        lefiSpacingTable* spTable;
  
        lefiParallel* parallel;
        
        Library* library = Library::getInstance(); 
        
        if (layer->lefiLayer::hasType()){
           std::string layerTypestr = layer->lefiLayer::type();
           if( layerTypestr.compare("routing") == 0 || layerTypestr.compare("ROUTING") == 0 )
           {
            //    std::cout << "Routing Layer: ...\n";
               auto layerBldr = library->getLayerBuilder();
               layerBldr->setId(1);
               layerBldr->setName(layer->lefiLayer::name() );
               layerBldr->setType(layer->lefiLayer::type());
               layerBldr->setDirection(layer->lefiLayer::direction());
               layerBldr->setMinWidth(layer->lefiLayer::minwidth());
               layerBldr->setArea(layer->lefiLayer::area());

               // Read Spacing
               UCal::Spacing spacing; 

               if (layer->lefiLayer::hasSpacingNumber()){
                    for (int i = 0; i < layer->lefiLayer::numSpacing(); i++) {
                        UCal::SpEOL spEOL;
                        spEOL.setSpacing(layer->lefiLayer::spacing(i));
                         if (layer->lefiLayer::hasSpacingEndOfLine(i)) {
                             spEOL.setEndOfLine(layer->lefiLayer::spacingEolWidth(i));
                             spEOL.setWidthIn(layer->lefiLayer::spacingEolWithin(i));
                             if (layer->lefiLayer::hasSpacingParellelEdge(i)) {
                                    spEOL.setParallelSpace(layer->lefiLayer::spacingParSpace(i));
                                    spEOL.setParallelWidthIn(layer->lefiLayer::spacingParWithin(i));
                             }//end if
                         }//end if 
                         spacing.setSpEOL(spEOL);
                    }//end for 
               }//end if 

            //    for (int i = 0; i < layer->lefiLayer::numSpacingTable(); i++) {
            //        spTable = layer->lefiLayer::spacingTable(i);
            //        if (spTable->lefiSpacingTable::isParallel()){
            //             parallel = spTable->lefiSpacingTable::parallel();
            //             // for (j = 0; j < parallel->lefiParallel::numLength(); j++) {
            //             //     fprintf(fout, " %g", parallel->lefiParallel::length(j));
            //             // }//end if 
            //             for (int j = 0; j < parallel->lefiParallel::numWidth(); j++) {
            //                 UCal::SpTable spTable;
            //                 spTable.setWidth(parallel->lefiParallel::width(j));                                   
            //                 for (int k = 0; k < parallel->lefiParallel::numLength(); k++) {
            //                     spTable.setWidthSpacing(parallel->lefiParallel::widthSpacing(j, k));
            //                 }//end for 
            //                 spacing.setSpTable(spTable);
            //             }//end for 
            //        }//end for 
            //    }//end for 
              
               
               layerBldr->setSpacing(spacing);

               // End Read Spacing

               // read pitch
               if (layer->lefiLayer::hasXYPitch()){
                   std::pair<double,double> pitch;
                   pitch.first  = layer->lefiLayer::pitchX();
                   pitch.second = layer->lefiLayer::pitchY();
                   layerBldr->setPitch(pitch);
               }//end if 
                


               library->createLayer(layerBldr);
               layerBldr->setWidth(layer->lefiLayer::width());
               
           }else if(layerTypestr.compare("cut") == 0 || layerTypestr.compare("CUT") == 0 ){
            //    std::cout << "Cut Layer: ...\n";
               auto layerBldr = library->getLayerBuilder();
               layerBldr->setId(1);
               layerBldr->setName(layer->lefiLayer::name() );
               layerBldr->setType(layer->lefiLayer::type());

               // Read Spacing
               UCal::Spacing spacing; 

               if (layer->lefiLayer::hasSpacingNumber()){
                    for (int i = 0; i < layer->lefiLayer::numSpacing(); i++) {
                        UCal::SpEOL spEOL;
                        spEOL.setSpacing(layer->lefiLayer::spacing(i));
                         if (layer->lefiLayer::hasSpacingEndOfLine(i)) {
                             spEOL.setEndOfLine(layer->lefiLayer::spacingEolWidth(i));
                             spEOL.setWidthIn(layer->lefiLayer::spacingEolWithin(i));
                         }//end if 
                         spacing.setSpEOL(spEOL);
                    }//end for 
               }//end if 

            //    for (int i = 0; i < layer->lefiLayer::numSpacingTable(); i++) {
            //        spTable = layer->lefiLayer::spacingTable(i);
            //        if (spTable->lefiSpacingTable::isParallel()){
            //             parallel = spTable->lefiSpacingTable::parallel();
            //             for (int j = 0; j < parallel->lefiParallel::numWidth(); j++) {
            //                 UCal::SpTable spTable;
            //                 spTable.setWidth(parallel->lefiParallel::width(j));                                   
            //                 for (int k = 0; k < parallel->lefiParallel::numLength(); k++) {
            //                     spTable.setWidthSpacing(parallel->lefiParallel::widthSpacing(j, k));
            //                 }//end for 
            //                 spacing.setSpTable(spTable);
            //             }//end for 
            //        }//end for 
            //    }//end for 
                layerBldr->setSpacing(spacing);

               // End Read Spacing


               layerBldr->setWidth(layer->lefiLayer::width());
               library->createLayer(layerBldr);
            //    layerBldr->setWidth(-1);

           }
        }else{
            std::cout << "Unkown Layer!!\n";
        }

        return 0;
    }//end method 

    static int viaCB(lefrCallbackType_e c, lefiVia* via, lefiUserData) {
        checkType(c);        
        lefVia(via);
        return 0;
    }//end method 

    static void lefVia(lefiVia *via){
        Library* library = Library::getInstance(); 
        auto viaBldr = library->getViaBuilder();
        viaBldr->setId(1);
        viaBldr->setName(via->lefiVia::name());
        
        if (via->lefiVia::hasDefault())
            viaBldr->setDefault();
        else if (via->lefiVia::hasGenerated())
            viaBldr->setGenerated();

        if (via->lefiVia::numLayers() > 0) {
            for (int i = 0; i < via->lefiVia::numLayers(); i++) {
                auto layer = library->getLayer(via->lefiVia::layerName(i));
                 for (int j = 0; j < via->lefiVia::numRects(i); j++){
                     UCal::Rect rect;
                     rect.setRect(
                         via->lefiVia::xl(i, j),
                         via->lefiVia::yl(i, j),
                         via->lefiVia::xh(i, j),
                         via->lefiVia::yh(i, j));
                    viaBldr->setVia(layer,rect);
                 }//end for 
                
            }//end for 
        }//end if

        library->createVia(viaBldr);

    }//end method 

    static void checkType(lefrCallbackType_e c) {
        if (c >= 0 && c <= lefrLibraryEndCbkType) {
            // OK
        } else {
            std::cout<< "ERROR: callback type is out of bounds!\n";
        }//end if
    }//end method 

    static int macroCB(lefrCallbackType_e c, lefiMacro* macro, lefiUserData ud) {
        checkType(c);
        Library* library = Library::getInstance(); 
        auto macroPtr = library->getMacro(library->getMacroNameCurrent());

        // macroPtr->setId(1);
        // macroBldr->setName(macro->lefiMacro::name());

        // LefParserV2& parser = LefParserV2::getParser(ud);
        // parser.macroName_ = macro->lefiMacro::name();
        // library->setMacroNameCurrent(macro->lefiMacro::name());
        // std::cout << "MacroName(MacroCB): " << macro->lefiMacro::name() << std::endl ;
        // read class 
        if (macro->lefiMacro::hasClass())
            macroPtr->setClass(macro->lefiMacro::macroClass());
        
        // read foreign
        if (macro->lefiMacro::hasForeign()) {
            
            for (int i = 0; i < macro->lefiMacro::numForeigns(); i++) {
                UCal::Foreign foreign;
                // std::cout << "Foreign: " << macro->lefiMacro::foreignName(i) << std::endl;
                foreign.setName(macro->lefiMacro::foreignName(i));
                if (macro->lefiMacro::hasForeignPoint(i)) {
                    std::pair<double, double> pointXY;
                    pointXY.first  = macro->lefiMacro::foreignX(i);
                    pointXY.second = macro->lefiMacro::foreignY(i);
                    foreign.setPointXY(pointXY);                           
                }//end if 
                 macroPtr->setForeign(foreign);
            }//end for 
        }//end if

        // read origin
        if (macro->lefiMacro::hasOrigin())
        {
            macroPtr->setOriginX(macro->lefiMacro::originX());
            macroPtr->setOriginY(macro->lefiMacro::originY());
        }//end if 

        // read size
        if (macro->lefiMacro::hasSize()){
            macroPtr->setSizeX(macro->lefiMacro::sizeX());
            macroPtr->setSizeY(macro->lefiMacro::sizeY());
        }//end if

        // read symmetry 
        std::string symmetry = "-1";
        if (macro->lefiMacro::hasXSymmetry()){
            symmetry = "X";
        }//end if 
        if (macro->lefiMacro::hasYSymmetry()){
            if(symmetry.compare("-1") == 0)
                symmetry = "Y";
            else
                symmetry = symmetry + "Y";
        }//end if 
        if (macro->lefiMacro::has90Symmetry()){
            if(symmetry.compare("-1") == 0)
                symmetry = "R90";
            else
                symmetry = symmetry + "R90";
        }//end if
        macroPtr->setSymmetry(symmetry);

        // read site
        if (macro->lefiMacro::hasSiteName())
            macroPtr->setSite(macro->lefiMacro::siteName());

        


        return 0;
    }//end method 

    static int pinCB(lefrCallbackType_e c, lefiPin* pin, lefiUserData ud) {

        Library* library = Library::getInstance(); 
        std::shared_ptr<Macro> macroPtr = library->getMacro(library->getMacroNameCurrent());
        auto pinBldr = macroPtr->getPinBuilder();
        
        pinBldr->setName(pin->lefiPin::name());


        if (pin->lefiPin::hasDirection())
            pinBldr->setDirection(pin->lefiPin::direction());
        if (pin->lefiPin::hasUse())
            pinBldr->setUse(pin->lefiPin::use());
            
        int numPorts = pin->lefiPin::numPorts();
        lefiGeometries* geometry;
        for (int i = 0; i < numPorts; i++) {
            geometry = pin->lefiPin::port(i);
            prtGeometry(geometry,pinBldr);
        }//end for 

        macroPtr->createPin(pinBldr);

        return 0;
    }//end method 

    static int macroBeginCB(lefrCallbackType_e c, const char* macroName, lefiUserData) {
        checkType(c);
        Library* library = Library::getInstance(); 
        library->setMacroNameCurrent(macroName);
        auto macroBldr = library->getMacroBuilder();

        macroBldr->setId(1);
        macroBldr->setName(macroName);
        library->createMacro(macroBldr);
        return 0;
    }//end method 

    static void prtGeometry(lefiGeometries *geometry,std::shared_ptr<Pin> pinPtr){
        int                 numItems = geometry->lefiGeometries::numItems();
        Library* library = Library::getInstance();
        lefiGeomRect *rect;
        std::string layerName = "-1";
        for (int i = 0; i < numItems; i++) {
                switch (geometry->lefiGeometries::itemType(i)) {
                    case lefiGeomLayerE:
                    {
                        layerName = geometry->lefiGeometries::getLayer(i);
                        break;
                    }//end case lefiGeomLayerE
                    
                    case lefiGeomRectE:
                    {
                        rect = geometry->lefiGeometries::getRect(i);
                        if (rect->colorMask != 0) {
                           //do nothing
                        } else {
                            UCal::Geo geometry;
                            geometry.setLayer(layerName);
                            geometry.setRect(rect->xl, rect->yl, rect->xh, rect->yh);
                            pinPtr->setPort(geometry); 
                        }//end if
                        break;
                    }//end case lefiGeomRectE


                }//end switch
        }//end for 

    }//end method 


    static void prtGeometry(lefiGeometries *geometry,std::shared_ptr<OBS> obsPtr){
        int                 numItems = geometry->lefiGeometries::numItems();
        Library* library = Library::getInstance();
        lefiGeomRect *rect;
        std::string layerName = "-1";
        for (int i = 0; i < numItems; i++) {
                switch (geometry->lefiGeometries::itemType(i)) {
                    case lefiGeomLayerE:
                    {
                        layerName = geometry->lefiGeometries::getLayer(i);
                        break;
                    }//end case lefiGeomLayerE
                    
                    case lefiGeomRectE:
                    {
                        rect = geometry->lefiGeometries::getRect(i);
                        if (rect->colorMask != 0) {
                           //do nothing
                        } else {
                            UCal::Geo geometry;
                            geometry.setLayer(layerName);
                            geometry.setRect(rect->xl, rect->yl, rect->xh, rect->yh);
                            obsPtr->setOBS(geometry); 
                        }//end if
                        break;
                    }//end case lefiGeomRectE


                }//end switch
        }//end for 

    }//end method 
  

    static int obstructionCB(lefrCallbackType_e c, lefiObstruction* obs,
                  lefiUserData) {
        lefiGeometries* geometry;

        Library* library = Library::getInstance(); 
        std::shared_ptr<Macro> macroPtr = library->getMacro(library->getMacroNameCurrent());
        auto obsBldr = macroPtr->getOBSBuilder();


        checkType(c);
        // if ((long)ud != userData) dataError();
        obsBldr->setName("OBS");        
        geometry = obs->lefiObstruction::geometries();
        prtGeometry(geometry,obsBldr);

         macroPtr->createOBS(obsBldr);
        // fprintf(fout, "  END\n");
        return 0;
    }//end method


};//end class 

};//end namespace 

#endif