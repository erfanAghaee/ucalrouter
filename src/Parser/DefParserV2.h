#ifndef DefParserV2_H
#define DefParserV2_H
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#ifndef WIN32
#   include <unistd.h>
#endif /* not WIN32 */
#include "defrReader.hpp"
#include "defiAlias.hpp"

#include "../Design/Design.h"
#include "../Library/Library.h"




namespace UCal{

// class Design{}   

class DefParserV2{
    std::shared_ptr<Design> design_;
public:
    DefParserV2(std::string defFileName, std::shared_ptr<Design> design){
        design_ = design;
        init(defFileName);
    }
private:
    bool init(std::string defFileName)
    {
        FILE* defFile;
        char* userData;
        int nCall = 0;

        if(nCall == 0){
            defrSetDividerCbk(dn);
            defrSetBusBitCbk(bbn);
            defrSetDesignCbk(dname);
            defrSetUnitsCbk(units);

            defrSetVersionStrCbk(versStr);

            defrSetDieAreaCbk((defrBoxCbkFnType)cls);
            defrSetRowCbk((defrRowCbkFnType)cls);
            defrSetTrackCbk((defrTrackCbkFnType)cls);
            defrSetComponentCbk(compf);

            defrSetNetCbk(netf);

            defrSetNetNameCbk(netNamef);
        }//end if 

        (void) defrSetOpenLogFileAppend();

        defrInit();


        defrReset();
        if ((defFile = fopen(defFileName.c_str(),"r")) == 0) 
        {
            std::cout << "Couldn't open input file  " << defFileName << std::endl;
            return(2);
        }
        // Set case sensitive to 0 to start with, in History & PropertyDefinition
        // reset it to 1.
        int res = defrRead(defFile, defFileName.c_str(), (void*) this, 1);

        if (res)
        {
            std::cout << "DEF Reader returns bad status; result is " << res << "\n";
        }
        else 
        {
            std::cout << "DEF Reader done; result is " << res << std::endl;
        }

        // (void)defrPrintUnusedCallbacks(m_outStream);
        (void)defrReleaseNResetMemory();
        (void)defrUnsetCallbacks();

        return res;
    }//end method 

    static int versStr(defrCallbackType_e c, const char* versionName, defiUserData ud) {
        checkType(c);
        
        std::shared_ptr<Design> designPtr = getDesign(ud);

        designPtr->setVersion(versionName);
        return 0;
    }//end method 

    static void checkType(defrCallbackType_e c) {
        if (c >= 0 && c <= defrDesignEndCbkType) {
            // OK
        } else {
            std::cout<< "ERROR: callback type is out of bounds!\n";
        }
    }//end method 

    static int dn(defrCallbackType_e c, const char* h, defiUserData ud) {
        checkType(c);
        std::shared_ptr<Design> designPtr = getDesign(ud);

        designPtr->setDivideChar(h);
        return 0;
    }//end method 


    static int bbn(defrCallbackType_e c, const char* h, defiUserData ud) {
        checkType(c);
        std::shared_ptr<Design> designPtr = getDesign(ud);
        designPtr->setBusBitChar(h);
        
        return 0;
    }//end method

    static int dname(defrCallbackType_e c, const char* string, defiUserData ud) {
        checkType(c);
        std::shared_ptr<Design> designPtr = getDesign(ud);
        designPtr->setDesingName(string);

        return 0;
    }//end method 

    static int units(defrCallbackType_e c, double d, defiUserData ud) {
        checkType(c);
        std::shared_ptr<Design> designPtr = getDesign(ud);
        designPtr->setUnit(d);      
        return 0;
    }//end method

    static char* orientStr(int orient) {
        switch (orient) {
            case 0: return ((char*)"N");
            case 1: return ((char*)"W");
            case 2: return ((char*)"S");
            case 3: return ((char*)"E");
            case 4: return ((char*)"FN");
            case 5: return ((char*)"FW");
            case 6: return ((char*)"FS");
            case 7: return ((char*)"FE");
        };
        return ((char*)"BOGUS");
    } //end method 


     static int cls(defrCallbackType_e c, void* cl, defiUserData ud) {
        defiBox* box;  // DieArea and 
        defiRow* row;
        defiTrack* track;
        checkType(c);
        std::shared_ptr<Design> designPtr = getDesign(ud);
        
  
        switch (c) {
            case defrRowCbkType :
            {
                row = (defiRow*)cl;
                Row rowBldr;
                rowBldr.setId(1);
                rowBldr.setName( row->name());
                rowBldr.setCoreSite( row->macro());                

                rowBldr.setRowX(row->x());
                rowBldr.setRowY(row->y());
                rowBldr.setOrient(orientStr(row->orient()));

                if (row->hasDo())
                {
                    rowBldr.setNumX(row->xNum());
                    rowBldr.setNumY(row->yNum());
                    if (row->hasDoStep())                        
                    {
                       rowBldr.setStepX(row->xStep());
                       rowBldr.setStepY(row->yStep());                    
                    }//end if
                        
                    
                        
                }//end if 
                designPtr->setRow(rowBldr);
                
                break;
            }//end case defrRowCbkType
           case defrDieAreaCbkType :
            {
                box = (defiBox*)cl;
                UCal::Rect rect; 
                rect.setRect(box->xl(), box->yl(), box->xh(),box->yh());
                designPtr->setDieArea(rect);
                break;
            }//end case defrDieAreaCbkType
            case defrTrackCbkType :
            {
                track = (defiTrack*)cl;
                auto library = Library::getInstance();
                for (int i = 0; i < track->numLayers(); i++){
                    auto layerPtr = library->getLayer(track->layer(i));
                    Track trackBldr;
                    trackBldr.setMacro(track->macro());
                    trackBldr.setX(track->x());
                    trackBldr.setNum(track->xNum());
                    trackBldr.setStep(track->xStep());
                    layerPtr->setTrack(trackBldr);
                }//end for 
                break;
            }//end case defrTrackCbkType

        }//end switch
        return 0;
    }//end method 


    static int compf(defrCallbackType_e c, defiComponent* co, defiUserData ud) {
        auto design = getDesign(ud);
        std::shared_ptr<Component> componentBldr = design->getComponentBuilder();
        componentBldr->setId(1);
        componentBldr->setName(co->id());
        componentBldr->setMacroName(co->name());
        if (co->isPlaced()) {
            componentBldr->setPlacementX(co->placementX());
            componentBldr->setPlacementY(co->placementY());
            componentBldr->setOrient(orientStr(co->placementOrient()));
        }//end if 


        design->createComponent(componentBldr);
        
        return 0;
    }//end method 

    static int netNamef(defrCallbackType_e c, const char* netName, defiUserData ud) {
        checkType(c);
        auto design = getDesign(ud);
        // std::cout << "Net Name0: " << netName << std::endl;
        auto netBldr = design->getNetBuilder();
        netBldr->setId(1);
        netBldr->setName(netName);
        design->setNetCurrentName(netName);
        design->createNet(netBldr);
        return 0;
    }//end method 

    static int netf(defrCallbackType_e c, defiNet* net, defiUserData ud) {
        auto design = getDesign(ud);
        for (int i = 0; i < net->numConnections(); i++) {
            auto netPtr = design->getNet(design->getNetCurrentName());
            Connection connection;
            connection.setInstance(net->instance(i));   
            connection.setPin(net->pin(i));
            netPtr->setConnection(connection);
        }//end for 

        auto netTmp = design->getNets();
        // std::cout << "netTmp size: " << netTmp.size() << std::endl;
        return 0;
    }//end method 


    inline static DefParserV2* getParser(defiUserData ud)
    {
        return (DefParserV2*)ud;
    }//end method 
    
    inline static std::shared_ptr<Design> getDesign(defiUserData ud)
    {
        return getParser(ud)->design_;
    }//end method 

   

};//end class 

};//end namespace 
#endif