#ifndef DefParser_H
#define DefParser_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#ifndef WIN32
#   include <unistd.h>
#endif /* not WIN32 */
#include "defrReader.hpp"
#include "defiAlias.hpp"


char defaultNameDef[64];
char defaultOutDef[64];

// Global variables
FILE* foutDef;
void* userDataDef;
int numObjs;
int isSumSet;      // to keep track if within SUM
int isProp = 0;    // for PROPERTYDEFINITIONS
int begOperand;    // to keep track for constraint, to print - as the 1st char
static double curVer = 0;
static int setSNetWireCbk = 0;
static int isSessionless = 0;
static int ignoreRowNames = 0;
static int ignoreViaNames = 0;
static int testDebugPrint = 0;  // test for ccr1488696
static int ccr1131444 = 0;

namespace UCal{

class DefParser{

public:
    DefParser(int argc, char** argv){
        
        init(argc,argv);
    }
private:
    // static void initParams(){

    // }//end method 

// -------------------------------------------------------------------
//                 Def Parser Methods                                |
// -------------------------------------------------------------------



static void myLogFunction(const char* errMsg){
   fprintf(foutDef, "ERROR: found error: %s\n", errMsg);
}

static void myWarningLogFunction(const char* errMsg){
   fprintf(foutDef, "WARNING: found error: %s\n", errMsg);
}

static void dataError() {
  fprintf(foutDef, "ERROR: returned user data is not correct!\n");
}

static void checkType(defrCallbackType_e c) {
  if (c >= 0 && c <= defrDesignEndCbkType) {
    // OK
  } else {
    fprintf(foutDef, "ERROR: callback type is out of bounds!\n");
  }
}


static int done(defrCallbackType_e c, void*, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "END DESIGN\n");
  return 0;
}

static int endfunc(defrCallbackType_e c, void*, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  return 0;
}


static char* orientStr(int orient) {
  switch (orient) {
      case 0: return ((char*)"N");
      case 1: return ((char*)"W");
      case 2: return ((char*)"S");
      case 3: return ((char*)"E");
      case 4: return ((char*)"FN");
      case 5: return ((char*)"FW");
      case 6: return ((char*)"FS");
      case 7: return ((char*)"FE");
  };
  return ((char*)"BOGUS");
}

static int compMSL(defrCallbackType_e c, defiComponentMaskShiftLayer* co, defiUserData ud) {
  int i;

  checkType(c);
  if (ud != userDataDef) dataError();

    if (co->numMaskShiftLayers()) {
	fprintf(foutDef, "\nCOMPONENTMASKSHIFT ");
	
	for (i = 0; i < co->numMaskShiftLayers(); i++) {
           fprintf(foutDef, "%s ", co->maskShiftLayer(i));
	}
	fprintf(foutDef, ";\n");
    }

  return 0;
}

static int compf(defrCallbackType_e c, defiComponent* co, defiUserData ud) {
  if (testDebugPrint) {
      co->print(foutDef);
  } else {
      int i;

      checkType(c);
      if (ud != userDataDef) dataError();
    //  missing GENERATE, FOREIGN
        fprintf(foutDef, "- %s %s ", co->id(),
                co->name());
    //    co->changeIdAndName("idName", "modelName");
    //    fprintf(foutDef, "%s %s ", co->id(),
    //            co->name());
        if (co->hasNets()) {
            for (i = 0; i < co->numNets(); i++)
                 fprintf(foutDef, "%s ", co->net(i));
        }
        if (co->isFixed()) 
            fprintf(foutDef, "+ FIXED %d %d %s ",
                    co->placementX(),
                    co->placementY(),
                    //orientStr(co->placementOrient()));
                    co->placementOrientStr());
        if (co->isCover()) 
            fprintf(foutDef, "+ COVER %d %d %s ",
                    co->placementX(),
                    co->placementY(),
                    orientStr(co->placementOrient()));
        if (co->isPlaced()) 
            fprintf(foutDef,"+ PLACED %d %d %s ",
                    co->placementX(),
                    co->placementY(),
                    orientStr(co->placementOrient()));
        if (co->isUnplaced()) {
            fprintf(foutDef,"+ UNPLACED ");
            if ((co->placementX() != -1) ||
                (co->placementY() != -1))
               fprintf(foutDef,"%d %d %s ",
                       co->placementX(),
                       co->placementY(),
                       orientStr(co->placementOrient()));
        }
        if (co->hasSource())
            fprintf(foutDef, "+ SOURCE %s ", co->source());
        if (co->hasGenerate()) {
            fprintf(foutDef, "+ GENERATE %s ", co->generateName());
            if (co->macroName() &&
                *(co->macroName()))
               fprintf(foutDef, "%s ", co->macroName());
        }
        if (co->hasWeight())
            fprintf(foutDef, "+ WEIGHT %d ", co->weight());
        if (co->hasEEQ())
            fprintf(foutDef, "+ EEQMASTER %s ", co->EEQ());
        if (co->hasRegionName())
            fprintf(foutDef, "+ REGION %s ", co->regionName());
        if (co->hasRegionBounds()) {
            int *xl, *yl, *xh, *yh;
            int size;
            co->regionBounds(&size, &xl, &yl, &xh, &yh);
            for (i = 0; i < size; i++) { 
                fprintf(foutDef, "+ REGION %d %d %d %d \n",
                        xl[i], yl[i], xh[i], yh[i]);
            }
        }
        if (co->maskShiftSize()) {
            fprintf(foutDef, "+ MASKSHIFT ");

            for (int i = co->maskShiftSize()-1; i >= 0; i--) {
                fprintf(foutDef, "%d", co->maskShift(i));
            }
            fprintf(foutDef, "\n");
        }
        if (co->hasHalo()) {
            int left, bottom, right, top;
            (void) co->haloEdges(&left, &bottom, &right, &top);
            fprintf(foutDef, "+ HALO ");
            if (co->hasHaloSoft())
               fprintf(foutDef, "SOFT ");
            fprintf(foutDef, "%d %d %d %d\n", left, bottom, right, top);
        }
        if (co->hasRouteHalo()) {
            fprintf(foutDef, "+ ROUTEHALO %d %s %s\n", co->haloDist(),
                    co->minLayer(), co->maxLayer());
        }
        if (co->hasForeignName()) {
            fprintf(foutDef, "+ FOREIGN %s %d %d %s %d ",
                    co->foreignName(), co->foreignX(),
                    co->foreignY(), co->foreignOri(),
                    co->foreignOrient());
        }
        if (co->numProps()) {
            for (i = 0; i < co->numProps(); i++) {
                fprintf(foutDef, "+ PROPERTY %s %s ", co->propName(i),
                        co->propValue(i));
                switch (co->propType(i)) {
                   case 'R': fprintf(foutDef, "REAL ");
                             break;
                   case 'I': fprintf(foutDef, "INTEGER ");
                             break;
                   case 'S': fprintf(foutDef, "STRING ");
                             break;
                   case 'Q': fprintf(foutDef, "QUOTESTRING ");
                             break;
                   case 'N': fprintf(foutDef, "NUMBER ");
                             break;
                }
            }
        }
        fprintf(foutDef, ";\n");
        --numObjs;
        if (numObjs <= 0)
            fprintf(foutDef, "END COMPONENTS\n");
    }

    return 0;
}


static int netpath(defrCallbackType_e, defiNet*, defiUserData) {
  fprintf(foutDef, "\n");

  fprintf (foutDef, "Callback of partial path for net\n");

  return 0;
}


static int netNamef(defrCallbackType_e c, const char* netName, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
    fprintf(foutDef, "- %s ", netName);
  return 0;
}

static int subnetNamef(defrCallbackType_e c, const char* subnetName, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
    if (curVer >= 5.6)
      fprintf(foutDef, "   + SUBNET CBK %s ", subnetName);
  return 0;
}

static int nondefRulef(defrCallbackType_e c, const char* ruleName, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
    if (curVer >= 5.6)
      fprintf(foutDef, "   + NONDEFAULTRULE CBK %s ", ruleName);
  return 0;
}

static int netf(defrCallbackType_e c, defiNet* net, defiUserData ud) {
  // For net and special net.
  int        i, j, k, w, x, y, z, count, newLayer;
  defiPath*  p;
  defiSubnet *s;
  int        path;
  defiVpin   *vpin;
  // defiShield *noShield;
  defiWire   *wire;

  checkType(c);
  if (ud != userDataDef) dataError();
  if (c != defrNetCbkType)
      fprintf(foutDef, "BOGUS NET TYPE  ");
  if (net->pinIsMustJoin(0))
      fprintf(foutDef, "- MUSTJOIN ");
// 5/6/2004 - don't need since I have a callback for the name
//  else
//      fprintf(foutDef, "- %s ", net->name());
 
//  net->changeNetName("newNetName");
//  fprintf(foutDef, "%s ", net->name());
  count = 0;
  // compName & pinName
  for (i = 0; i < net->numConnections(); i++) {
      // set the limit of only 5 items per line
      count++;
      if (count >= 5) {
          fprintf(foutDef, "\n");
          count = 0;
      }
      fprintf(foutDef, "( %s %s ) ", net->instance(i),
              net->pin(i));
//      net->changeInstance("newInstance", i);
//      net->changePin("newPin", i);
//      fprintf(foutDef, "( %s %s ) ", net->instance(i),
//              net->pin(i));
      if (net->pinIsSynthesized(i))
          fprintf(foutDef, "+ SYNTHESIZED ");
  }

  if (net->hasNonDefaultRule())
      fprintf(foutDef, "+ NONDEFAULTRULE %s\n", net->nonDefaultRule());

  for (i = 0; i < net->numVpins(); i++) {
      vpin = net->vpin(i);
      fprintf(foutDef, "  + %s", vpin->name());
      if (vpin->layer()) 
          fprintf(foutDef, " %s", vpin->layer());
      fprintf(foutDef, " %d %d %d %d", vpin->xl(), vpin->yl(), vpin->xh(),
              vpin->yh());
      if (vpin->status() != ' ') {
          fprintf(foutDef, " %c", vpin->status());
          fprintf(foutDef, " %d %d", vpin->xLoc(), vpin->yLoc());
          if (vpin->orient() != -1)
              fprintf(foutDef, " %s", orientStr(vpin->orient()));
      }
      fprintf(foutDef, "\n");
  }

  // regularWiring
  if (net->numWires()) {
     for (i = 0; i < net->numWires(); i++) {
        newLayer = 0;
        wire = net->wire(i);
        fprintf(foutDef, "\n  + %s ", wire->wireType());
        count = 0;
        for (j = 0; j < wire->numPaths(); j++) {
           p = wire->path(j);
           p->initTraverse();
           while ((path = (int)p->next()) != DEFIPATH_DONE) {
              count++;
              // Don't want the line to be too long
              if (count >= 5) {
                  fprintf(foutDef, "\n");
                  count = 0;
              } 
              switch (path) {
                case DEFIPATH_LAYER:
                     if (newLayer == 0) {
                         fprintf(foutDef, "%s ", p->getLayer());
                         newLayer = 1;
                     } else
                         fprintf(foutDef, "NEW %s ", p->getLayer());
                     break;
		case DEFIPATH_MASK:
		     fprintf(foutDef, "MASK %d ", p->getMask());
                     break;
                case DEFIPATH_VIAMASK:
                     fprintf(foutDef, "MASK %d%d%d ", 
                             p->getViaTopMask(), 
                             p->getViaCutMask(),
                             p->getViaBottomMask());
                     break;
                case DEFIPATH_VIA:
                     fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                     break;
                case DEFIPATH_VIAROTATION:
                     fprintf(foutDef, "%s ", 
                             orientStr(p->getViaRotation()));
                     break;
		case DEFIPATH_RECT:
		     p->getViaRect(&w, &x, &y, &z);
                     fprintf(foutDef, "RECT ( %d %d %d %d ) ", w, x, y, z);
                     break;
		case DEFIPATH_VIRTUALPOINT:
		     p->getVirtualPoint(&x, &y);
		     fprintf(foutDef, "VIRTUAL ( %d %d ) ", x, y);
                     break;
                case DEFIPATH_WIDTH:
                     fprintf(foutDef, "%d ", p->getWidth());
                     break;
                case DEFIPATH_POINT:
                     p->getPoint(&x, &y);
                     fprintf(foutDef, "( %d %d ) ", x, y);
                     break;
                case DEFIPATH_FLUSHPOINT:
                     p->getFlushPoint(&x, &y, &z);
                     fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                     break;
                case DEFIPATH_TAPER:
                     fprintf(foutDef, "TAPER ");
                     break;
                case DEFIPATH_TAPERRULE:
                     fprintf(foutDef, "TAPERRULE %s ",p->getTaperRule());
                     break;
                case DEFIPATH_STYLE:
                     fprintf(foutDef, "STYLE %d ",p->getStyle());
                     break;
              }
           }
        }
        fprintf(foutDef, "\n");
        count = 0;
     }
  }

  // SHIELDNET
  if (net->numShieldNets()) {
     for (i = 0; i < net->numShieldNets(); i++) 
         fprintf(foutDef, "\n  + SHIELDNET %s", net->shieldNet(i));
  }
/* obsolete in 5.4
  if (net->numNoShields()) {
     for (i = 0; i < net->numNoShields(); i++) { 
         noShield = net->noShield(i); 
         fprintf(foutDef, "\n  + NOSHIELD ");
         newLayer = 0;
         for (j = 0; j < noShield->numPaths(); j++) {
            p = noShield->path(j);
            p->initTraverse();
            while ((path = (int)p->next()) != DEFIPATH_DONE) {
               count++;
               // Don't want the line to be too long
               if (count >= 5) {
                   fprintf(foutDef, "\n");
                   count = 0;
               }
               switch (path) {
                 case DEFIPATH_LAYER:
                      if (newLayer == 0) {
                          fprintf(foutDef, "%s ", p->getLayer());
                          newLayer = 1;
                      } else
                          fprintf(foutDef, "NEW %s ", p->getLayer());
                      break;
                 case DEFIPATH_VIA:
                      fprintf(foutDef, "%s ", p->getVia());
                      break;
                 case DEFIPATH_VIAROTATION:
                      fprintf(foutDef, "%s ", 
                             orientStr(p->getViaRotation()));
                      break;
                 case DEFIPATH_WIDTH:
                      fprintf(foutDef, "%d ", p->getWidth());
                      break;
                 case DEFIPATH_POINT:
                      p->getPoint(&x, &y);
                      fprintf(foutDef, "( %d %d ) ", x, y);
                      break;
                 case DEFIPATH_FLUSHPOINT:
                      p->getFlushPoint(&x, &y, &z);
                      fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                      break;
                 case DEFIPATH_TAPER:
                      fprintf(foutDef, "TAPER ");
                      break;
                 case DEFIPATH_TAPERRULE:
                      fprintf(foutDef, "TAPERRULE %s ",
                              p->getTaperRule());
                      break;
               }
            }
         }
     }
  }
*/

  if (net->hasSubnets()) {
     for (i = 0; i < net->numSubnets(); i++) {
        s = net->subnet(i);
        fprintf(foutDef, "\n");
 
        if (s->numConnections()) {
           if (s->pinIsMustJoin(0))
              fprintf(foutDef, "- MUSTJOIN ");
           else
              fprintf(foutDef, "  + SUBNET %s ", s->name());
           for (j = 0; j < s->numConnections(); j++)
              fprintf(foutDef, " ( %s %s )\n", s->instance(j),
                      s->pin(j));

           // regularWiring
           if (s->numWires()) {
              for (k = 0; k < s->numWires(); k++) {
                 newLayer = 0;
                 wire = s->wire(k);
                 fprintf(foutDef, "  %s ", wire->wireType());
                 count = 0;
                 for (j = 0; j < wire->numPaths(); j++) {
                    p = wire->path(j);
                    p->initTraverse();
                    while ((path = (int)p->next()) != DEFIPATH_DONE) {
                       count++;
                       // Don't want the line to be too long
                       if (count >= 5) {
                           fprintf(foutDef, "\n");
                           count = 0;
                       } 
                       switch (path) {
                         case DEFIPATH_LAYER:
                              if (newLayer == 0) {
                                  fprintf(foutDef, "%s ", p->getLayer());
                                  newLayer = 1;
                              } else
                                  fprintf(foutDef, "NEW %s ",
                                          p->getLayer());
                              break;
                         case DEFIPATH_VIA:
                              fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                              break;
                         case DEFIPATH_VIAROTATION:
                              fprintf(foutDef, "%s ",
                                      p->getViaRotationStr());
                              break;
                         case DEFIPATH_WIDTH:
                              fprintf(foutDef, "%d ", p->getWidth());
                              break;
                         case DEFIPATH_POINT:
                              p->getPoint(&x, &y);
                              fprintf(foutDef, "( %d %d ) ", x, y);
                              break;
                         case DEFIPATH_FLUSHPOINT:
                              p->getFlushPoint(&x, &y, &z);
                              fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                              break;
                         case DEFIPATH_TAPER:
                              fprintf(foutDef, "TAPER ");
                              break;
                         case DEFIPATH_TAPERRULE:
                              fprintf(foutDef, "TAPERRULE  %s ",
                                      p->getTaperRule());
                              break;
                         case DEFIPATH_STYLE:
                              fprintf(foutDef, "STYLE  %d ",
                                      p->getStyle());
                              break;
                       }
                    }
                 }
              }
           }
         }
      }
   }

  if (net->numProps()) {
    for (i = 0; i < net->numProps(); i++) {
        fprintf(foutDef, "  + PROPERTY %s ", net->propName(i));
        switch (net->propType(i)) {
           case 'R': fprintf(foutDef, "%g REAL ", net->propNumber(i));
                     break;
           case 'I': fprintf(foutDef, "%g INTEGER ", net->propNumber(i));
                     break;
           case 'S': fprintf(foutDef, "%s STRING ", net->propValue(i));
                     break;
           case 'Q': fprintf(foutDef, "%s QUOTESTRING ", net->propValue(i));
                     break;
           case 'N': fprintf(foutDef, "%g NUMBER ", net->propNumber(i));
                     break;
        }
        fprintf(foutDef, "\n");
    }
  }

  if (net->hasWeight())
    fprintf(foutDef, "+ WEIGHT %d ", net->weight());
  if (net->hasCap())
    fprintf(foutDef, "+ ESTCAP %g ", net->cap());
  if (net->hasSource())
    fprintf(foutDef, "+ SOURCE %s ", net->source());
  if (net->hasFixedbump())
    fprintf(foutDef, "+ FIXEDBUMP ");
  if (net->hasFrequency())
    fprintf(foutDef, "+ FREQUENCY %g ", net->frequency());
  if (net->hasPattern())
    fprintf(foutDef, "+ PATTERN %s ", net->pattern());
  if (net->hasOriginal())
    fprintf(foutDef, "+ ORIGINAL %s ", net->original());
  if (net->hasUse())
    fprintf(foutDef, "+ USE %s ", net->use());

  fprintf (foutDef, ";\n");
  --numObjs;
  if (numObjs <= 0)
      fprintf(foutDef, "END NETS\n");
  return 0;
}


static int snetpath(defrCallbackType_e c, defiNet* ppath, defiUserData ud) {
  int         i, j, x, y, z, count, newLayer;
  char*       layerName;
  double      dist, left, right;
  defiPath*   p;
  defiSubnet  *s;
  int         path;
  defiShield* shield;
  defiWire*   wire;
  int         numX, numY, stepX, stepY;

  if (c != defrSNetPartialPathCbkType)
      return 1;
  if (ud != userDataDef) dataError();

  fprintf (foutDef, "SPECIALNET partial data\n");

  fprintf(foutDef, "- %s ", ppath->name());

  count = 0;
  // compName & pinName
  for (i = 0; i < ppath->numConnections(); i++) {
      // set the limit of only 5 items print out in one line
      count++;
      if (count >= 5) {
          fprintf(foutDef, "\n");
          count = 0;
      }
      fprintf (foutDef, "( %s %s ) ", ppath->instance(i),
               ppath->pin(i));
      if (ppath->pinIsSynthesized(i))
          fprintf(foutDef, "+ SYNTHESIZED ");
  }

  // specialWiring
  // POLYGON
  if (ppath->numPolygons()) {
     struct defiPoints points;
    for (i = 0; i < ppath->numPolygons(); i++) {
      fprintf(foutDef, "\n  + POLYGON %s ", ppath->polygonName(i));
      points = ppath->getPolygon(i);
      for (j = 0; j < points.numPoints; j++)
        fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
    }
  }
  // RECT
  if (ppath->numRectangles()) {
     for (i = 0; i < ppath->numRectangles(); i++) {
       fprintf(foutDef, "\n  + RECT %s %d %d %d %d", ppath->rectName(i),
               ppath->xl(i), ppath->yl(i),
               ppath->xh(i), ppath->yh(i));
     }
  }

  // COVER, FIXED, ROUTED or SHIELD
  if (ppath->numWires()) {
     newLayer = 0;
     for (i = 0; i < ppath->numWires(); i++) {
        newLayer = 0;
        wire = ppath->wire(i);
        fprintf(foutDef, "\n  + %s ", wire->wireType());
        if (strcmp (wire->wireType(), "SHIELD") == 0)
           fprintf(foutDef, "%s ", wire->wireShieldNetName());
        for (j = 0; j < wire->numPaths(); j++) {
           p = wire->path(j);
           p->initTraverse();
           while ((path = (int)p->next()) != DEFIPATH_DONE) {
              count++;
              // Don't want the line to be too long
              if (count >= 5) {
                  fprintf(foutDef, "\n");
                  count = 0;
              }
              switch (path) {
                case DEFIPATH_LAYER:
                     if (newLayer == 0) {
                         fprintf(foutDef, "%s ", p->getLayer());
                         newLayer = 1;
                     } else
                         fprintf(foutDef, "NEW %s ", p->getLayer());
                     break;
                case DEFIPATH_VIA:
                     fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                     break;
                case DEFIPATH_VIAROTATION:
                     fprintf(foutDef, "%s ",
                             orientStr(p->getViaRotation()));
                     break;
                case DEFIPATH_VIADATA:
                     p->getViaData(&numX, &numY, &stepX, &stepY);
                     fprintf(foutDef, "DO %d BY %d STEP %d %d ", numX, numY,
                             stepX, stepY);
                     break;
                case DEFIPATH_WIDTH:
                     fprintf(foutDef, "%d ", p->getWidth());
                     break;
		case DEFIPATH_MASK:
		     fprintf(foutDef, "MASK %d ", p->getMask());
		     break;
                case DEFIPATH_VIAMASK:
                    fprintf(foutDef, "MASK %d%d%d ", 
                            p->getViaTopMask(), 
                            p->getViaCutMask(),
                            p->getViaBottomMask());
                    break;
                case DEFIPATH_POINT:
                     p->getPoint(&x, &y);
                     fprintf(foutDef, "( %d %d ) ", x, y);
                     break;
                case DEFIPATH_FLUSHPOINT:
                     p->getFlushPoint(&x, &y, &z);
                     fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                     break;
                case DEFIPATH_TAPER:
                     fprintf(foutDef, "TAPER ");
                     break;
                case DEFIPATH_SHAPE:
                     fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                     break;
                case DEFIPATH_STYLE:
                     fprintf(foutDef, "+ STYLE %d ", p->getStyle());
                     break;
              }
           }
        }
        fprintf(foutDef, "\n");
        count = 0;
     }
  }

  if (ppath->hasSubnets()) {
    for (i = 0; i < ppath->numSubnets(); i++) {
      s = ppath->subnet(i);
      if (s->numConnections()) {
          if (s->pinIsMustJoin(0))
              fprintf(foutDef, "- MUSTJOIN ");
          else
              fprintf(foutDef, "- %s ", s->name());
          for (j = 0; j < s->numConnections(); j++) {
              fprintf(foutDef, " ( %s %s )\n", s->instance(j),
                      s->pin(j));
        }
      }

      // regularWiring
      if (s->numWires()) {
         for (i = 0; i < s->numWires(); i++) {
            wire = s->wire(i);
            fprintf(foutDef, "  + %s ", wire->wireType());
            for (j = 0; j < wire->numPaths(); j++) {
              p = wire->path(j);
              p->print(foutDef);
            }
         }
      }
    }
  }

  if (ppath->numProps()) {
    for (i = 0; i < ppath->numProps(); i++) {
        if (ppath->propIsString(i))
           fprintf(foutDef, "  + PROPERTY %s %s ", ppath->propName(i),
                   ppath->propValue(i));
        if (ppath->propIsNumber(i))
           fprintf(foutDef, "  + PROPERTY %s %g ", ppath->propName(i),
                   ppath->propNumber(i));
        switch (ppath->propType(i)) {
           case 'R': fprintf(foutDef, "REAL ");
                     break;
           case 'I': fprintf(foutDef, "INTEGER ");
                     break;
           case 'S': fprintf(foutDef, "STRING ");
                     break;
           case 'Q': fprintf(foutDef, "QUOTESTRING ");
                     break;
           case 'N': fprintf(foutDef, "NUMBER ");
                     break;
        }
        fprintf(foutDef, "\n");
    }
  }

  // SHIELD
  count = 0;
  // testing the SHIELD for 5.3, obsolete in 5.4
  if (ppath->numShields()) {
    for (i = 0; i < ppath->numShields(); i++) {
       shield = ppath->shield(i);
       fprintf(foutDef, "\n  + SHIELD %s ", shield->shieldName());
       newLayer = 0;
       for (j = 0; j < shield->numPaths(); j++) {
          p = shield->path(j);
          p->initTraverse();
          while ((path = (int)p->next()) != DEFIPATH_DONE) {
             count++;
             // Don't want the line to be too long
             if (count >= 5) {
                 fprintf(foutDef, "\n");
                 count = 0;
             }
             switch (path) {
               case DEFIPATH_LAYER:
                    if (newLayer == 0) {
                        fprintf(foutDef, "%s ", p->getLayer());
                        newLayer = 1;
                    } else
                        fprintf(foutDef, "NEW %s ", p->getLayer());
                    break;
               case DEFIPATH_VIA:
                    fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                    break;
               case DEFIPATH_VIAROTATION:
                    if (newLayer)
                       fprintf(foutDef, "%s ",
                               orientStr(p->getViaRotation()));
                    else
                       fprintf(foutDef, "Str %s ",
                               p->getViaRotationStr());
                    break;
               case DEFIPATH_WIDTH:
                    fprintf(foutDef, "%d ", p->getWidth());
                    break;
	       case DEFIPATH_MASK:
		    fprintf(foutDef, "MASK %d ", p->getMask());
		    break;
               case DEFIPATH_VIAMASK:
                    fprintf(foutDef, "MASK %d%d%d ", 
                            p->getViaTopMask(), 
                            p->getViaCutMask(),
                            p->getViaBottomMask());
                    break;
               case DEFIPATH_POINT:
                    p->getPoint(&x, &y);
                    fprintf(foutDef, "( %d %d ) ", x, y);
                    break;
               case DEFIPATH_FLUSHPOINT:
                    p->getFlushPoint(&x, &y, &z);
                    fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                    break;
               case DEFIPATH_TAPER:
                    fprintf(foutDef, "TAPER ");
                    break;
               case DEFIPATH_SHAPE:
                    fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                    break;
               case DEFIPATH_STYLE:
                    fprintf(foutDef, "+ STYLE %d ", p->getStyle());
             }
          }
       }
    }
  }

  // layerName width
  if (ppath->hasWidthRules()) {
    for (i = 0; i < ppath->numWidthRules(); i++) {
        ppath->widthRule(i, &layerName, &dist);
        fprintf (foutDef, "\n  + WIDTH %s %g ", layerName, dist);
    }
  }

  // layerName spacing
  if (ppath->hasSpacingRules()) {
    for (i = 0; i < ppath->numSpacingRules(); i++) {
        ppath->spacingRule(i, &layerName, &dist, &left, &right);
        if (left == right)
            fprintf (foutDef, "\n  + SPACING %s %g ", layerName, dist);
        else
            fprintf (foutDef, "\n  + SPACING %s %g RANGE %g %g ",
                     layerName, dist, left, right);
    }
  }

  if (ppath->hasFixedbump())
    fprintf(foutDef, "\n  + FIXEDBUMP ");
  if (ppath->hasFrequency())
    fprintf(foutDef, "\n  + FREQUENCY %g ", ppath->frequency());
  if (ppath->hasVoltage())
    fprintf(foutDef, "\n  + VOLTAGE %g ", ppath->voltage());
  if (ppath->hasWeight())
    fprintf(foutDef, "\n  + WEIGHT %d ", ppath->weight());
  if (ppath->hasCap())
    fprintf(foutDef, "\n  + ESTCAP %g ", ppath->cap());
  if (ppath->hasSource())
    fprintf(foutDef, "\n  + SOURCE %s ", ppath->source());
  if (ppath->hasPattern())
    fprintf(foutDef, "\n  + PATTERN %s ", ppath->pattern());
  if (ppath->hasOriginal())
    fprintf(foutDef, "\n  + ORIGINAL %s ", ppath->original());
  if (ppath->hasUse())
    fprintf(foutDef, "\n  + USE %s ", ppath->use());

  fprintf(foutDef, "\n");

  return 0;
}


static int snetwire(defrCallbackType_e c, defiNet* ppath, defiUserData ud) {
  int         i, j, x, y, z, count = 0, newLayer;
  defiPath*   p;
  int         path;
  defiWire*   wire;
  defiShield* shield;
  int         numX, numY, stepX, stepY;

  if (c != defrSNetWireCbkType)
      return 1;
  if (ud != userDataDef) dataError();

  fprintf (foutDef, "SPECIALNET wire data\n");

  fprintf(foutDef, "- %s ", ppath->name());

  // POLYGON
  if (ppath->numPolygons()) {
	struct defiPoints points;
	for (i = 0; i < ppath->numPolygons(); i++) {
	    fprintf(foutDef, "\n  + POLYGON %s ", ppath->polygonName(i));

	    points = ppath->getPolygon(i);

	    for (j = 0; j < points.numPoints; j++) {
		fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
	    }
	}
  // RECT
  } 
  if (ppath->numRectangles()) {
	for (i = 0; i < ppath->numRectangles(); i++) {
	    fprintf(foutDef, "\n  + RECT %s %d %d %d %d", ppath->rectName(i),
            ppath->xl(i), ppath->yl(i),
            ppath->xh(i), ppath->yh(i));
	}
  }
  // VIA
  if (ppath->numViaSpecs()) {
	for (i = 0; i < ppath->numViaSpecs(); i++) {
	    fprintf(foutDef, "\n  + VIA %s ", ppath->viaName(i)),
	    fprintf(foutDef, " %s", ppath->viaOrientStr(i));

	    defiPoints points = ppath->getViaPts(i);

	    for (int j = 0; j < points.numPoints; j++) {
		fprintf(foutDef, " %d %d", points.x[j], points.y[j]);
	    }
	}
  }

  // specialWiring
  if (ppath->numWires()) {
     newLayer = 0;
     for (i = 0; i < ppath->numWires(); i++) {
        newLayer = 0;
        wire = ppath->wire(i);
        fprintf(foutDef, "\n  + %s ", wire->wireType());
        if (strcmp (wire->wireType(), "SHIELD") == 0)
           fprintf(foutDef, "%s ", wire->wireShieldNetName());
        for (j = 0; j < wire->numPaths(); j++) {
           p = wire->path(j);
           p->initTraverse();
           while ((path = (int)p->next()) != DEFIPATH_DONE) {
              count++;
              // Don't want the line to be too long
              if (count >= 5) {
                  fprintf(foutDef, "\n");
                  count = 0;
              }
              switch (path) {
                case DEFIPATH_LAYER:
                     if (newLayer == 0) {
                         fprintf(foutDef, "%s ", p->getLayer());
                         newLayer = 1;
                     } else
                         fprintf(foutDef, "NEW %s ", p->getLayer());
                     break;
                case DEFIPATH_VIA:
                     fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                     break;
                case DEFIPATH_VIAROTATION:
                     fprintf(foutDef, "%s ",
                             orientStr(p->getViaRotation()));
                     break;
                case DEFIPATH_VIADATA:
                     p->getViaData(&numX, &numY, &stepX, &stepY);
                     fprintf(foutDef, "DO %d BY %d STEP %d %d ", numX, numY,
                             stepX, stepY);
                     break;
                case DEFIPATH_WIDTH:
                     fprintf(foutDef, "%d ", p->getWidth());
                     break;
		case DEFIPATH_MASK:
		     fprintf(foutDef, "MASK %d ", p->getMask());
		     break;
                case DEFIPATH_VIAMASK:
                     fprintf(foutDef, "MASK %d%d%d ", 
                             p->getViaTopMask(), 
                             p->getViaCutMask(),
                             p->getViaBottomMask());
                    break;
                case DEFIPATH_POINT:
                     p->getPoint(&x, &y);
                     fprintf(foutDef, "( %d %d ) ", x, y);
                     break;
                case DEFIPATH_FLUSHPOINT:
                     p->getFlushPoint(&x, &y, &z);
                     fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                     break;
                case DEFIPATH_TAPER:
                     fprintf(foutDef, "TAPER ");
                     break;
                case DEFIPATH_SHAPE:
                     fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                     break;
                case DEFIPATH_STYLE:
                     fprintf(foutDef, "+ STYLE %d ", p->getStyle());
                     break;
              }
           }
        }
        fprintf(foutDef, "\n");
        count = 0;
     }
  } else if (ppath->numShields()) {
    for (i = 0; i < ppath->numShields(); i++) {
       shield = ppath->shield(i);
       fprintf(foutDef, "\n  + SHIELD %s ", shield->shieldName());
       newLayer = 0;
       for (j = 0; j < shield->numPaths(); j++) {
          p = shield->path(j);
          p->initTraverse();
          while ((path = (int)p->next()) != DEFIPATH_DONE) {
             count++;
             // Don't want the line to be too long
             if (count >= 5) {
                 fprintf(foutDef, "\n");
                 count = 0;
             } 
             switch (path) {
               case DEFIPATH_LAYER:
                    if (newLayer == 0) {
                        fprintf(foutDef, "%s ", p->getLayer());
                        newLayer = 1;
                    } else
                        fprintf(foutDef, "NEW %s ", p->getLayer());
                    break;
               case DEFIPATH_VIA:
                    fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                    break;
               case DEFIPATH_VIAROTATION:
                    fprintf(foutDef, "%s ", 
                            orientStr(p->getViaRotation()));
                    break;
               case DEFIPATH_WIDTH:
                    fprintf(foutDef, "%d ", p->getWidth());
                    break;
	       case DEFIPATH_MASK:
		    fprintf(foutDef, "MASK %d ", p->getMask());
		    break;
               case DEFIPATH_VIAMASK:
                   fprintf(foutDef, "MASK %d%d%d ", 
                           p->getViaTopMask(), 
                           p->getViaCutMask(),
                           p->getViaBottomMask());
                   break;
               case DEFIPATH_POINT:
                    p->getPoint(&x, &y);
                    fprintf(foutDef, "( %d %d ) ", x, y);
                    break;
               case DEFIPATH_FLUSHPOINT:
                    p->getFlushPoint(&x, &y, &z);
                    fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                    break;
               case DEFIPATH_TAPER:
                    fprintf(foutDef, "TAPER ");
                    break;
               case DEFIPATH_SHAPE:
                    fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                    break;
               case DEFIPATH_STYLE:
                    fprintf(foutDef, "+ STYLE %d ", p->getStyle());
                    break;
             }
          }
       }
    } 
  }

  fprintf(foutDef, "\n");

  return 0;
}

static int snetf(defrCallbackType_e c, defiNet* net, defiUserData ud) {
  // For net and special net.
  int         i, j, x, y, z, count, newLayer;
  char*       layerName;
  double      dist, left, right;
  defiPath*   p;
  defiSubnet  *s;
  int         path;
  defiShield* shield;
  defiWire*   wire;
  int         numX, numY, stepX, stepY;

  checkType(c);
  if (ud != userDataDef) dataError();
  if (c != defrSNetCbkType)
      fprintf(foutDef, "BOGUS NET TYPE  ");

// 5/6/2004 - don't need since I have a callback for the name
//  fprintf(foutDef, "- %s ", net->name());

  count = 0;
  // compName & pinName
  for (i = 0; i < net->numConnections(); i++) {
      // set the limit of only 5 items print out in one line
      count++;
      if (count >= 5) {
          fprintf(foutDef, "\n");
          count = 0;
      }
      fprintf (foutDef, "( %s %s ) ", net->instance(i),
               net->pin(i));
      if (net->pinIsSynthesized(i))
          fprintf(foutDef, "+ SYNTHESIZED ");
  }

  // specialWiring
  if (net->numWires()) {
     newLayer = 0;
     for (i = 0; i < net->numWires(); i++) {
        newLayer = 0;
        wire = net->wire(i);
        fprintf(foutDef, "\n  + %s ", wire->wireType());
        if (strcmp (wire->wireType(), "SHIELD") == 0)
           fprintf(foutDef, "%s ", wire->wireShieldNetName());
        for (j = 0; j < wire->numPaths(); j++) {
            p = wire->path(j);
            p->initTraverse();
            if (testDebugPrint) {
                p->print(foutDef);
            } else {
                while ((path = (int)p->next()) != DEFIPATH_DONE) {
                  count++;
                  // Don't want the line to be too long
                  if (count >= 5) {
                      fprintf(foutDef, "\n");
                      count = 0;
                  }
                  switch (path) {
                    case DEFIPATH_LAYER:
                         if (newLayer == 0) {
                             fprintf(foutDef, "%s ", p->getLayer());
                             newLayer = 1;
                         } else
                             fprintf(foutDef, "NEW %s ", p->getLayer());
                         break;
                    case DEFIPATH_VIA:
                         fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                         break;
                    case DEFIPATH_VIAROTATION:
                         fprintf(foutDef, "%s ", 
                                 orientStr(p->getViaRotation()));
                         break;
                    case DEFIPATH_VIADATA:
                         p->getViaData(&numX, &numY, &stepX, &stepY);
                         fprintf(foutDef, "DO %d BY %d STEP %d %d ", numX, numY,
                                 stepX, stepY);
                         break;
                    case DEFIPATH_WIDTH:
                         fprintf(foutDef, "%d ", p->getWidth());
                         break;
                    case DEFIPATH_MASK:
                         fprintf(foutDef, "MASK %d ", p->getMask());
                         break;
                    case DEFIPATH_VIAMASK:
                         fprintf(foutDef, "MASK %d%d%d ", 
                                 p->getViaTopMask(), 
                                 p->getViaCutMask(),
                                 p->getViaBottomMask());
                        break;
                    case DEFIPATH_POINT:
                         p->getPoint(&x, &y);
                         fprintf(foutDef, "( %d %d ) ", x, y);
                         break;
                    case DEFIPATH_FLUSHPOINT:
                         p->getFlushPoint(&x, &y, &z);
                         fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                         break;
                    case DEFIPATH_TAPER:
                         fprintf(foutDef, "TAPER ");
                         break;
                    case DEFIPATH_SHAPE:
                         fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                         break;
                    case DEFIPATH_STYLE:
                         fprintf(foutDef, "+ STYLE %d ", p->getStyle());
                         break;
                    }
                }
            }
        }
        fprintf(foutDef, "\n");
        count = 0;
     }
  }

  // POLYGON
  if (net->numPolygons()) {
    struct defiPoints points;

    for (i = 0; i < net->numPolygons(); i++) {
      if (curVer >= 5.8 ) {
	 if (strcmp(net->polyRouteStatus(i), "") != 0) {
	   fprintf(foutDef, "\n  + %s ", net->polyRouteStatus(i));
	   if (strcmp(net->polyRouteStatus(i), "SHIELD") == 0) {
	      fprintf(foutDef, "\n  + %s ", net->polyRouteStatusShieldName(i));
	   }
         }
         if (strcmp(net->polyShapeType(i), "") != 0) {
	   fprintf(foutDef, "\n  + SHAPE %s ", net->polyShapeType(i));
         }
      }
      if (net->polyMask(i)) {
	  fprintf(foutDef, "\n  + MASK %d + POLYGON % s ", net->polyMask(i),
		  net->polygonName(i));
      } else {
          fprintf(foutDef, "\n  + POLYGON %s ", net->polygonName(i));
      }
      points = net->getPolygon(i);
      for (j = 0; j < points.numPoints; j++)
        fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
    }
  }
  // RECT
  if (net->numRectangles()) {

     for (i = 0; i < net->numRectangles(); i++) {
       if (curVer >= 5.8 ) {
	 if (strcmp(net->rectRouteStatus(i), "") != 0) {
	   fprintf(foutDef, "\n  + %s ", net->rectRouteStatus(i));
	   if (strcmp(net->rectRouteStatus(i), "SHIELD") == 0) {
	      fprintf(foutDef, "\n  + %s ", net->rectRouteStatusShieldName(i));
	   }
         }
         if (strcmp(net->rectShapeType(i), "") != 0) {
	   fprintf(foutDef, "\n  + SHAPE %s ", net->rectShapeType(i));
         }
       }
       if (net->rectMask(i)) {
	  fprintf(foutDef, "\n  + MASK %d + RECT %s %d %d %d %d", 
		  net->rectMask(i), net->rectName(i),
		  net->xl(i), net->yl(i), net->xh(i),
                  net->yh(i));
       } else {
	    fprintf(foutDef, "\n  + RECT %s %d %d %d %d", 
		    net->rectName(i),
                    net->xl(i), 
		    net->yl(i), 
		    net->xh(i),
                    net->yh(i));
      }
     }
  }
  // VIA
  if (curVer >= 5.8 && net->numViaSpecs()) {
     for (i = 0; i < net->numViaSpecs(); i++) {
       if (strcmp(net->viaRouteStatus(i), "") != 0) {
	fprintf(foutDef, "\n  + %s ", net->viaRouteStatus(i));
	if (strcmp(net->viaRouteStatus(i), "SHIELD") == 0) {
	      fprintf(foutDef, "\n  + %s ", net->viaRouteStatusShieldName(i));
	   }
       }
       if (strcmp(net->viaShapeType(i), "") != 0) {
	fprintf(foutDef, "\n  + SHAPE %s ", net->viaShapeType(i));
       }
       if (net->topMaskNum(i) || net->cutMaskNum(i) || net->bottomMaskNum(i)) {
	fprintf(foutDef, "\n  + MASK %d%d%d + VIA %s ", net->topMaskNum(i), 
                net->cutMaskNum(i),
                net->bottomMaskNum(i),
		net->viaName(i));
       } else {
	fprintf(foutDef, "\n  + VIA %s ", net->viaName(i));
       }
       fprintf(foutDef, " %s", net->viaOrientStr(i));
      
       defiPoints points = net->getViaPts(i);

       for (int j = 0; j < points.numPoints; j++) {
          fprintf(foutDef, " %d %d", points.x[j], points.y[j]);
       }
       fprintf(foutDef, ";\n"); 

     }
  }

  if (net->hasSubnets()) {
    for (i = 0; i < net->numSubnets(); i++) {
      s = net->subnet(i);
      if (s->numConnections()) {
          if (s->pinIsMustJoin(0))
              fprintf(foutDef, "- MUSTJOIN ");
          else
              fprintf(foutDef, "- %s ", s->name());
          for (j = 0; j < s->numConnections(); j++) {
              fprintf(foutDef, " ( %s %s )\n", s->instance(j),
                      s->pin(j));
        }
      }
 
      // regularWiring
      if (s->numWires()) {
         for (i = 0; i < s->numWires(); i++) {
            wire = s->wire(i);
            fprintf(foutDef, "  + %s ", wire->wireType());
            for (j = 0; j < wire->numPaths(); j++) {
              p = wire->path(j);
              p->print(foutDef);
            }
         }
      }
    }
  }

  if (net->numProps()) {
    for (i = 0; i < net->numProps(); i++) {
        if (net->propIsString(i))
           fprintf(foutDef, "  + PROPERTY %s %s ", net->propName(i),
                   net->propValue(i));
        if (net->propIsNumber(i))
           fprintf(foutDef, "  + PROPERTY %s %g ", net->propName(i),
                   net->propNumber(i));
        switch (net->propType(i)) {
           case 'R': fprintf(foutDef, "REAL ");
                     break;
           case 'I': fprintf(foutDef, "INTEGER ");
                     break;
           case 'S': fprintf(foutDef, "STRING ");
                     break;
           case 'Q': fprintf(foutDef, "QUOTESTRING ");
                     break;
           case 'N': fprintf(foutDef, "NUMBER ");
                     break;
        }
        fprintf(foutDef, "\n");
    }
  }

  // SHIELD
  count = 0;
  // testing the SHIELD for 5.3, obsolete in 5.4
  if (net->numShields()) {
    for (i = 0; i < net->numShields(); i++) {
       shield = net->shield(i);
       fprintf(foutDef, "\n  + SHIELD %s ", shield->shieldName());
       newLayer = 0;
       for (j = 0; j < shield->numPaths(); j++) {
          p = shield->path(j);
          p->initTraverse();
          while ((path = (int)p->next()) != DEFIPATH_DONE) {
             count++;
             // Don't want the line to be too long
             if (count >= 5) {
                 fprintf(foutDef, "\n");
                 count = 0;
             } 
             switch (path) {
               case DEFIPATH_LAYER:
                    if (newLayer == 0) {
                        fprintf(foutDef, "%s ", p->getLayer());
                        newLayer = 1;
                    } else
                        fprintf(foutDef, "NEW %s ", p->getLayer());
                    break;
               case DEFIPATH_VIA:
                    fprintf(foutDef, "%s ", ignoreViaNames ? "XXX" : p->getVia());
                    break;
               case DEFIPATH_VIAROTATION:
                    fprintf(foutDef, "%s ", 
                            orientStr(p->getViaRotation()));
                    break;
               case DEFIPATH_WIDTH:
                    fprintf(foutDef, "%d ", p->getWidth());
                    break;
	       case DEFIPATH_MASK:
		    fprintf(foutDef, "MASK %d ", p->getMask());
		    break;
               case DEFIPATH_VIAMASK:
                    fprintf(foutDef, "MASK %d%d%d ", 
                            p->getViaTopMask(), 
                            p->getViaCutMask(),
                            p->getViaBottomMask());
                   break;
               case DEFIPATH_POINT:
                    p->getPoint(&x, &y);
                    fprintf(foutDef, "( %d %d ) ", x, y);
                    break;
               case DEFIPATH_FLUSHPOINT:
                    p->getFlushPoint(&x, &y, &z);
                    fprintf(foutDef, "( %d %d %d ) ", x, y, z);
                    break;
               case DEFIPATH_TAPER:
                    fprintf(foutDef, "TAPER ");
                    break;
               case DEFIPATH_SHAPE:
                    fprintf(foutDef, "+ SHAPE %s ", p->getShape());
                    break;
               case DEFIPATH_STYLE:
                    fprintf(foutDef, "+ STYLE %d ", p->getStyle());
                    break;
             }
          }
       }
    }
  }

  // layerName width
  if (net->hasWidthRules()) {
    for (i = 0; i < net->numWidthRules(); i++) {
        net->widthRule(i, &layerName, &dist);
        fprintf (foutDef, "\n  + WIDTH %s %g ", layerName, dist);
    }
  }

  // layerName spacing
  if (net->hasSpacingRules()) {
    for (i = 0; i < net->numSpacingRules(); i++) {
        net->spacingRule(i, &layerName, &dist, &left, &right);
        if (left == right)
            fprintf (foutDef, "\n  + SPACING %s %g ", layerName, dist);
        else
            fprintf (foutDef, "\n  + SPACING %s %g RANGE %g %g ",
                     layerName, dist, left, right);
    }
  }

  if (net->hasFixedbump())
    fprintf(foutDef, "\n  + FIXEDBUMP ");
  if (net->hasFrequency())
    fprintf(foutDef, "\n  + FREQUENCY %g ", net->frequency());
  if (net->hasVoltage())
    fprintf(foutDef, "\n  + VOLTAGE %g ", net->voltage());
  if (net->hasWeight())
    fprintf(foutDef, "\n  + WEIGHT %d ", net->weight());
  if (net->hasCap())
    fprintf(foutDef, "\n  + ESTCAP %g ", net->cap());
  if (net->hasSource())
    fprintf(foutDef, "\n  + SOURCE %s ", net->source());
  if (net->hasPattern())
    fprintf(foutDef, "\n  + PATTERN %s ", net->pattern());
  if (net->hasOriginal())
    fprintf(foutDef, "\n  + ORIGINAL %s ", net->original());
  if (net->hasUse())
    fprintf(foutDef, "\n  + USE %s ", net->use());

  fprintf (foutDef, ";\n");
  --numObjs;
  if (numObjs <= 0)
      fprintf(foutDef, "END SPECIALNETS\n");
  return 0;
}


static int ndr(defrCallbackType_e c, defiNonDefault* nd, defiUserData ud) {
  // For nondefaultrule
  int i;

  checkType(c);
  if (ud != userDataDef) dataError();
  if (c != defrNonDefaultCbkType)
      fprintf(foutDef, "BOGUS NONDEFAULTRULE TYPE  ");
  fprintf(foutDef, "- %s\n", nd->name());
  if (nd->hasHardspacing())
      fprintf(foutDef, "   + HARDSPACING\n");
  for (i = 0; i < nd->numLayers(); i++) {
    fprintf(foutDef, "   + LAYER %s", nd->layerName(i));
    fprintf(foutDef, " WIDTH %d", nd->layerWidthVal(i));
    if (nd->hasLayerDiagWidth(i)) 
      fprintf(foutDef, " DIAGWIDTH %d",
              nd->layerDiagWidthVal(i));
    if (nd->hasLayerSpacing(i)) 
      fprintf(foutDef, " SPACING %d", nd->layerSpacingVal(i));
    if (nd->hasLayerWireExt(i)) 
      fprintf(foutDef, " WIREEXT %d", nd->layerWireExtVal(i));
    fprintf(foutDef, "\n");
  }
  for (i = 0; i < nd->numVias(); i++)
    fprintf(foutDef, "   + VIA %s\n", nd->viaName(i));
  for (i = 0; i < nd->numViaRules(); i++)
    fprintf(foutDef, "   + VIARULE %s\n", ignoreViaNames ? "XXX" : nd->viaRuleName(i));
  for (i = 0; i < nd->numMinCuts(); i++)
    fprintf(foutDef, "   + MINCUTS %s %d\n", nd->cutLayerName(i),
            nd->numCuts(i));
  for (i = 0; i < nd->numProps(); i++) {
    fprintf(foutDef, "   + PROPERTY %s %s ", nd->propName(i),
            nd->propValue(i));
    switch (nd->propType(i)) {
      case 'R': fprintf(foutDef, "REAL\n");
                break;
      case 'I': fprintf(foutDef, "INTEGER\n");
                break;
      case 'S': fprintf(foutDef, "STRING\n");
                break;
      case 'Q': fprintf(foutDef, "QUOTESTRING\n");
                break;
      case 'N': fprintf(foutDef, "NUMBER\n");
                break;
    }
  }
  --numObjs;
  if (numObjs <= 0)
    fprintf(foutDef, "END NONDEFAULTRULES\n");
  return 0;
}

static int tname(defrCallbackType_e c, const char* string, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "TECHNOLOGY %s ;\n", string);
  return 0;
}

static int dname(defrCallbackType_e c, const char* string, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "DESIGN %s ;\n", string);

  return 0;
}


static char* address(const char* in) {
  return ((char*)in);
}

static int cs(defrCallbackType_e c, int num, defiUserData ud) {
  char* name;

  checkType(c);

  if (ud != userDataDef) dataError();

  switch (c) {
  case defrComponentStartCbkType : name = address("COMPONENTS"); break;
  case defrNetStartCbkType : name = address("NETS"); break;
  case defrStartPinsCbkType : name = address("PINS"); break;
  case defrViaStartCbkType : name = address("VIAS"); break;
  case defrRegionStartCbkType : name = address("REGIONS"); break;
  case defrSNetStartCbkType : name = address("SPECIALNETS"); break;
  case defrGroupsStartCbkType : name = address("GROUPS"); break;
  case defrScanchainsStartCbkType : name = address("SCANCHAINS"); break;
  case defrIOTimingsStartCbkType : name = address("IOTIMINGS"); break;
  case defrFPCStartCbkType : name = address("FLOORPLANCONSTRAINTS"); break;
  case defrTimingDisablesStartCbkType : name = address("TIMING DISABLES"); break;
  case defrPartitionsStartCbkType : name = address("PARTITIONS"); break;
  case defrPinPropStartCbkType : name = address("PINPROPERTIES"); break;
  case defrBlockageStartCbkType : name = address("BLOCKAGES"); break;
  case defrSlotStartCbkType : name = address("SLOTS"); break;
  case defrFillStartCbkType : name = address("FILLS"); break;
  case defrNonDefaultStartCbkType : name = address("NONDEFAULTRULES"); break;
  case defrStylesStartCbkType : name = address("STYLES"); break;
  default : name = address("BOGUS"); return 1;
  }
  fprintf(foutDef, "\n%s %d ;\n", name, num);
  numObjs = num;
  return 0;
}

static int constraintst(defrCallbackType_e c, int num, defiUserData ud) {
  // Handles both constraints and assertions
  checkType(c);
  if (ud != userDataDef) dataError();
  if (c == defrConstraintsStartCbkType)
      fprintf(foutDef, "\nCONSTRAINTS %d ;\n\n", num);
  else
      fprintf(foutDef, "\nASSERTIONS %d ;\n\n", num);
  numObjs = num;
  return 0;
}

static void operand(defrCallbackType_e c, defiAssertion* a, int ind) {
  int i, first = 1;
  char* netName;
  char* fromInst, * fromPin, * toInst, * toPin;

  if (a->isSum()) {
      // Sum in operand, recursively call operand
      fprintf(foutDef, "- SUM ( ");
      a->unsetSum();
      isSumSet = 1;
      begOperand = 0;
      operand (c, a, ind);
      fprintf(foutDef, ") ");
  } else {
      // operand
      if (ind >= a->numItems()) {
          fprintf(foutDef, "ERROR: when writing out SUM in Constraints.\n");
          return;
       }
      if (begOperand) {
         fprintf(foutDef, "- ");
         begOperand = 0;
      }
      for (i = ind; i < a->numItems(); i++) {
          if (a->isNet(i)) {
              a->net(i, &netName);
              if (!first)
                  fprintf(foutDef, ", "); // print , as separator
              fprintf(foutDef, "NET %s ", netName); 
          } else if (a->isPath(i)) {
              a->path(i, &fromInst, &fromPin, &toInst,
                                     &toPin);
              if (!first)
                  fprintf(foutDef, ", ");
              fprintf(foutDef, "PATH %s %s %s %s ", fromInst, fromPin, toInst,
                      toPin);
          } else if (isSumSet) {
              // SUM within SUM, reset the flag
              a->setSum();
              operand(c, a, i);
          }
          first = 0;
      } 
      
  }
}

static int constraint(defrCallbackType_e c, defiAssertion* a, defiUserData ud) {
  // Handles both constraints and assertions

  checkType(c);
  if (ud != userDataDef) dataError();
  if (a->isWiredlogic())
      // Wirelogic
      fprintf(foutDef, "- WIREDLOGIC %s + MAXDIST %g ;\n",
// Wiredlogic dist is also store in fallMax
//              a->netName(), a->distance());
              a->netName(), a->fallMax());
  else {
      // Call the operand function
      isSumSet = 0;    // reset the global variable
      begOperand = 1;
      operand (c, a, 0);
      // Get the Rise and Fall
      if (a->hasRiseMax())
          fprintf(foutDef, "+ RISEMAX %g ", a->riseMax());
      if (a->hasFallMax())
          fprintf(foutDef, "+ FALLMAX %g ", a->fallMax());
      if (a->hasRiseMin())
          fprintf(foutDef, "+ RISEMIN %g ", a->riseMin());
      if (a->hasFallMin())
          fprintf(foutDef, "+ FALLMIN %g ", a->fallMin());
      fprintf(foutDef, ";\n");
  }
  --numObjs;
  if (numObjs <= 0) {
      if (c == defrConstraintCbkType)
          fprintf(foutDef, "END CONSTRAINTS\n");
      else 
          fprintf(foutDef, "END ASSERTIONS\n");
  }
  return 0;
}


static int propstart(defrCallbackType_e c, void*, defiUserData) {
  checkType(c);
  fprintf(foutDef, "\nPROPERTYDEFINITIONS\n");
  isProp = 1;

  return 0;
}


static int prop(defrCallbackType_e c, defiProp* p, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  if (strcmp(p->propType(), "design") == 0)
      fprintf(foutDef, "DESIGN %s ", p->propName());
  else if (strcmp(p->propType(), "net") == 0)
      fprintf(foutDef, "NET %s ", p->propName());
  else if (strcmp(p->propType(), "component") == 0)
      fprintf(foutDef, "COMPONENT %s ", p->propName());
  else if (strcmp(p->propType(), "specialnet") == 0)
      fprintf(foutDef, "SPECIALNET %s ", p->propName());
  else if (strcmp(p->propType(), "group") == 0)
      fprintf(foutDef, "GROUP %s ", p->propName());
  else if (strcmp(p->propType(), "row") == 0)
      fprintf(foutDef, "ROW %s ", p->propName());
  else if (strcmp(p->propType(), "componentpin") == 0)
      fprintf(foutDef, "COMPONENTPIN %s ", p->propName());
  else if (strcmp(p->propType(), "region") == 0)
      fprintf(foutDef, "REGION %s ", p->propName());
  else if (strcmp(p->propType(), "nondefaultrule") == 0)
      fprintf(foutDef, "NONDEFAULTRULE %s ", p->propName());
  if (p->dataType() == 'I')
      fprintf(foutDef, "INTEGER ");
  if (p->dataType() == 'R')
      fprintf(foutDef, "REAL ");
  if (p->dataType() == 'S')
      fprintf(foutDef, "STRING ");
  if (p->dataType() == 'Q')
      fprintf(foutDef, "STRING ");
  if (p->hasRange()) {
      fprintf(foutDef, "RANGE %g %g ", p->left(),
              p->right());
  }
  if (p->hasNumber())
      fprintf(foutDef, "%g ", p->number());
  if (p->hasString())
      fprintf(foutDef, "\"%s\" ", p->string());
  fprintf(foutDef, ";\n");

  return 0;
}


static int propend(defrCallbackType_e c, void*, defiUserData) {
  checkType(c);
  if (isProp) {
      fprintf(foutDef, "END PROPERTYDEFINITIONS\n\n");
      isProp = 0;
  }

  return 0;
}


static int hist(defrCallbackType_e c, const char* h, defiUserData ud) {
  checkType(c);
  defrSetCaseSensitivity(0);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "HISTORY %s ;\n", h);
  defrSetCaseSensitivity(1);
  return 0;
}


static int an(defrCallbackType_e c, const char* h, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "ARRAY %s ;\n", h);
  return 0;
}


static int fn(defrCallbackType_e c, const char* h, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "FLOORPLAN %s ;\n", h);
  return 0;
}


static int bbn(defrCallbackType_e c, const char* h, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "BUSBITCHARS \"%s\" ;\n", h);
  return 0;
}


static int vers(defrCallbackType_e c, double d, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) 
      dataError();
  fprintf(foutDef, "VERSION %g ;\n", d);  
  curVer = d;

  fprintf(foutDef, "ALIAS alias1 aliasValue1 1 ;\n");
  fprintf(foutDef, "ALIAS alias2 aliasValue2 0 ;\n");

  return 0;
}


static int versStr(defrCallbackType_e c, const char* versionName, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "VERSION %s ;\n", versionName);
  return 0;
}


static int units(defrCallbackType_e c, double d, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "UNITS DISTANCE MICRONS %g ;\n", d);
  return 0;
}


static int casesens(defrCallbackType_e c, int d, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  if (d == 1)
     fprintf(foutDef, "NAMESCASESENSITIVE ON ;\n", d);
  else
     fprintf(foutDef, "NAMESCASESENSITIVE OFF ;\n", d);
  return 0;
}


static int cls(defrCallbackType_e c, void* cl, defiUserData ud) {
  defiSite* site;  // Site and Canplace and CannotOccupy
  defiBox* box;  // DieArea and 
  defiPinCap* pc;
  defiPin* pin;
  int i, j, k;
  defiRow* row;
  defiTrack* track;
  defiGcellGrid* gcg;
  defiVia* via;
  defiRegion* re;
  defiGroup* group;
  defiComponentMaskShiftLayer* maskShiftLayer = NULL;
  defiScanchain* sc;
  defiIOTiming* iot;
  defiFPC* fpc;
  defiTimingDisable* td;
  defiPartition* part;
  defiPinProp* pprop;
  defiBlockage* block;
  defiSlot* slots;
  defiFill* fills;
  defiStyles* styles;
  int xl, yl, xh, yh;
  char *name, *a1, *b1;
  char **inst, **inPin, **outPin;
  int  *bits;
  int  size;
  int corner, typ;
  const char *itemT;
  char dir;
  defiPinAntennaModel* aModel;
  struct defiPoints points;

  checkType(c);
  if (ud != userDataDef) dataError();
  switch (c) {

  case defrSiteCbkType :
         site = (defiSite*)cl;
         fprintf(foutDef, "SITE %s %g %g %s ", site->name(),
                 site->x_orig(), site->y_orig(),
                 orientStr(site->orient()));
         fprintf(foutDef, "DO %g BY %g STEP %g %g ;\n",
                 site->x_num(), site->y_num(),
                 site->x_step(), site->y_step());
         break;
  case defrCanplaceCbkType :
         site = (defiSite*)cl;
         fprintf(foutDef, "CANPLACE %s %g %g %s ", site->name(),
                 site->x_orig(), site->y_orig(),
                 orientStr(site->orient()));
         fprintf(foutDef, "DO %g BY %g STEP %g %g ;\n",
                 site->x_num(), site->y_num(),
                 site->x_step(), site->y_step());
         break;
  case defrCannotOccupyCbkType : 
         site = (defiSite*)cl;
         fprintf(foutDef, "CANNOTOCCUPY %s %g %g %s ",
                 site->name(), site->x_orig(),
                 site->y_orig(), orientStr(site->orient()));
         fprintf(foutDef, "DO %g BY %g STEP %g %g ;\n",
                 site->x_num(), site->y_num(),
                 site->x_step(), site->y_step());
         break;
  case defrDieAreaCbkType :
         box = (defiBox*)cl;
         fprintf(foutDef, "DIEAREA %d %d %d %d ;\n",
                 box->xl(), box->yl(), box->xh(),
                 box->yh());
         fprintf(foutDef, "DIEAREA ");
         points = box->getPoint();
         for (i = 0; i < points.numPoints; i++)
           fprintf(foutDef, "%d %d ", points.x[i], points.y[i]);
         fprintf(foutDef, ";\n");
         break;
  case defrPinCapCbkType :
         pc = (defiPinCap*)cl;
         if (testDebugPrint) {
             pc->print(foutDef);
         } else {
             fprintf(foutDef, "MINPINS %d WIRECAP %g ;\n",
                     pc->pin(), pc->cap());
             --numObjs;
             if (numObjs <= 0)
                 fprintf(foutDef, "END DEFAULTCAP\n");
         }
         break;
  case defrPinCbkType :
         pin = (defiPin*)cl;
         if (testDebugPrint) {
             pin->print(foutDef);
         } else {
             fprintf(foutDef, "- %s + NET %s ", pin->pinName(),
                     pin->netName());
    //         pin->changePinName("pinName");
    //         fprintf(foutDef, "%s ", pin->pinName());
             if (pin->hasDirection())
                 fprintf(foutDef, "+ DIRECTION %s ", pin->direction());
             if (pin->hasUse())
                 fprintf(foutDef, "+ USE %s ", pin->use());
             if (pin->hasNetExpr())
                 fprintf(foutDef, "+ NETEXPR \"%s\" ", pin->netExpr());
             if (pin->hasSupplySensitivity())
                 fprintf(foutDef, "+ SUPPLYSENSITIVITY %s ",
                         pin->supplySensitivity());
             if (pin->hasGroundSensitivity())
                 fprintf(foutDef, "+ GROUNDSENSITIVITY %s ",
                         pin->groundSensitivity());
             if (pin->hasLayer()) {
                 struct defiPoints points;
                 for (i = 0; i < pin->numLayer(); i++) {
                    fprintf(foutDef, "\n  + LAYER %s ", pin->layer(i));
                    if (pin->layerMask(i)) 
                        fprintf(foutDef, "MASK %d ",
                        pin->layerMask(i));
                    if (pin->hasLayerSpacing(i))
                      fprintf(foutDef, "SPACING %d ",
                             pin->layerSpacing(i));
                    if (pin->hasLayerDesignRuleWidth(i))
                      fprintf(foutDef, "DESIGNRULEWIDTH %d ",
                             pin->layerDesignRuleWidth(i));
                    pin->bounds(i, &xl, &yl, &xh, &yh);
                    fprintf(foutDef, "%d %d %d %d ", xl, yl, xh, yh);
                 }
                 for (i = 0; i < pin->numPolygons(); i++) {
                    fprintf(foutDef, "\n  + POLYGON %s ",
                            pin->polygonName(i));
                    if (pin->polygonMask(i))
                      fprintf(foutDef, "MASK %d ",
                              pin->polygonMask(i));
                    if (pin->hasPolygonSpacing(i))
                      fprintf(foutDef, "SPACING %d ",
                             pin->polygonSpacing(i));
                    if (pin->hasPolygonDesignRuleWidth(i))
                      fprintf(foutDef, "DESIGNRULEWIDTH %d ",
                             pin->polygonDesignRuleWidth(i));
                    points = pin->getPolygon(i);
                    for (j = 0; j < points.numPoints; j++)
                      fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
                 }
                 for (i = 0; i < pin->numVias(); i++) {
                     if (pin->viaTopMask(i) || pin->viaCutMask(i) || pin->viaBottomMask(i)) {
                         fprintf(foutDef, "\n  + VIA %s MASK %d%d%d %d %d ", 
                             pin->viaName(i),
                             pin->viaTopMask(i),
                             pin->viaCutMask(i),
                             pin->viaBottomMask(i),
                             pin->viaPtX(i), 
                             pin->viaPtY(i));
                     } else {
                         fprintf(foutDef, "\n  + VIA %s %d %d ", pin->viaName(i),
                                 pin->viaPtX(i), pin->viaPtY(i));
                     }
                 }
             }
             if (pin->hasPort()) {
                 struct defiPoints points;
                 defiPinPort* port;
                 for (j = 0; j < pin->numPorts(); j++) {
                    port = pin->pinPort(j);
                    fprintf(foutDef, "\n  + PORT");
                    for (i = 0; i < port->numLayer(); i++) {
                       fprintf(foutDef, "\n     + LAYER %s ",
                               port->layer(i));
                       if (port->layerMask(i))
                           fprintf(foutDef, "MASK %d ",
                                   port->layerMask(i));
                       if (port->hasLayerSpacing(i))
                         fprintf(foutDef, "SPACING %d ",
                                 port->layerSpacing(i));
                       if (port->hasLayerDesignRuleWidth(i))
                         fprintf(foutDef, "DESIGNRULEWIDTH %d ",
                                 port->layerDesignRuleWidth(i));
                       port->bounds(i, &xl, &yl, &xh, &yh);
                       fprintf(foutDef, "%d %d %d %d ", xl, yl, xh, yh);
                    }
                    for (i = 0; i < port->numPolygons(); i++) {
                       fprintf(foutDef, "\n     + POLYGON %s ",
                               port->polygonName(i));
                       if (port->polygonMask(i))
                         fprintf(foutDef, "MASK %d ",
                                 port->polygonMask(i));
                       if (port->hasPolygonSpacing(i))
                         fprintf(foutDef, "SPACING %d ",
                                port->polygonSpacing(i));
                       if (port->hasPolygonDesignRuleWidth(i))
                         fprintf(foutDef, "DESIGNRULEWIDTH %d ",
                                port->polygonDesignRuleWidth(i));
                       points = port->getPolygon(i);
                       for (k = 0; k < points.numPoints; k++)
                         fprintf(foutDef, "( %d %d ) ", points.x[k], points.y[k]);
                    }
                    for (i = 0; i < port->numVias(); i++) {
                        if (port->viaTopMask(i) || port->viaCutMask(i) 
                            || port->viaBottomMask(i)) {
                            fprintf(foutDef, "\n     + VIA %s MASK %d%d%d ( %d %d ) ",
                                port->viaName(i),
                                port->viaTopMask(i),
                                port->viaCutMask(i),
                                port->viaBottomMask(i),
                                port->viaPtX(i),
                                port->viaPtY(i));
                        } else {
                            fprintf(foutDef, "\n     + VIA %s ( %d %d ) ",
                               port->viaName(i),
                               port->viaPtX(i),
                               port->viaPtY(i));
                        }
                    }
                    if (port->hasPlacement()) {
                       if (port->isPlaced()) {
                          fprintf(foutDef, "\n     + PLACED ");
                          fprintf(foutDef, "( %d %d ) %s ",
                             port->placementX(),
                             port->placementY(),
                             orientStr(port->orient()));
                       }
                       if (port->isCover()) {
                          fprintf(foutDef, "\n     + COVER ");
                          fprintf(foutDef, "( %d %d ) %s ",
                             port->placementX(),
                             port->placementY(),
                             orientStr(port->orient()));
                       }
                       if (port->isFixed()) {
                          fprintf(foutDef, "\n     + FIXED ");
                          fprintf(foutDef, "( %d %d ) %s ",
                             port->placementX(),
                             port->placementY(),
                             orientStr(port->orient()));
                       }
                    }
                }
             }
             if (pin->hasPlacement()) {
                 if (pin->isPlaced()) {
                     fprintf(foutDef, "+ PLACED ");
                     fprintf(foutDef, "( %d %d ) %s ", pin->placementX(),
                         pin->placementY(), 
                         orientStr(pin->orient()));
                }
                 if (pin->isCover()) {
                     fprintf(foutDef, "+ COVER ");
                     fprintf(foutDef, "( %d %d ) %s ", pin->placementX(),
                         pin->placementY(), 
                         orientStr(pin->orient()));
                 }
                 if (pin->isFixed()) {
                     fprintf(foutDef, "+ FIXED ");
                     fprintf(foutDef, "( %d %d ) %s ", pin->placementX(),
                         pin->placementY(), 
                         orientStr(pin->orient()));
                 }
                 if (pin->isUnplaced())
                     fprintf(foutDef, "+ UNPLACED ");
             }
             if (pin->hasSpecial()) {
                 fprintf(foutDef, "+ SPECIAL ");
             }
             if (pin->hasAPinPartialMetalArea()) {
                 for (i = 0; i < pin->numAPinPartialMetalArea(); i++) {
                    fprintf(foutDef, "ANTENNAPINPARTIALMETALAREA %d",
                            pin->APinPartialMetalArea(i));
                    if (*(pin->APinPartialMetalAreaLayer(i)))
                        fprintf(foutDef, " LAYER %s",
                                pin->APinPartialMetalAreaLayer(i));
                    fprintf(foutDef, "\n");
                 }
             }
             if (pin->hasAPinPartialMetalSideArea()) {
                 for (i = 0; i < pin->numAPinPartialMetalSideArea(); i++) {
                    fprintf(foutDef, "ANTENNAPINPARTIALMETALSIDEAREA %d",
                            pin->APinPartialMetalSideArea(i));
                    if (*(pin->APinPartialMetalSideAreaLayer(i)))
                        fprintf(foutDef, " LAYER %s",
                            pin->APinPartialMetalSideAreaLayer(i));
                    fprintf(foutDef, "\n");
                 }
             }
             if (pin->hasAPinDiffArea()) {
                 for (i = 0; i < pin->numAPinDiffArea(); i++) {
                    fprintf(foutDef, "ANTENNAPINDIFFAREA %d", pin->APinDiffArea(i));
                    if (*(pin->APinDiffAreaLayer(i)))
                        fprintf(foutDef, " LAYER %s", pin->APinDiffAreaLayer(i));
                    fprintf(foutDef, "\n");
                 }
             }
             if (pin->hasAPinPartialCutArea()) {
                 for (i = 0; i < pin->numAPinPartialCutArea(); i++) {
                    fprintf(foutDef, "ANTENNAPINPARTIALCUTAREA %d",
                            pin->APinPartialCutArea(i));
                    if (*(pin->APinPartialCutAreaLayer(i)))
                        fprintf(foutDef, " LAYER %s", pin->APinPartialCutAreaLayer(i));
                    fprintf(foutDef, "\n");
                 }
             }

             for (j = 0; j < pin->numAntennaModel(); j++) {
                aModel = pin->antennaModel(j);
 
                fprintf(foutDef, "ANTENNAMODEL %s\n",
                        aModel->antennaOxide()); 
 
                if (aModel->hasAPinGateArea()) {
                    for (i = 0; i < aModel->numAPinGateArea();
                       i++) {
                       fprintf(foutDef, "ANTENNAPINGATEAREA %d",
                               aModel->APinGateArea(i));
                       if (aModel->hasAPinGateAreaLayer(i))
                           fprintf(foutDef, " LAYER %s", aModel->APinGateAreaLayer(i));
                       fprintf(foutDef, "\n");
                    }
                }
                if (aModel->hasAPinMaxAreaCar()) {
                    for (i = 0;
                       i < aModel->numAPinMaxAreaCar(); i++) {
                       fprintf(foutDef, "ANTENNAPINMAXAREACAR %d",
                               aModel->APinMaxAreaCar(i));
                       if (aModel->hasAPinMaxAreaCarLayer(i))
                           fprintf(foutDef,
                               " LAYER %s", aModel->APinMaxAreaCarLayer(i));
                       fprintf(foutDef, "\n");
                    }
                }
                if (aModel->hasAPinMaxSideAreaCar()) {
                    for (i = 0;
                         i < aModel->numAPinMaxSideAreaCar(); 
                         i++) {
                       fprintf(foutDef, "ANTENNAPINMAXSIDEAREACAR %d",
                               aModel->APinMaxSideAreaCar(i));
                       if (aModel->hasAPinMaxSideAreaCarLayer(i))
                           fprintf(foutDef,
                               " LAYER %s", aModel->APinMaxSideAreaCarLayer(i));
                       fprintf(foutDef, "\n");
                    }
                }
                if (aModel->hasAPinMaxCutCar()) {
                    for (i = 0; i < aModel->numAPinMaxCutCar();
                       i++) {
                       fprintf(foutDef, "ANTENNAPINMAXCUTCAR %d",
                           aModel->APinMaxCutCar(i));
                       if (aModel->hasAPinMaxCutCarLayer(i))
                           fprintf(foutDef, " LAYER %s",
                           aModel->APinMaxCutCarLayer(i));
                       fprintf(foutDef, "\n");
                    }
                }
             }
             fprintf(foutDef, ";\n");
             --numObjs;
             if (numObjs <= 0)
                 fprintf(foutDef, "END PINS\n");
         }
         break;
  case defrDefaultCapCbkType :
         i = (long)cl;
         fprintf(foutDef, "DEFAULTCAP %d\n", i);
         numObjs = i;
         break;
  case defrRowCbkType :
         row = (defiRow*)cl;
         fprintf(foutDef, "ROW %s %s %g %g %s ", ignoreRowNames ? "XXX" : row->name(),
                 row->macro(), row->x(), row->y(),
                 orientStr(row->orient()));
         if (row->hasDo()) {
             fprintf(foutDef, "DO %g BY %g ",
                     row->xNum(), row->yNum());
             if (row->hasDoStep())
                 fprintf(foutDef, "STEP %g %g ;\n",
                         row->xStep(), row->yStep());
             else
                 fprintf(foutDef, ";\n");
         } else
            fprintf(foutDef, ";\n");
         if (row->numProps() > 0) {
            for (i = 0; i < row->numProps(); i++) {
                fprintf(foutDef, "  + PROPERTY %s %s ",
                        row->propName(i),
                        row->propValue(i));
                switch (row->propType(i)) {
                   case 'R': fprintf(foutDef, "REAL ");
                             break;
                   case 'I': fprintf(foutDef, "INTEGER ");
                             break;
                   case 'S': fprintf(foutDef, "STRING ");
                             break;
                   case 'Q': fprintf(foutDef, "QUOTESTRING ");
                             break;
                   case 'N': fprintf(foutDef, "NUMBER ");
                             break;
                }
            }
            fprintf(foutDef, ";\n");
         }
         break;
  case defrTrackCbkType :
         track = (defiTrack*)cl;
	 if (track->firstTrackMask()) {
	    if (track->sameMask()) {
		fprintf(foutDef, "TRACKS %s %g DO %g STEP %g MASK %d SAMEMASK LAYER ",
                 track->macro(), track->x(),
                 track->xNum(), track->xStep(),
	         track->firstTrackMask());
	    } else {
		fprintf(foutDef, "TRACKS %s %g DO %g STEP %g MASK %d LAYER ",
                 track->macro(), track->x(),
                 track->xNum(), track->xStep(),
	         track->firstTrackMask());
	    }
	 } else {
	    fprintf(foutDef, "TRACKS %s %g DO %g STEP %g LAYER ",
                 track->macro(), track->x(),
                 track->xNum(), track->xStep());
	 }
         for (i = 0; i < track->numLayers(); i++)
            fprintf(foutDef, "%s ", track->layer(i));
         fprintf(foutDef, ";\n"); 
         break;
  case defrGcellGridCbkType :
         gcg = (defiGcellGrid*)cl;
         fprintf(foutDef, "GCELLGRID %s %d DO %d STEP %g ;\n",
                 gcg->macro(), gcg->x(),
                 gcg->xNum(), gcg->xStep());
         break;
  case defrViaCbkType :
         via = (defiVia*)cl;
         if (testDebugPrint) {
             via->print(foutDef);
         } else {
             fprintf(foutDef, "- %s ", via->name());
             if (via->hasPattern())
                 fprintf(foutDef, "+ PATTERNNAME %s ", via->pattern());
             for (i = 0; i < via->numLayers(); i++) {
                 via->layer(i, &name, &xl, &yl, &xh, &yh);
                 int rectMask = via->rectMask(i);

                 if (rectMask) {
                     fprintf(foutDef, "+ RECT %s + MASK %d %d %d %d %d \n",
                             name, rectMask, xl, yl, xh, yh);
                 } else {
                     fprintf(foutDef, "+ RECT %s %d %d %d %d \n",
                             name, xl, yl, xh, yh);
                 }
             }
             // POLYGON
             if (via->numPolygons()) {
                 struct defiPoints points;
                 for (i = 0; i < via->numPolygons(); i++) {
                     int polyMask = via->polyMask(i);

                     if (polyMask) {
                         fprintf(foutDef, "\n  + POLYGON %s + MASK %d ", 
                                 via->polygonName(i), polyMask);
                     } else {
                         fprintf(foutDef, "\n  + POLYGON %s ", via->polygonName(i));
                     }
                     points = via->getPolygon(i);
                     for (j = 0; j < points.numPoints; j++)
                         fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
                 }
             }
             fprintf(foutDef, " ;\n");
             if (via->hasViaRule()) {
                 char *vrn, *bl, *cl, *tl;
                 int xs, ys, xcs, ycs, xbe, ybe, xte, yte;
                 int cr, cc, xo, yo, xbo, ybo, xto, yto;
                 (void)via->viaRule(&vrn, &xs, &ys, &bl, &cl, &tl, &xcs,
                                             &ycs, &xbe, &ybe, &xte, &yte);
                 fprintf(foutDef, "+ VIARULE '%s'\n", ignoreViaNames ? "XXX" : vrn);
                 fprintf(foutDef, "  + CUTSIZE %d %d\n", xs, ys);
                 fprintf(foutDef, "  + LAYERS %s %s %s\n", bl, cl, tl);
                 fprintf(foutDef, "  + CUTSPACING %d %d\n", xcs, ycs);
                 fprintf(foutDef, "  + ENCLOSURE %d %d %d %d\n", xbe, ybe, xte, yte);
                 if (via->hasRowCol()) {
                    (void)via->rowCol(&cr, &cc);
                    fprintf(foutDef, "  + ROWCOL %d %d\n", cr, cc);
                 }
                 if (via->hasOrigin()) {
                    (void)via->origin(&xo, &yo);
                    fprintf(foutDef, "  + ORIGIN %d %d\n", xo, yo);
                 }
                 if (via->hasOffset()) {
                    (void)via->offset(&xbo, &ybo, &xto, &yto);
                    fprintf(foutDef, "  + OFFSET %d %d %d %d\n", xbo, ybo, xto, yto);
                 }
                 if (via->hasCutPattern())
                    fprintf(foutDef, "  + PATTERN '%s'\n", via->cutPattern());
             }
             --numObjs;
             if (numObjs <= 0)
                 fprintf(foutDef, "END VIAS\n");
         }
         break;
  case defrRegionCbkType :
         re = (defiRegion*)cl;
         fprintf(foutDef, "- %s ", re->name());
         for (i = 0; i < re->numRectangles(); i++)
             fprintf(foutDef, "%d %d %d %d \n", re->xl(i),
                     re->yl(i), re->xh(i),
                     re->yh(i));
         if (re->hasType())
             fprintf(foutDef, "+ TYPE %s\n", re->type());
         if (re->numProps()) {
             for (i = 0; i < re->numProps(); i++) {
                 fprintf(foutDef, "+ PROPERTY %s %s ", re->propName(i),
                         re->propValue(i));
                 switch (re->propType(i)) {
                    case 'R': fprintf(foutDef, "REAL ");
                              break;
                    case 'I': fprintf(foutDef, "INTEGER ");
                              break;
                    case 'S': fprintf(foutDef, "STRING ");
                              break;
                    case 'Q': fprintf(foutDef, "QUOTESTRING ");
                              break;
                    case 'N': fprintf(foutDef, "NUMBER ");
                              break;
                 }
             }
         }
         fprintf(foutDef, ";\n"); 
         --numObjs;
         if (numObjs <= 0) {
             fprintf(foutDef, "END REGIONS\n");
         }
         break;
  case defrGroupNameCbkType :
         if ((char*)cl) {
             fprintf(foutDef, "- %s", (char*)cl);
         }
         break;
  case defrGroupMemberCbkType :
         if ((char*)cl) {
             fprintf(foutDef, " %s", (char*)cl);
         }
         break;
  case defrComponentMaskShiftLayerCbkType :
	 fprintf(foutDef, "COMPONENTMASKSHIFT ");
        
        for (i = 0; i < maskShiftLayer->numMaskShiftLayers(); i++) {
           fprintf(foutDef, "%s ", maskShiftLayer->maskShiftLayer(i));
        } 
        fprintf(foutDef, ";\n");
	 break;
  case defrGroupCbkType :
         group = (defiGroup*)cl;
         if (group->hasMaxX() | group->hasMaxY()
             | group->hasPerim()) {
             fprintf(foutDef, "\n  + SOFT ");
             if (group->hasPerim()) 
                 fprintf(foutDef, "MAXHALFPERIMETER %d ",
                         group->perim());
             if (group->hasMaxX())
                 fprintf(foutDef, "MAXX %d ", group->maxX());
             if (group->hasMaxY()) 
                 fprintf(foutDef, "MAXY %d ", group->maxY());
         } 
         if (group->hasRegionName())
             fprintf(foutDef, "\n  + REGION %s ", group->regionName());
         if (group->hasRegionBox()) {
             int *gxl, *gyl, *gxh, *gyh;
             int size;
             group->regionRects(&size, &gxl, &gyl, &gxh, &gyh);
             for (i = 0; i < size; i++)
                 fprintf(foutDef, "REGION %d %d %d %d ", gxl[i], gyl[i],
                         gxh[i], gyh[i]);
         }
         if (group->numProps()) {
             for (i = 0; i < group->numProps(); i++) {
                 fprintf(foutDef, "\n  + PROPERTY %s %s ",
                         group->propName(i),
                         group->propValue(i));
                 switch (group->propType(i)) {
                    case 'R': fprintf(foutDef, "REAL ");
                              break;
                    case 'I': fprintf(foutDef, "INTEGER ");
                              break;
                    case 'S': fprintf(foutDef, "STRING ");
                              break;
                    case 'Q': fprintf(foutDef, "QUOTESTRING ");
                              break;
                    case 'N': fprintf(foutDef, "NUMBER ");
                              break;
                 }
             }
         }
         fprintf(foutDef, " ;\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END GROUPS\n");
         break;
  case defrScanchainCbkType :
         sc = (defiScanchain*)cl;
         fprintf(foutDef, "- %s\n", sc->name());
         if (sc->hasStart()) {
             sc->start(&a1, &b1);
             fprintf(foutDef, "  + START %s %s\n", a1, b1);
         }
         if (sc->hasStop()) {
             sc->stop(&a1, &b1);
             fprintf(foutDef, "  + STOP %s %s\n", a1, b1);
         }
         if (sc->hasCommonInPin() ||
             sc->hasCommonOutPin()) {
             fprintf(foutDef, "  + COMMONSCANPINS ");
             if (sc->hasCommonInPin())
                fprintf(foutDef, " ( IN %s ) ", sc->commonInPin());
             if (sc->hasCommonOutPin())
                fprintf(foutDef, " ( OUT %s ) ",sc->commonOutPin());
             fprintf(foutDef, "\n");
         }
         if (sc->hasFloating()) {
            sc->floating(&size, &inst, &inPin, &outPin, &bits);
            if (size > 0)
                fprintf(foutDef, "  + FLOATING\n");
            for (i = 0; i < size; i++) {
                fprintf(foutDef, "    %s ", inst[i]);
                if (inPin[i])
                   fprintf(foutDef, "( IN %s ) ", inPin[i]);
                if (outPin[i])
                   fprintf(foutDef, "( OUT %s ) ", outPin[i]);
                if (bits[i] != -1)
                   fprintf(foutDef, "( BITS %d ) ", bits[i]);
                fprintf(foutDef, "\n");
            }
         }

         if (sc->hasOrdered()) {
            for (i = 0; i < sc->numOrderedLists(); i++) {
                sc->ordered(i, &size, &inst, &inPin, &outPin,
                                           &bits);
                if (size > 0)
                    fprintf(foutDef, "  + ORDERED\n");
                for (j = 0; j < size; j++) {
                    fprintf(foutDef, "    %s ", inst[j]); 
                    if (inPin[j])
                       fprintf(foutDef, "( IN %s ) ", inPin[j]);
                    if (outPin[j])
                       fprintf(foutDef, "( OUT %s ) ", outPin[j]);
                    if (bits[j] != -1)
                       fprintf(foutDef, "( BITS %d ) ", bits[j]);
                    fprintf(foutDef, "\n");
                }
            }
         }

         if (sc->hasPartition()) {
            fprintf(foutDef, "  + PARTITION %s ",
                    sc->partitionName());
            if (sc->hasPartitionMaxBits())
              fprintf(foutDef, "MAXBITS %d ",
                      sc->partitionMaxBits());
         }
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END SCANCHAINS\n");
         break;
  case defrIOTimingCbkType :
         iot = (defiIOTiming*)cl;
         fprintf(foutDef, "- ( %s %s )\n", iot->inst(),
                 iot->pin());
         if (iot->hasSlewRise())
             fprintf(foutDef, "  + RISE SLEWRATE %g %g\n",
                     iot->slewRiseMin(),
                     iot->slewRiseMax());
         if (iot->hasSlewFall())
             fprintf(foutDef, "  + FALL SLEWRATE %g %g\n",
                     iot->slewFallMin(),
                     iot->slewFallMax());
         if (iot->hasVariableRise())
             fprintf(foutDef, "  + RISE VARIABLE %g %g\n",
                     iot->variableRiseMin(),
                     iot->variableRiseMax());
         if (iot->hasVariableFall())
             fprintf(foutDef, "  + FALL VARIABLE %g %g\n",
                     iot->variableFallMin(),
                     iot->variableFallMax());
         if (iot->hasCapacitance())
             fprintf(foutDef, "  + CAPACITANCE %g\n",
                     iot->capacitance());
         if (iot->hasDriveCell()) {
             fprintf(foutDef, "  + DRIVECELL %s ",
                     iot->driveCell());
             if (iot->hasFrom())
                 fprintf(foutDef, "  FROMPIN %s ",
                         iot->from());
             if (iot->hasTo())
                 fprintf(foutDef, "  TOPIN %s ",
                         iot->to());
             if (iot->hasParallel())
                 fprintf(foutDef, "PARALLEL %g",
                         iot->parallel());
             fprintf(foutDef, "\n");
         }
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END IOTIMINGS\n");
         break;
  case defrFPCCbkType :
         fpc = (defiFPC*)cl;
         fprintf(foutDef, "- %s ", fpc->name());
         if (fpc->isVertical())
             fprintf(foutDef, "VERTICAL ");
         if (fpc->isHorizontal())
             fprintf(foutDef, "HORIZONTAL ");
         if (fpc->hasAlign())
             fprintf(foutDef, "ALIGN ");
         if (fpc->hasMax())
             fprintf(foutDef, "%g ", fpc->alignMax());
         if (fpc->hasMin())
             fprintf(foutDef, "%g ", fpc->alignMin());
         if (fpc->hasEqual())
             fprintf(foutDef, "%g ", fpc->equal());
         for (i = 0; i < fpc->numParts(); i++) {
             fpc->getPart(i, &corner, &typ, &name);
             if (corner == 'B')
                 fprintf(foutDef, "BOTTOMLEFT ");
             else
                 fprintf(foutDef, "TOPRIGHT ");
             if (typ == 'R')
                 fprintf(foutDef, "ROWS %s ", name);
             else
                 fprintf(foutDef, "COMPS %s ", name);
         }
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END FLOORPLANCONSTRAINTS\n");
         break;
  case defrTimingDisableCbkType :
         td = (defiTimingDisable*)cl;
         if (td->hasFromTo())
             fprintf(foutDef, "- FROMPIN %s %s ",
                     td->fromInst(),
                     td->fromPin(),
                     td->toInst(),
                     td->toPin());
         if (td->hasThru())
             fprintf(foutDef, "- THRUPIN %s %s ",
                     td->thruInst(),
                     td->thruPin());
         if (td->hasMacroFromTo())
             fprintf(foutDef, "- MACRO %s FROMPIN %s %s ",
                     td->macroName(),
                     td->fromPin(),
                     td->toPin());
         if (td->hasMacroThru())
             fprintf(foutDef, "- MACRO %s THRUPIN %s %s ",
                     td->macroName(),
                     td->fromPin());
         fprintf(foutDef, ";\n");
         break;
  case defrPartitionCbkType :
         part = (defiPartition*)cl;
         fprintf(foutDef, "- %s ", part->name());
         if (part->isSetupRise() |
             part->isSetupFall() |
             part->isHoldRise() |
             part->isHoldFall()) {
             // has turnoff 
             fprintf(foutDef, "TURNOFF "); 
             if (part->isSetupRise())
                 fprintf(foutDef, "SETUPRISE "); 
             if (part->isSetupFall())
                 fprintf(foutDef, "SETUPFALL "); 
             if (part->isHoldRise())
                 fprintf(foutDef, "HOLDRISE "); 
             if (part->isHoldFall())
                 fprintf(foutDef, "HOLDFALL "); 
         }
         itemT = part->itemType();
         dir = part->direction();
         if (strcmp(itemT, "CLOCK") == 0) {
             if (dir == 'T')    // toclockpin
                 fprintf(foutDef, "+ TOCLOCKPIN %s %s ",
                         part->instName(),
                         part->pinName());
             if (dir == 'F')    // fromclockpin
                 fprintf(foutDef, "+ FROMCLOCKPIN %s %s ",
                         part->instName(),
                         part->pinName());
             if (part->hasMin())
                 fprintf(foutDef, "MIN %g %g ",
                         part->partitionMin(),
                         part->partitionMax());
             if (part->hasMax())
                 fprintf(foutDef, "MAX %g %g ",
                         part->partitionMin(),
                         part->partitionMax());
             fprintf(foutDef, "PINS ");
             for (i = 0; i < part->numPins(); i++)
                  fprintf(foutDef, "%s ", part->pin(i));
         } else if (strcmp(itemT, "IO") == 0) {
             if (dir == 'T')    // toiopin
                 fprintf(foutDef, "+ TOIOPIN %s %s ",
                         part->instName(),
                         part->pinName());
             if (dir == 'F')    // fromiopin
                 fprintf(foutDef, "+ FROMIOPIN %s %s ",
                         part->instName(),
                         part->pinName());
         } else if (strcmp(itemT, "COMP") == 0) {
             if (dir == 'T')    // tocomppin
                 fprintf(foutDef, "+ TOCOMPPIN %s %s ",
                         part->instName(),
                         part->pinName());
             if (dir == 'F')    // fromcomppin
                 fprintf(foutDef, "+ FROMCOMPPIN %s %s ",
                         part->instName(),
                         part->pinName());
         }
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END PARTITIONS\n");
         break;

  case defrPinPropCbkType :
         pprop = (defiPinProp*)cl;
         if (pprop->isPin())
            fprintf(foutDef, "- PIN %s ", pprop->pinName());
         else 
            fprintf(foutDef, "- %s %s ",
                    pprop->instName(),
                    pprop->pinName());
         fprintf(foutDef, ";\n");
         if (pprop->numProps() > 0) {
            for (i = 0; i < pprop->numProps(); i++) {
                fprintf(foutDef, "  + PROPERTY %s %s ",
                        pprop->propName(i),
                        pprop->propValue(i));
                switch (pprop->propType(i)) {
                   case 'R': fprintf(foutDef, "REAL ");
                             break;
                   case 'I': fprintf(foutDef, "INTEGER ");
                             break;
                   case 'S': fprintf(foutDef, "STRING ");
                             break;
                   case 'Q': fprintf(foutDef, "QUOTESTRING ");
                             break;
                   case 'N': fprintf(foutDef, "NUMBER ");
                             break;
                }
            }
            fprintf(foutDef, ";\n");
         }
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END PINPROPERTIES\n");
         break;
  case defrBlockageCbkType :
         block = (defiBlockage*)cl;
         if (testDebugPrint) {
             block->print(foutDef);
         } else {
             if (block->hasLayer()) {
                fprintf(foutDef, "- LAYER %s\n", block->layerName());
                if (block->hasComponent())
                   fprintf(foutDef, "   + COMPONENT %s\n",
                           block->layerComponentName());
                if (block->hasSlots())
                   fprintf(foutDef, "   + SLOTS\n");
                if (block->hasFills())
                   fprintf(foutDef, "   + FILLS\n");
                if (block->hasPushdown())
                   fprintf(foutDef, "   + PUSHDOWN\n");
                if (block->hasExceptpgnet())
                   fprintf(foutDef, "   + EXCEPTPGNET\n");
                if (block->hasMask())
                   fprintf(foutDef, "   + MASK %d\n", block->mask());
                if (block->hasSpacing())
                   fprintf(foutDef, "   + SPACING %d\n",
                           block->minSpacing());
                if (block->hasDesignRuleWidth())
                   fprintf(foutDef, "   + DESIGNRULEWIDTH %d\n",
                           block->designRuleWidth());
             }
             else if (block->hasPlacement()) {
                fprintf(foutDef, "- PLACEMENT\n");
                if (block->hasSoft())
                   fprintf(foutDef, "   + SOFT\n");
                if (block->hasPartial())
                   fprintf(foutDef, "   + PARTIAL %g\n",
                           block->placementMaxDensity());
                if (block->hasComponent())
                   fprintf(foutDef, "   + COMPONENT %s\n",
                           block->placementComponentName());
                if (block->hasPushdown())
                   fprintf(foutDef, "   + PUSHDOWN\n");
             }

             for (i = 0; i < block->numRectangles(); i++) {
                fprintf(foutDef, "   RECT %d %d %d %d\n", 
                        block->xl(i), block->yl(i),
                        block->xh(i), block->yh(i));
             } 

             for (i = 0; i < block->numPolygons(); i++) {
                fprintf(foutDef, "   POLYGON ");
                points = block->getPolygon(i);
                for (j = 0; j < points.numPoints; j++)
                   fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
                fprintf(foutDef, "\n");
             }
             fprintf(foutDef, ";\n");
             --numObjs;
             if (numObjs <= 0)
                 fprintf(foutDef, "END BLOCKAGES\n");
         }
         break;
  case defrSlotCbkType :
         slots = (defiSlot*)cl;
         if (slots->hasLayer())
            fprintf(foutDef, "- LAYER %s\n", slots->layerName());

         for (i = 0; i < slots->numRectangles(); i++) {
            fprintf(foutDef, "   RECT %d %d %d %d\n", 
                    slots->xl(i), slots->yl(i),
                    slots->xh(i), slots->yh(i));
         } 
         for (i = 0; i < slots->numPolygons(); i++) {
            fprintf(foutDef, "   POLYGON ");
            points = slots->getPolygon(i);
            for (j = 0; j < points.numPoints; j++)
              fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
            fprintf(foutDef, ";\n");
         }
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END SLOTS\n");
         break;
  case defrFillCbkType :
         fills = (defiFill*)cl;
         if (testDebugPrint) {
              fills->print(foutDef);
         } else {
             if (fills->hasLayer()) {
                fprintf(foutDef, "- LAYER %s", fills->layerName());
                if (fills->layerMask()) {
                    fprintf(foutDef, " + MASK %d", fills->layerMask());
                }
                if (fills->hasLayerOpc())
                   fprintf(foutDef, " + OPC");
                fprintf(foutDef, "\n");

                for (i = 0; i < fills->numRectangles(); i++) {
                   fprintf(foutDef, "   RECT %d %d %d %d\n", 
                           fills->xl(i), fills->yl(i),
                           fills->xh(i), fills->yh(i));
                } 
                for (i = 0; i < fills->numPolygons(); i++) {
                   fprintf(foutDef, "   POLYGON "); 
                   points = fills->getPolygon(i);
                   for (j = 0; j < points.numPoints; j++)
                     fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
                   fprintf(foutDef, ";\n");
                } 
                fprintf(foutDef, ";\n");
             }
             --numObjs;
             if (fills->hasVia()) {
                fprintf(foutDef, "- VIA %s", fills->viaName());
                if (fills->viaTopMask() || fills->viaCutMask()
                    || fills->viaBottomMask()) {
                    fprintf(foutDef, " + MASK %d%d%d", 
                    fills->viaTopMask(),
                           fills->viaCutMask(),
                           fills->viaBottomMask());
                }
                if (fills->hasViaOpc())
                   fprintf(foutDef, " + OPC");
                fprintf(foutDef, "\n");

                for (i = 0; i < fills->numViaPts(); i++) {
                   points = fills->getViaPts(i);
                   for (j = 0; j < points.numPoints; j++)
                      fprintf(foutDef, " %d %d", points.x[j], points.y[j]);
                   fprintf(foutDef, ";\n"); 
                }
                fprintf(foutDef, ";\n");
             }
             if (numObjs <= 0)
                 fprintf(foutDef, "END FILLS\n");
         }
         break;
  case defrStylesCbkType :
         struct defiPoints points;
         styles = (defiStyles*)cl;
         fprintf(foutDef, "- STYLE %d ", styles->style());
         points = styles->getPolygon();
         for (j = 0; j < points.numPoints; j++)
            fprintf(foutDef, "%d %d ", points.x[j], points.y[j]);
         fprintf(foutDef, ";\n");
         --numObjs;
         if (numObjs <= 0)
             fprintf(foutDef, "END STYLES\n");
         break;

  default: fprintf(foutDef, "BOGUS callback to cls.\n"); return 1;
  }
  return 0;
}


static int dn(defrCallbackType_e c, const char* h, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "DIVIDERCHAR \"%s\" ;\n",h);
  return 0;
}


static int ext(defrCallbackType_e t, const char* c, defiUserData ud) {
  char* name;

  checkType(t);
  if (ud != userDataDef) dataError();

  switch (t) {
  case defrNetExtCbkType : name = address("net"); break;
  case defrComponentExtCbkType : name = address("component"); break;
  case defrPinExtCbkType : name = address("pin"); break;
  case defrViaExtCbkType : name = address("via"); break;
  case defrNetConnectionExtCbkType : name = address("net connection"); break;
  case defrGroupExtCbkType : name = address("group"); break;
  case defrScanChainExtCbkType : name = address("scanchain"); break;
  case defrIoTimingsExtCbkType : name = address("io timing"); break;
  case defrPartitionsExtCbkType : name = address("partition"); break;
  default: name = address("BOGUS"); return 1;
  }
  fprintf(foutDef, "  %s extension %s\n", name, c);
  return 0;
}

static int extension(defrCallbackType_e c, const char* extsn, defiUserData ud) {
  checkType(c);
  if (ud != userDataDef) dataError();
  fprintf(foutDef, "BEGINEXT %s\n", extsn);
  return 0;
}

static void* mallocCB(size_t size) {
  return malloc(size);
}

static void* reallocCB(void* name, size_t size) {
  return realloc(name, size);
}

static void freeCB(void* name) {
  free(name);
  return;
}


// BEGIN_LEFDEF_PARSER_NAMESPACE
// extern long long nlines;
// END_LEFDEF_PARSER_NAMESPACE
// static int ccr1131444 = 0;


static void lineNumberCB(long long lineNo) {

    // The CCR 1131444 tests ability of the DEF parser to count 
    // input line numbers out of 32-bit int range. On the first callback 
    // call 10G lines will be added to line counter. It should be 
    // reflected in output.
    if (ccr1131444) {
        lineNo += 10000000000LL;
        defrSetNLines(lineNo);
        ccr1131444 = 0;
    }

#ifdef _WIN32
  fprintf(foutDef, "Parsed %I64d number of lines!!\n", lineNo);
#else 
  fprintf(foutDef, "Parsed %lld number of lines!!\n", lineNo);
#endif
}

static int unUsedCB(defrCallbackType_e, void*, defiUserData) {
  fprintf(foutDef, "This callback is not used.\n");
  return 0;
}

static void printWarning(const char *str)
{
    fprintf(stderr, "%s\n", str);
}
// ----------------------------------------------------------------
//                    End Def Parser
// -----------------------------------------------------------------



    int  init(int argc, char** argv){

        int num = 99;
        char* inFile[6];
        char* outFile;
        FILE* f;
        int res;
        int noCalls = 0;
        //  long start_mem;
        int retStr = 0;
        int numInFile = 0;
        int fileCt = 0;
        int test1 = 0;
        int test2 = 0;
        int noNetCb = 0;
        int ccr749853 = 0;
        int line_num_print_interval = 50;

        #ifdef WIN32
            // Enable two-digit exponent format
            _set_output_format(_TWO_DIGIT_EXPONENT);
        #endif

        //  start_mem = (long)sbrk(0);

        strcpy(defaultNameDef, "def.in");
        strcpy(defaultOutDef, "list");
        inFile[0] = defaultNameDef;
        outFile = defaultOutDef;
        foutDef = stdout;
        userDataDef = (void*) 0x01020304;
        argc--;
        argv++;

        if (argc == 0) {
            fprintf(stderr, "Type 'defrw --help' for the help.\n");
            return 2;
        }

        while (argc--) {
            if (strcmp(*argv, "-d") == 0) {
            argv++;
            argc--;
            sscanf(*argv, "%d", &num);
            defiSetDebug(num, 1);
            } else if (strcmp(*argv, "-nc") == 0) {
            noCalls = 1;
            } else if (strcmp(*argv, "-o") == 0) {
            argv++;
            argc--;
            outFile = *argv;
            if ((foutDef = fopen(outFile, "w")) == 0) {
                    fprintf(stderr, "ERROR: could not open output file\n");
                    return 2;
                }
            } else if (strcmp(*argv, "-verStr") == 0) {
                /* New to set the version callback routine to return a string    */
                /* instead of double.                                            */
                retStr = 1;
            } else if (strcmp(*argv, "-i") == 0) {
                argv++;
                argc--;
                line_num_print_interval = atoi(*argv);
            } else if (strcmp(*argv, "-test1") == 0) {
            test1 = 1;
            } else if (strcmp(*argv, "-test2") == 0) {
            test2 = 1;
            } else if (strcmp(*argv, "-noNet") == 0) {
            noNetCb = 1;
            } else if (strcmp(*argv, "-ccr749853") == 0) {
            ccr749853 = 1;
            } else if (strcmp(*argv, "-ccr1131444") == 0) {
                ccr1131444 = 1;
            } else if (strcmp(*argv, "-testDebugPrint") == 0) {
                testDebugPrint = 1;
            } else if (strcmp(*argv, "-sessionless") == 0) {
                isSessionless = 1;
            } else if (strcmp(*argv, "-ignoreRowNames") == 0) {
                ignoreRowNames = 1;
            } else if (strcmp(*argv, "-ignoreViaNames") == 0) {
                ignoreViaNames = 1;
            } else if (argv[0][0] != '-') {
            if (numInFile >= 6) {
                fprintf(stderr, "ERROR: too many input files, max = 6.\n");
                return 2;
            }
            inFile[numInFile++] = *argv;
            } else if ((strcmp(*argv, "-h") == 0) || (strcmp(*argv, "--help") == 0)) {
            fprintf(stderr, "Usage: defrw (<option>|<file>)* \n");
            fprintf(stderr, "Files:\n");
            fprintf(stderr, "\tupto 6 DEF files many be supplied.\n");
            fprintf(stderr, "\tif <defFileName> is set to 'STDIN' - takes data from stdin.\n");
            fprintf(stderr, "Options:\n");
            fprintf(stderr, "\t-i <num_lines> -- sets processing msg interval (default: 50 lines).\n");
            fprintf(stderr, "\t-nc            -- no functional callbacks will be called.\n");
            fprintf(stderr, "\t-o <out_file>  -- write output to the file.\n");
            fprintf(stderr, "\t-ignoreRowNames   -- don't output row names.\n");
            fprintf(stderr, "\t-ignoreViaNames   -- don't output via names.\n");
            return 2;
            } else if (strcmp(*argv, "-setSNetWireCbk") == 0) {
            setSNetWireCbk = 1;
            } else {
            fprintf(stderr, "ERROR: Illegal command line option: '%s'\n", *argv);
            return 2;
            }

            argv++;
        }

        //defrSetLogFunction(myLogFunction);
        //defrSetWarningLogFunction(myWarningLogFunction);

        if (isSessionless) {
            defrInitSession(0);
            defrSetLongLineNumberFunction(lineNumberCB);
            defrSetDeltaNumberLines(line_num_print_interval);
        }

        defrInitSession(isSessionless ? 0 : 1);

        if (noCalls == 0) {

            defrSetWarningLogFunction(printWarning);


            defrSetUserData((void*)3);
            defrSetDesignCbk(dname);
            defrSetTechnologyCbk(tname);
            defrSetExtensionCbk(extension);
            defrSetDesignEndCbk(done);
            defrSetPropDefStartCbk(propstart);
            defrSetPropCbk(prop);
            defrSetPropDefEndCbk(propend);
            /* Test for CCR 766289*/
            if (!noNetCb)
            defrSetNetCbk(netf);
            defrSetNetNameCbk(netNamef);
            defrSetNetNonDefaultRuleCbk(nondefRulef);
            defrSetNetSubnetNameCbk(subnetNamef);
            defrSetNetPartialPathCbk(netpath);
            defrSetSNetCbk(snetf);
            defrSetSNetPartialPathCbk(snetpath);
            if (setSNetWireCbk)
            defrSetSNetWireCbk(snetwire);
            defrSetComponentMaskShiftLayerCbk(compMSL);
            defrSetComponentCbk(compf);
            defrSetAddPathToNet();
            defrSetHistoryCbk(hist);
            defrSetConstraintCbk(constraint);
            defrSetAssertionCbk(constraint);
            defrSetArrayNameCbk(an);
            defrSetFloorPlanNameCbk(fn);
            defrSetDividerCbk(dn);
            defrSetBusBitCbk(bbn);
            defrSetNonDefaultCbk(ndr);

            defrSetAssertionsStartCbk(constraintst);
            defrSetConstraintsStartCbk(constraintst);
            defrSetComponentStartCbk(cs);
            defrSetPinPropStartCbk(cs);
            defrSetNetStartCbk(cs);
            defrSetStartPinsCbk(cs);
            defrSetViaStartCbk(cs);
            defrSetRegionStartCbk(cs);
            defrSetSNetStartCbk(cs);
            defrSetGroupsStartCbk(cs);
            defrSetScanchainsStartCbk(cs);
            defrSetIOTimingsStartCbk(cs);
            defrSetFPCStartCbk(cs);
            defrSetTimingDisablesStartCbk(cs);
            defrSetPartitionsStartCbk(cs);
            defrSetBlockageStartCbk(cs);
            defrSetSlotStartCbk(cs);
            defrSetFillStartCbk(cs);
            defrSetNonDefaultStartCbk(cs);
            defrSetStylesStartCbk(cs);

            // All of the extensions point to the same function.
            defrSetNetExtCbk(ext);
            defrSetComponentExtCbk(ext);
            defrSetPinExtCbk(ext);
            defrSetViaExtCbk(ext);
            defrSetNetConnectionExtCbk(ext);
            defrSetGroupExtCbk(ext);
            defrSetScanChainExtCbk(ext);
            defrSetIoTimingsExtCbk(ext);
            defrSetPartitionsExtCbk(ext);

            defrSetUnitsCbk(units);
            if (!retStr)
            defrSetVersionCbk(vers);
            else
            defrSetVersionStrCbk(versStr);
            defrSetCaseSensitiveCbk(casesens);

            // The following calls are an example of using one function "cls"
            // to be the callback for many DIFFERENT types of constructs.
            // We have to cast the function type to meet the requirements
            // of each different set function.
            defrSetSiteCbk((defrSiteCbkFnType)cls);
            defrSetCanplaceCbk((defrSiteCbkFnType)cls);
            defrSetCannotOccupyCbk((defrSiteCbkFnType)cls);
            defrSetDieAreaCbk((defrBoxCbkFnType)cls);
            defrSetPinCapCbk((defrPinCapCbkFnType)cls);
            defrSetPinCbk((defrPinCbkFnType)cls);
            defrSetPinPropCbk((defrPinPropCbkFnType)cls);
            defrSetDefaultCapCbk((defrIntegerCbkFnType)cls);
            defrSetRowCbk((defrRowCbkFnType)cls);
            defrSetTrackCbk((defrTrackCbkFnType)cls);
            defrSetGcellGridCbk((defrGcellGridCbkFnType)cls);
            defrSetViaCbk((defrViaCbkFnType)cls);
            defrSetRegionCbk((defrRegionCbkFnType)cls);
            defrSetGroupNameCbk((defrStringCbkFnType)cls);
            defrSetGroupMemberCbk((defrStringCbkFnType)cls);
            defrSetGroupCbk((defrGroupCbkFnType)cls);
            defrSetScanchainCbk((defrScanchainCbkFnType)cls);
            defrSetIOTimingCbk((defrIOTimingCbkFnType)cls);
            defrSetFPCCbk((defrFPCCbkFnType)cls);
            defrSetTimingDisableCbk((defrTimingDisableCbkFnType)cls);
            defrSetPartitionCbk((defrPartitionCbkFnType)cls);
            defrSetBlockageCbk((defrBlockageCbkFnType)cls);
            defrSetSlotCbk((defrSlotCbkFnType)cls);
            defrSetFillCbk((defrFillCbkFnType)cls);
            defrSetStylesCbk((defrStylesCbkFnType)cls);

            defrSetAssertionsEndCbk(endfunc);
            defrSetComponentEndCbk(endfunc);
            defrSetConstraintsEndCbk(endfunc);
            defrSetNetEndCbk(endfunc);
            defrSetFPCEndCbk(endfunc);
            defrSetFPCEndCbk(endfunc);
            defrSetGroupsEndCbk(endfunc);
            defrSetIOTimingsEndCbk(endfunc);
            defrSetNetEndCbk(endfunc);
            defrSetPartitionsEndCbk(endfunc);
            defrSetRegionEndCbk(endfunc);
            defrSetSNetEndCbk(endfunc);
            defrSetScanchainsEndCbk(endfunc);
            defrSetPinEndCbk(endfunc);
            defrSetTimingDisablesEndCbk(endfunc);
            defrSetViaEndCbk(endfunc);
            defrSetPinPropEndCbk(endfunc);
            defrSetBlockageEndCbk(endfunc);
            defrSetSlotEndCbk(endfunc);
            defrSetFillEndCbk(endfunc);
            defrSetNonDefaultEndCbk(endfunc);
            defrSetStylesEndCbk(endfunc);

            defrSetMallocFunction(mallocCB);
            defrSetReallocFunction(reallocCB);
            defrSetFreeFunction(freeCB);

            //defrSetRegisterUnusedCallbacks();

            // Testing to set the number of warnings
            defrSetAssertionWarnings(3);
            defrSetBlockageWarnings(3);
            defrSetCaseSensitiveWarnings(3);
            defrSetComponentWarnings(3);
            defrSetConstraintWarnings(0);
            defrSetDefaultCapWarnings(3);
            defrSetGcellGridWarnings(3);
            defrSetIOTimingWarnings(3);
            defrSetNetWarnings(3);
            defrSetNonDefaultWarnings(3);
            defrSetPinExtWarnings(3);
            defrSetPinWarnings(3);
            defrSetRegionWarnings(3);
            defrSetRowWarnings(3);
            defrSetScanchainWarnings(3);
            defrSetSNetWarnings(3);
            defrSetStylesWarnings(3);
            defrSetTrackWarnings(3);
            defrSetUnitsWarnings(3);
            defrSetVersionWarnings(3);
            defrSetViaWarnings(3);
        }

        if (! isSessionless) {
            defrSetLongLineNumberFunction(lineNumberCB);
            defrSetDeltaNumberLines(line_num_print_interval);
        }

        (void) defrSetOpenLogFileAppend();

        if (ccr749853) {
            defrSetTotalMsgLimit (5);
            defrSetLimitPerMsg (6008, 2);

        } 
        
        if (test1) {  // for special tests
            for (fileCt = 0; fileCt < numInFile; fileCt++) {
                
            if ((f = fopen(inFile[fileCt],"r")) == 0) {
                fprintf(stderr,"Couldn't open input file '%s'\n", inFile[fileCt]);
                return(2);
            }
            // Set case sensitive to 0 to start with, in History & PropertyDefinition
            // reset it to 1.
            res = defrRead(f, inFile[fileCt], userDataDef, 1);

            if (res)
                fprintf(stderr, "Reader returns bad status.\n", inFile[fileCt]);

            (void)defrPrintUnusedCallbacks(foutDef);
            (void)defrReleaseNResetMemory();
            (void)defrUnsetNonDefaultCbk(); 
            (void)defrUnsetNonDefaultStartCbk(); 
            (void)defrUnsetNonDefaultEndCbk(); 
            }
        }
        else if (test2) {  // for special tests
            // this test is design to test the 3 APIs, defrDisableParserMsgs,
            // defrEnableParserMsgs & defrEnableAllMsgs
            // It uses the file ccr523879.def.  This file will parser 3 times
            // 1st it will have defrDisableParserMsgs set to both 7010 & 7016
            // 2nd will enable 7016 by calling defrEnableParserMsgs
            // 3rd enable all msgs by call defrEnableAllMsgs

            int nMsgs = 2;
            int dMsgs[2];

            for (fileCt = 0; fileCt < numInFile; fileCt++) {
            if (fileCt == 0) {
                dMsgs[0] = 7010;
                dMsgs[1] = 7016;
                defrDisableParserMsgs (2, (int*)dMsgs);
            } else if (fileCt == 1) {
                dMsgs[0] = 7016;
                defrEnableParserMsgs (1, (int*)dMsgs);
            } else
                defrEnableAllMsgs();
        
            if ((f = fopen(inFile[fileCt],"r")) == 0) {
                fprintf(stderr,"Couldn't open input file '%s'\n", inFile[fileCt]);
                return(2);
            }
        
            res = defrRead(f, inFile[fileCt], userDataDef, 1);
        
            if (res)
                fprintf(stderr, "Reader returns bad status.\n", inFile[fileCt]);
        
            (void)defrPrintUnusedCallbacks(foutDef);
            (void)defrReleaseNResetMemory();
            (void)defrUnsetNonDefaultCbk();
            (void)defrUnsetNonDefaultStartCbk();
            (void)defrUnsetNonDefaultEndCbk();
            }
        } else {
            for (fileCt = 0; fileCt < numInFile; fileCt++) {
            if (strcmp(inFile[fileCt], "STDIN") == 0) {
                    f = stdin;
            } else if ((f = fopen(inFile[fileCt],"r")) == 0) {
                    fprintf(stderr,"Couldn't open input file '%s'\n", inFile[fileCt]);
                    return(2);
            }
            // Set case sensitive to 0 to start with, in History & PropertyDefinition
            // reset it to 1.

            res = defrRead(f, inFile[fileCt], userDataDef, 1);

            if (res)
                fprintf(stderr, "Reader returns bad status.\n", inFile[fileCt]);

            // Testing the aliases API.
            defrAddAlias ("alias1", "aliasValue1", 1);

            defiAlias_itr aliasStore;
            const char    *alias1Value = NULL;

            while (aliasStore.Next()) {
                if (strcmp(aliasStore.Key(), "alias1") == 0) {
                        alias1Value = aliasStore.Data();
                }
            } 

            if (!alias1Value || strcmp(alias1Value, "aliasValue1")) {
                    fprintf(stderr, "ERROR: Aliases don't work\n");
            }

            (void)defrPrintUnusedCallbacks(foutDef);
            (void)defrReleaseNResetMemory();
            }
            (void)defrUnsetCallbacks();
            (void)defrSetUnusedCallbacks(unUsedCB);
        }

        // Unset all the callbacks
        defrUnsetArrayNameCbk ();
        defrUnsetAssertionCbk ();
        defrUnsetAssertionsStartCbk ();
        defrUnsetAssertionsEndCbk ();
        defrUnsetBlockageCbk ();
        defrUnsetBlockageStartCbk ();
        defrUnsetBlockageEndCbk ();
        defrUnsetBusBitCbk ();
        defrUnsetCannotOccupyCbk ();
        defrUnsetCanplaceCbk ();
        defrUnsetCaseSensitiveCbk ();
        defrUnsetComponentCbk ();
        defrUnsetComponentExtCbk ();
        defrUnsetComponentStartCbk ();
        defrUnsetComponentEndCbk ();
        defrUnsetConstraintCbk ();
        defrUnsetConstraintsStartCbk ();
        defrUnsetConstraintsEndCbk ();
        defrUnsetDefaultCapCbk ();
        defrUnsetDesignCbk ();
        defrUnsetDesignEndCbk ();
        defrUnsetDieAreaCbk ();
        defrUnsetDividerCbk ();
        defrUnsetExtensionCbk ();
        defrUnsetFillCbk ();
        defrUnsetFillStartCbk ();
        defrUnsetFillEndCbk ();
        defrUnsetFPCCbk ();
        defrUnsetFPCStartCbk ();
        defrUnsetFPCEndCbk ();
        defrUnsetFloorPlanNameCbk ();
        defrUnsetGcellGridCbk ();
        defrUnsetGroupCbk ();
        defrUnsetGroupExtCbk ();
        defrUnsetGroupMemberCbk ();
        defrUnsetComponentMaskShiftLayerCbk ();
        defrUnsetGroupNameCbk ();
        defrUnsetGroupsStartCbk ();
        defrUnsetGroupsEndCbk ();
        defrUnsetHistoryCbk ();
        defrUnsetIOTimingCbk ();
        defrUnsetIOTimingsStartCbk ();
        defrUnsetIOTimingsEndCbk ();
        defrUnsetIOTimingsExtCbk ();
        defrUnsetNetCbk ();
        defrUnsetNetNameCbk ();
        defrUnsetNetNonDefaultRuleCbk ();
        defrUnsetNetConnectionExtCbk ();
        defrUnsetNetExtCbk ();
        defrUnsetNetPartialPathCbk ();
        defrUnsetNetSubnetNameCbk ();
        defrUnsetNetStartCbk ();
        defrUnsetNetEndCbk ();
        defrUnsetNonDefaultCbk ();
        defrUnsetNonDefaultStartCbk ();
        defrUnsetNonDefaultEndCbk ();
        defrUnsetPartitionCbk ();
        defrUnsetPartitionsExtCbk ();
        defrUnsetPartitionsStartCbk ();
        defrUnsetPartitionsEndCbk ();
        defrUnsetPathCbk ();
        defrUnsetPinCapCbk ();
        defrUnsetPinCbk ();
        defrUnsetPinEndCbk ();
        defrUnsetPinExtCbk ();
        defrUnsetPinPropCbk ();
        defrUnsetPinPropStartCbk ();
        defrUnsetPinPropEndCbk ();
        defrUnsetPropCbk ();
        defrUnsetPropDefEndCbk ();
        defrUnsetPropDefStartCbk ();
        defrUnsetRegionCbk ();
        defrUnsetRegionStartCbk ();
        defrUnsetRegionEndCbk ();
        defrUnsetRowCbk ();
        defrUnsetScanChainExtCbk ();
        defrUnsetScanchainCbk ();
        defrUnsetScanchainsStartCbk ();
        defrUnsetScanchainsEndCbk ();
        defrUnsetSiteCbk ();
        defrUnsetSlotCbk ();
        defrUnsetSlotStartCbk ();
        defrUnsetSlotEndCbk ();
        defrUnsetSNetWireCbk ();
        defrUnsetSNetCbk ();
        defrUnsetSNetStartCbk ();
        defrUnsetSNetEndCbk ();
        defrUnsetSNetPartialPathCbk ();
        defrUnsetStartPinsCbk ();
        defrUnsetStylesCbk ();
        defrUnsetStylesStartCbk ();
        defrUnsetStylesEndCbk ();
        defrUnsetTechnologyCbk ();
        defrUnsetTimingDisableCbk ();
        defrUnsetTimingDisablesStartCbk ();
        defrUnsetTimingDisablesEndCbk ();
        defrUnsetTrackCbk ();
        defrUnsetUnitsCbk ();
        defrUnsetVersionCbk ();
        defrUnsetVersionStrCbk ();
        defrUnsetViaCbk ();
        defrUnsetViaExtCbk ();
        defrUnsetViaStartCbk ();
        defrUnsetViaEndCbk ();

        fclose(foutDef);

        // Release allocated singleton data.
        defrClear();

        return res;

    }//end method 

   




};//end class 

};//end namespace 

#endif