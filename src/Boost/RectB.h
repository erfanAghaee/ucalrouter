#ifndef RectB_H
#define RectB_H

#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/adapted/boost_polygon.hpp>

namespace traits{
    template <typename P, int D>
    struct access{};
};//end namespace 

namespace UCal{
    struct mypoint
    {
        double x,y;
    };//end struct 


    template<typename P1, typename P2>
    double distance(P1 const& a, P2 const& b){
       double dx = a.x - b.x;
       double dy = a.y - b.y;
       return sqrt(dx*dx + dy*dy); 
    }
};//end namespace 

#endif
