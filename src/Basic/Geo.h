#ifndef Geo_H
#define Geo_H


#include "Rect.h"

namespace UCal{
class Geo : public Rect{
    std::string layer_;
public:
    Geo(){layer_ = "-1";}

    // set & get 
    void setLayer(std::string layer){layer_ = layer;}
    std::string getLayer(){return layer_;}

    bool hasLayer(){if(layer_.compare("-1")==0)return false; return true;}

};//end class

};//end namespace 

#endif