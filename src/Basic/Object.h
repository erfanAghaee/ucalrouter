#ifndef OBJECT_H
#define OBJECT_H

#include "DefineTypes.h"
namespace UCal{

class Object{
public:
    Object(const IdType id){
        id_ = id;
    }//end method

    IdType getId(){return id_;}


private:
    IdType id_;

};//end class
};//name space 


#endif