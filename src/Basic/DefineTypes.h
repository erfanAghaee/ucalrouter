
#ifndef DEFINEDTYPES_H_
#define DEFINEDTYPES_H_

namespace UCal{

typedef unsigned int IdType;
typedef unsigned int DBU;
typedef std::pair<DBU,DBU> DBUxy;
enum LayerType{
        UNKNOWNLT, ROUTINGLT, CUTLT, OVERLAPLT
};//end enum

enum LayerDirection{
        UNKNOWNLD, HORIZONTALLD, VERTICALLD
};//end enum
};//end namespace 
extern int    argc_global;
extern char** argv_global;
#endif