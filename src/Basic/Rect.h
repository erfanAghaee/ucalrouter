#ifndef RECT_H
#define RECT_H

#include "DefineTypes.h"
#include <algorithm>
#include <string>

namespace UCal{

class Rect{
    double xl_;
    double xh_;
    double yl_;
    double yh_;


public:
    Rect(){
        xl_ = -1;
        xh_ = -1;
        yl_ = -1;
        yh_ = -1;
    }

    // set & get
    void setRect(double xl, double yl, double xh, double yh){
        xl_ = std::min(xl,xh);   xh_ = std::max(xl,xh);
        yl_ = std::min(yl,yh);   yh_ = std::max(yl,yh);
    }//end method 

    double getXl(){return xl_;}
    double getYl(){return yl_;}
    double getXh(){return xh_;}
    double getYh(){return yh_;}

    std::pair<double, double > getMin(){
        std::pair<double, double> pair; 
        pair.first = xl_; pair.second = yl_;
        return pair;
    }//end method

    std::pair<double, double > getMax(){
        std::pair<double, double> pair; 
        pair.first = xh_; pair.second = yh_;
        return pair;
    }






    // checker
    bool hasRect(){if(xl_ == -1 || xh_ == -1 || yl_ == -1 || yh_ == -1) return false; return true;}

    // get for debug purposes 
    std::string getString(){
        return "(" + std::to_string(xl_) + ", " + std::to_string(yl_) + ", " 
            + std::to_string(xh_) + ", " + std::to_string(yh_) + ") ";
    }
    
    std::string getStringMatlab(){
        return "\"" + std::to_string(xl_) + "\" , \"" + std::to_string(yl_)     + "\" , \""
             + std::to_string(xh_) + "\" , \"" + std::to_string(yh_) + "\" ";
    }
    
    std::string getStringMatlabDBU(int dataBaseUnit){
        return "\"" + std::to_string(xl_*dataBaseUnit) + "\" , \"" + std::to_string(yl_*dataBaseUnit)     + "\" , \""
             + std::to_string(xh_*dataBaseUnit) + "\" , \"" + std::to_string(yh_*dataBaseUnit) + "\" ";
    }


};//end class

};//end namespace 

#endif /* RECT_H */