#ifndef BSTreeSTD_H
#define BSTreeSTD_H

#include <iostream>
#include <set>

namespace UCal{

class BSTreeSTD{
    std::set<int> bTree_;
public:
    void start();
    void stop();
    void run();
public:
    void init(std::vector<int>& input);
    void print();
    void insert(int x);
    bool remove(int x);
    bool find(int x);
    


};//end class 

bool BSTreeSTD::remove(int value){
    if(!find(value)){
        return false;
    }//end if 
    bTree_.erase(value);
    return true;
}//end method 


void BSTreeSTD::init(std::vector<int>& input)
{
    for(auto itr : input){
        bTree_.insert(itr);
    }//end for  
    // print();    
};//end method  


void BSTreeSTD::start(){
};//end method 


bool BSTreeSTD::find(int value){
    if(bTree_.find(value) == bTree_.end())
        return false;
    return true;
}//end method 

void BSTreeSTD::insert(int value){
    bTree_.insert(value);
}//end method 


void BSTreeSTD::print(){
    for(auto itr: bTree_){
        std::cout << itr << std::endl;
    }//end for
};//end method 


};//end namespace 

#endif 