#ifndef BTREE_H
#define BTREE_H


#include <memory>
#include <boost/lambda/lambda.hpp>

namespace UCal{

class BTNode{
private:
    double  element_;
public:
    std::shared_ptr<BTNode> left_;
    std::shared_ptr<BTNode> right_;
    std::shared_ptr<BTNode> parent_;

    BTNode(){
        element_ = -1;
        left_    = nullptr;
        right_   = nullptr;
        parent_  = nullptr;
    }//end method 

    // set & get
    void setElement(double element){element_ = element;}
    double getElement() {return element_;}

    //checker
    bool hasElement(){if(element_ == -1)return false; return true;}
};//end class 

class BTree{
    std::shared_ptr<BTNode> root_;
    int     numNodes_;
public:
    BTree(){
        root_ = nullptr;
        numNodes_ = 0;
    }//end method 

    int getNumNodes(){return numNodes_;}


    // Tree functions

    // Insertion
    bool insertion(double x){
        std::shared_ptr<BTNode> p = findLast(x);
        std::shared_ptr<BTNode> newNode = getNodeBuilder(x);
        return addChild(p,newNode);
    }//end method 

    // Depth
    int depth(std::shared_ptr<BTNode> u){
        int d = 0;
        while(u != root_){
            u = u->parent_;
            d++;
        }
        return d;
    }//end method 
    

    void traverse(){
        std::shared_ptr<BTNode> u = root_;
        std::shared_ptr<BTNode> prev = nullptr;
        std::shared_ptr<BTNode> next = nullptr;

        while(u != nullptr){
            std::cout << "Node: " << u->getElement() << std::endl;
            if(prev == u->parent_){
                if(u->left_ != nullptr) 
                    next = u->left_;
                else if (u->right_ != nullptr) next = u->right_;
                else next = u->parent_;
            } else if( prev == u->left_){
                if(u->right_ != nullptr) next = u->right_;
                else
                    next = u->parent_;
            } else {
                next = u->parent_;
            }//end if 
            prev = u;
            u    = next;
        }//end method 

    }//end method 


    // get Builder
    std::shared_ptr<BTNode> getNodeBuilder(double element){
        std::shared_ptr<BTNode> node = std::make_shared<BTNode>();
        node->setElement(element);
        return node;
    }//end method 

private:
    std::shared_ptr<BTNode> findLast(double x){
        std::shared_ptr<BTNode> w = root_;
        std::shared_ptr<BTNode> prev = nullptr;
        while( w != nullptr){
            prev = w;
            int comp = compare(x, w->getElement());
            if(comp < 0)
            {
                w = w->left_;    
            }else if(comp > 0){
                w = w->right_;
            }else {
                return w;
            }
        }//end while
        return prev;
    }//end method 

    bool addChild(std::shared_ptr<BTNode> p, std::shared_ptr<BTNode> u){
        if(p == nullptr){
            root_ = u;
        }else{
            int comp = compare(p->getElement(),u->getElement());
            if(comp < 0){
                p->left_ = u;
            }else if(comp > 0){
                p->right_ = u;
            }else{
                return false;
            }//end if 
            u->parent_ = p;
        }//end if 
        numNodes_++;
        return true;
    }//end method 


    int compare(double x, double y){
        if(x > y)    
            return 1;
        else if (x < y)
            return -1;
        return 0;
    }

};//end class 


// class BTree{

// };//end class

};//end namespace 
#endif