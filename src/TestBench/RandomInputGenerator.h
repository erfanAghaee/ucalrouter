#ifndef RandomInputGenerator_H
#define RandomInputGenerator_H

#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <string>
#include <vector>

namespace UCal{

void randomInputFile(int len){
    std::ofstream     log_file_;
    log_file_.open("Log//InputRand.txt", std::ios::out | std::ios::trunc );
    int temp = 0;
    for(int i = 0 ; i < len; i++){
        temp = std::rand() % len ;
        log_file_ << std::to_string(temp) << "\n";
    }//end for 
    log_file_.close();
};//end method 


void readFile(std::vector<int>& vec){
    std::ifstream     log_file_;
    std::string       line;
    log_file_.open("Log//InputRand.txt");
    if (log_file_.is_open())
    {
        while ( getline (log_file_,line) )
        {
            vec.push_back(std::atoi (line.c_str()));
        // cout << line << '\n';
        }//end for 
    }//end if

    log_file_.close();
};//end method 

};//end namespace 








#endif        