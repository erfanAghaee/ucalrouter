#ifndef DATASTRUCTURETEST_H
#define DATASTRUCTURETEST_H

#include "Test.h"
#include "../DataStructure/BTree.h"
#include "../DataStructure/BSTreeSTD.h"
#include "RandomInputGenerator.h"
#include <stdlib.h>     /* srand, rand */
#include <chrono>


namespace UCal{


class DataStructureTest : public Test
{
private: 
    std::shared_ptr<Design> design_;
public:
    bool doTest(std::shared_ptr<Design> design) override{
        int len = 10000000;

        std::ofstream     log_file_;
        log_file_.open("Log//StatesLog.txt", std::ios::out | std::ios::trunc );

        // ----------------------------------------------
        std::cout << "Write the input file ... " << std::endl;
        auto start = std::chrono::steady_clock::now();

        
        randomInputFile(len);

        auto end = std::chrono::steady_clock::now();
            
        std::string message = "Elapsed time Write the input file in nanoseconds : " 
            + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
            + " ns" ;
        log_file_ << message << std::endl;
        // ----------------------------------------------
        std::cout << "Read the input file ... " << std::endl;
        start = std::chrono::steady_clock::now();
        std::vector<int> inputVec;
        
        readFile(inputVec);


        end = std::chrono::steady_clock::now();
            
        message = "Elapsed time Read the input file in nanoseconds : " 
            + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
            + " ns" ;
        log_file_ << message << std::endl;

        // ----------------------------------------------
        start = std::chrono::steady_clock::now();
        BSTreeSTD bTree;
        std::cout << "Init the BTree ... " << std::endl;
        bTree.init(inputVec);

        end = std::chrono::steady_clock::now();
            
        message = "Elapsed time Init the BTree in nanoseconds : " 
            + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
            + " ns" ;
        log_file_ << message << std::endl;

        // ---------------------------------------------
        start = std::chrono::steady_clock::now();
        std::cout << "Find 84 ... " << std::endl;
        bool bFind = bTree.find(8568844);        
        std::cout << "Found 84: " << bFind <<std::endl;
        end = std::chrono::steady_clock::now();
            
        message = "Elapsed time Find the BTree in nanoseconds : " 
            + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
            + " ns" ;
        log_file_ << message << std::endl;

        // std::cout << "Find 85 ... " << std::endl;
        // bFind = bTree.find(85);
        // std::cout << "Found 85: " << bFind <<std::endl;


        // std::cout << "Find 8500123 ... " << std::endl;
        // bFind = bTree.find(8500123);
        // std::cout << "Found 8500123: " << bFind <<std::endl;

        log_file_.close();
        
        // design_ = design;

        // BTree bTree;
        // int len = 10;
        // std::vector<double> vec = {4, 7, 8, 6, 4, 6, 7, 3, 10, 2};
        // for(int i = 0 ; i < len; i++)
        // {
        //     // double data = std::rand() % len + 1;
        //     bTree.insertion(vec[i]);
        //     // std::cout << data << ", ";
        // }//end for 
        // std::cout << std::endl;
        // // bTree.insertion(1);
        // std::cout << "Num Nodes: " << bTree.getNumNodes() << std::endl;
        // bTree.traverse();
        
        return true;
    }//end method 
};//end class



};//end namespace 

#endif