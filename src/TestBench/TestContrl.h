#ifndef TestContrl_H
#define TestContrl_H

#include <memory>
#include "Test.h"
#include <string>
#include "StatesTest.h"
#include "LefDefTest.h"
#include "DataStructureTest.h"
namespace UCal{

class TestContr{
public:
    std::shared_ptr<Test> getTest(std::string testType)
    {
        if(testType.empty()){
            return NULL;
        }		
        
        if(testType.compare("statesTest") == 0 ){
            auto StateTestPtr = std::make_shared<StatesTest>();
            return StateTestPtr;
        }else if (testType.compare("lefDefTest") == 0 ){
            auto lefDefTestPtr = std::make_shared<LefDefTest>();
            return lefDefTestPtr;
        }else if (testType.compare("DataStructureTest") == 0 ){
            auto lefDefTestPtr = std::make_shared<DataStructureTest>();
            return lefDefTestPtr;
        }//end if 

        return nullptr;

    }//end method 

};

};//end namespace 

#endif 