#ifndef TEST_H
#define TEST_H

#include <iostream>
#include "../Design/Design.h"

namespace UCal{

class Test{

public:
    virtual bool doTest(std::shared_ptr<Design> design){}
    
};//end class 

};//end namespace 

#endif 
