#ifndef TESTSTATE_H
#define TESTSTATE_H

#include "Test.h"

#include "../States/State.h"
#include "../States/StateContr.h"
#include <iostream>
#include <chrono>
#include <unistd.h>
#include <fstream>
#include "../Basic/DefineTypes.h"


namespace UCal{


class StatesTest : public Test
{
    bool doTest(std::shared_ptr<Design> design) override{
        
        // Input 
        std::vector<int> inputSize = {1};
        int avgRep = 1;

        //end Input

        std::cout << "State Test Start ...\n";
        // std::cout << "argc_global: "  << argc_global << std::endl;
        // getchar();
        UCal::StateContr stateContr;
        std::shared_ptr<UCal::State> state;
        std::ofstream     log_file_;
        log_file_.open("Log//StatesLog.txt", std::ios::out | std::ios::trunc );

        auto start = std::chrono::steady_clock::now();
        for(int idx = 0; idx < inputSize.size() ; idx++)
        {
            for(int i = 0 ; i < avgRep; i++)
            {
                for(int j = 0 ; j < inputSize[idx]; j++)
                {
                    state = stateContr.getState("start");
                    state->doAction();

                    state = stateContr.getState("stop");
                    state->doAction();
                }//end for 
            }//end for 
            auto end = std::chrono::steady_clock::now();
            //nanoseconds
            //microseconds
            //milliseconds
            //seconds
            std::string message = "Elapsed time "+std::to_string(inputSize[idx]) + "in nanoseconds : " 
                + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
                + " ns" ;
         	log_file_ << message << std::endl;


            // std::cout << "Elapsed time in nanoseconds : " 
            //     << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()
            //     << " ns" << std::endl;
        }//end for 
        log_file_.close();
        

        

        

        return true;
    }
};//end class 

};//end namespace 

#endif 