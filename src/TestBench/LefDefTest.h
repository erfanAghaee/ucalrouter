#ifndef LefDefTest_H
#define LefDefTest_H

#include "Test.h"

#include <iostream>
#include <chrono>
#include <unistd.h>
#include <fstream>
#include "../Basic/DefineTypes.h"

#include "../Parser/DefParser.h"
#include "../Parser/LefParser.h"
#include "../Parser/LefParserV2.h"
#include "../Parser/DefParserV2.h"
#include "../Parser/DefWriter.h"
#include <memory>


namespace UCal{


class LefDefTest : public Test
{
    private: 
        std::shared_ptr<Design> design_;
    // void setDesing(std::shared_ptr<Design> design) override {desing_ = design;}

    bool doTest(std::shared_ptr<Design> design) override{
        design_ = design;
        std::cout << "Design Addr2: " << design_ << std::endl;

        // Input 
        std::vector<int> inputSize = {1};
        int avgRep = 1;

        //end Input
        char** lefInFiles = new char*[2]; 
        char** defInFiles = new char*[2]; 
        defInFiles[0] = argv_global[0];
        defInFiles[1] = argv_global[2];
        lefInFiles[0] = argv_global[0];
        lefInFiles[1] = argv_global[1];
        // printf("defInFiles[0]: %s\n",defInFiles[0]);
        // printf("defInFiles[1]: %s\n",defInFiles[1]);
        // printf("lefInFiles[0]: %s\n",lefInFiles[0]);
        // printf("lefInFiles[1]: %s\n",lefInFiles[1]);

        // std::cout << "After change" << std::endl;
        // lefInFiles[1] = "Hi";
        // printf("defInFiles[0]: %s\n",defInFiles[0]);
        // printf("defInFiles[1]: %s\n",defInFiles[1]);
        // printf("lefInFiles[0]: %s\n",lefInFiles[0]);
        // printf("lefInFiles[1]: %s\n",lefInFiles[1]);

        // UCal::DefParser defParser(2,defInFiles);
        // UCal::LefParser lefParser(2,lefInFiles);
        argv_global++;
        UCal::LefParserV2 lefParser(*argv_global);
        argv_global++;
        UCal::DefParserV2 defParser(*argv_global,design);
        argv_global++;
        UCal::DefWriter defWriter(*argv_global,design);

      
    
        // getchar();
        // UCal::StateContr stateContr;
        // std::shared_ptr<UCal::State> state;
        // std::ofstream     log_file_;
        // log_file_.open("Log//StatesLog.txt", std::ios::out | std::ios::trunc );

        // auto start = std::chrono::steady_clock::now();
        // for(int idx = 0; idx < inputSize.size() ; idx++)
        // {
        //     for(int i = 0 ; i < avgRep; i++)
        //     {
        //         for(int j = 0 ; j < inputSize[idx]; j++)
        //         {
        //             state = stateContr.getState("start");
        //             state->doAction();

        //             state = stateContr.getState("stop");
        //             state->doAction();
        //         }//end for 
        //     }//end for 
        //     auto end = std::chrono::steady_clock::now();
        //     //nanoseconds
        //     //microseconds
        //     //milliseconds
        //     //seconds
        //     std::string message = "Elapsed time "+std::to_string(inputSize[idx]) + "in nanoseconds : " 
        //         + std::to_string(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())
        //         + " ns" ;
        //  	log_file_ << message << std::endl;


        //     // std::cout << "Elapsed time in nanoseconds : " 
        //     //     << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()
        //     //     << " ns" << std::endl;
        // }//end for 
        // log_file_.close();
        

        

        

        return true;
    }
};//end class 

};//end namespace 

#endif 