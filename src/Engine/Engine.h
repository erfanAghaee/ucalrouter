#ifndef ENGINE_H
#define ENGINE_H


#include "../Design/Design.h"
#include "../Parser/LefParserV2.h"
#include "../Parser/DefParserV2.h"
#include "../Library/Library.h"
#include <fstream>
#include <memory>

namespace UCal{


class Engine{
    std::shared_ptr<Design> design_;
public:
    Engine(std::shared_ptr<Design> design){
        design_ = design;

                // Input 
        std::vector<int> inputSize = {1};


        //end Input
        char** lefInFiles = new char*[2]; 
        char** defInFiles = new char*[2]; 
        defInFiles[0] = argv_global[0];
        defInFiles[1] = argv_global[2];
        lefInFiles[0] = argv_global[0];
        lefInFiles[1] = argv_global[1];
        // printf("defInFiles[0]: %s\n",defInFiles[0]);
        // printf("defInFiles[1]: %s\n",defInFiles[1]);
        // printf("lefInFiles[0]: %s\n",lefInFiles[0]);
        // printf("lefInFiles[1]: %s\n",lefInFiles[1]);

        // std::cout << "After change" << std::endl;
        // lefInFiles[1] = "Hi";
        // printf("defInFiles[0]: %s\n",defInFiles[0]);
        // printf("defInFiles[1]: %s\n",defInFiles[1]);
        // printf("lefInFiles[0]: %s\n",lefInFiles[0]);
        // printf("lefInFiles[1]: %s\n",lefInFiles[1]);

        // UCal::DefParser defParser(2,defInFiles);
        // UCal::LefParser lefParser(2,lefInFiles);
        argv_global++;
        UCal::LefParserV2 lefParser(*argv_global);
        argv_global++;
        UCal::DefParserV2 defParser(*argv_global,design);
        argv_global++;
        // UCal::DefWriter defWriter(*argv_global,design);


        
        Library* library = Library::getInstance();
        library->logMacroCells();


        design_->logInstances();
        library->logCellPins();
        library->logLayer();
        library->logMacroCells();

    }//end method 

};//end class

};//end namespace 

#endif